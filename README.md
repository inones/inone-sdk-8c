# inone-sdk-8c

#### 介绍
inone-sdk-8c是合一智联公开的8c平台的sdk, 适配全志D1-H/D1s/F133芯片，需要搭配合一智联的Aiinone-IDE集成开发环境整体使用，主要用于APP层GUI和控制层代码开发。
提供GUI可视化开发环境，提供windows下的交叉编译环境，一套环境搞定APP开发。

- APP UI开发
- 代码编程
- 代码编译
- 代码下载调试。

#### 软件架构
软件架构说明
![输入图片说明](tools/develop_flow.png)

#### 安装教程使用说明
SDK安装参考：
https://www.aiinone.cn/developer/aiinoneide/ide/install.html
Aiinone-IDE下载： https://www.aiinone.cn/developer.html

#### 特技

1.  使用aiinone的SDK包可以提供完备的GUI开发，多达30种控件；
2.  Aiinone-IDE + SDK搭配使用，可以提供优秀的嵌入式开发环境，让用户嵌入式系统开发更加得心应手；
3.  Aiinone-IDE + SDK搭配使用，智能化布局 简单快捷；
4.  Aiinone-IDE + SDK搭配使用，智能化编程 省时省心；
5.  Aiinone-IDE + SDK搭配使用，成果呈现   说见就见；
