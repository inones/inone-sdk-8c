@echo off
@echo config inone-sdk [START]
setlocal

if [%1]==[] ( 
	@echo config inone-sdk [FAIL], no sdk path paramter! 
	goto :eof
)

@echo [OLD] inone-sdk %INONE_SDK%
setx INONE_SDK %1
@echo [NEW] inone-sdk %1
endlocal
@echo config inone-sdk [END]  