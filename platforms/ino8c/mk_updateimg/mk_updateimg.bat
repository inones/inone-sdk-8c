@echo off
@echo make update image...

setlocal
@REM save paramter
set TSIZE=4194304
set PSIZE=1024
set ESIZE=65536

::%INONE_SDK%\tools\mkfs.jffs2 -x lzo -x zlib -lUv -p -s %PSIZE% -e %ESIZE% -d .\rootfs_data -o .\images\rootfs_data.img
%INONE_SDK%\tools\mkfs.jffs2 -x lzo -x zlib -lU -p %TSIZE% -s %PSIZE% -e %ESIZE% -d .\rootfs_data -o .\images\rootfs_data.img
%INONE_SDK%\tools\InonePackImage.exe -f .\pkgimage_rootfs_data.json
@echo Found [inoupdate.img] in folder @@ %~dp0\out @@
endlocal
PAUSE
