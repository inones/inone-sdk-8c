//mk_updateimg
=======================================
Usage:
  running mk_updateimg.bat to pack rootfs_data partition
===
1. rootfs_data -->rootfs_data partition data
2. images --> inoupdate raw partition images
3. out  -->inoupdate.img output folder
4. mk_updateimg.bat --> pack inoupdate scripts
5. pkgimage_xxx.json --> pack inoupdate configure file
