@echo off
@echo update-debug-inogui [START]
setlocal
set TARGET_DIR=/tmp/
set TARGET_APPDIR=%TARGET_DIR%
set TARGET_LIBDIR=%TARGET_DIR%

adb shell killall -9 inogui superdaemon
adb push lib\libinogui.so %TARGET_LIBDIR%
adb push lib\libinowidgets.so %TARGET_LIBDIR%
endlocal
@echo update-debug-inogui [END]  