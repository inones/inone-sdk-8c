/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __RPC_COMMON_H__
#define __RPC_COMMON_H__

#ifndef INT
#define INT int
#endif

#ifndef VOID
#define VOID void
#endif

#ifndef BOOL
#define BOOL unsigned char
#endif

#ifndef LPVOID
#define LPVOID void*
#endif

#ifndef ERR_OK
#define ERR_OK 0
#endif

#ifndef ERR_INVALID
#define ERR_INVALID -1
#endif

#ifndef INVALID_VALUE
#define INVALID_VALUE -1
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef STATIC
#define STATIC static
#endif

#ifndef LPCTSTR
typedef const char * LPCTSTR;
#endif

#ifndef UINT8
typedef unsigned char UINT8;
#endif

#ifndef UINT32
typedef unsigned int UINT32;
#endif

#define DDBUG		printf

#define LT_CHECK_RET(condition, ret)                        \
            if (!(condition)) return ret

#define LT_CHECK(condition)                                 \
            if (!(condition)) return

#ifndef TOSTRING
#define TOSTRING(A)			#A
#endif

#endif //__RPC_COMMON_H__