/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __CTRLANIMMANAGER_H__
#define __CTRLANIMMANAGER_H__

#include "inogui.h"
#include "ctrlanimators/ctrlanimator.h"
#include "ctrlanimators/ctrlanim_move.h"

namespace INONEGUI_FRAMEWORK {
#define DEF_CTRLANIM_DURATION        500
#define DEF_CTRLANIM_DELAY                0
#define DEF_CTRLANIM_TYPE                   ECTRL_ANIMATOR_MOVE
#define DEF_CTRLANIM_EASING              EEASING_POW_INOUT

STATIC INT CtrAnimThreadTask(void *para);
class CtrlAnimManager {  
public:  
        virtual ~CtrlAnimManager();
        STATIC CtrlAnimManager* GetInstance();
        VOID  DoInit();
        VOID  DeInit();
        CtrlAnimator* CreateCtrlAnimator(InoControl *pCtrl, ECtrlAnimType eAnimType = DEF_CTRLANIM_TYPE, EEasingType eEasing = DEF_CTRLANIM_EASING, UINT32 iDelay = DEF_CTRLANIM_DELAY, UINT32 iDuration = DEF_CTRLANIM_DURATION);      
        INT CtrlAnimManager_Remove( CtrlAnimator *pCtrlAnim);        
        friend INT CtrAnimThreadTask(void *para);
private:
        CtrlAnimManager();
        INT CtrlAnimManager_RemoveAll();
        INT CtrlAnimManager_Add(CtrlAnimator *pCtrlAnim);
        
private:
        STATIC CtrlAnimManager* mpInstance;
        LTGUI_THREAD *mpCtrlAnimThread;
        LTGUI_MUTEX *mpMutex;
        BOOL mExitAnimThread;
        CPtrArray mCtrlAnimList;
        CPtrArray mCtrlAnimDestoryingList;
}; 

#define CAM              	(CtrlAnimManager::GetInstance())
}//end namespace

#endif // __CTRLANIMMANAGER_H__

