/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __CTRLANIMATOR_H__
#define __CTRLANIMATOR_H__

#include "inocontrol.h"
#include "utils/easing.h"

namespace INONEGUI_FRAMEWORK {

typedef enum _CtrlAnimType{
        ECTRL_ANIMATOR_NONE,
        ECTRL_ANIMATOR_MOVE,
        ECTRL_ANIMATOR_FADE,
        MAX_ECTRL_ANIMATOR,
}ECtrlAnimType;

typedef enum _AnimStateType{
          ECTRL_ANIMATOR_CREATED = 0,
          ECTRL_ANIMATOR_RUNNING,
          ECTRL_ANIMATOR_PAUSED,
          ECTRL_ANIMATOR_STOPPED,
          ECTRL_ANIMATOR_DONE
} EAnimStateType;

typedef enum _AnimEventType{
        ECTRLANIM_EVENT_START = 0,
        ECTRLANIM_EVENT_STOP,
        ECTRLANIM_EVENT_PAUSE,
        ECTRLANIM_EVENT_ONCE,
        ECTRLANIM_EVENT_END,
        ECTRLANIM_EVENT_DESTORYED
} EAnimEventType;

typedef enum _CtrlAnimAttType{ 
        ECTRLANIM_ATT_REVERSED  = 0x0001,
        ECTRLANIM_ATT_DESTORY    = 0x0002,     //destory it when done
        ECTRLANIM_ATT_FOREVER    = 0x0004,     //animation forever
}ECtrlAnimAttType;

class CtrlAnimator;
typedef INT (*CbCtrlAnimListener)(CtrlAnimator *mpAnimCtrl, EAnimEventType eEventType, WPARAM wParam, LPARAM lParam);

class CtrlAnimator {
public:  
    CtrlAnimator();
    virtual ~CtrlAnimator();
    virtual ECtrlAnimType GetCtrlAnimatorType() = 0; 
    virtual INT UpdateCtrlAnimatorParam(FLOAT fPercent) = 0; 
    virtual INT InitialCtrlAnimator(InoControl *pCtrl, EEasingType eEasing, UINT32 iDelay, UINT32 iDuration);
    virtual INT UpdateCtrlStep(UINT32 iTimeValue); 
    virtual INT SetCtrlAnimator_PrivParam(STRING strName, STRING strVal);
    virtual INT StartCtrlAnimator();
    virtual INT StopCtrlAnimator();
    virtual INT PauseCtrlAnimator();
    virtual INT SetCtrlAnimatorListener(CbCtrlAnimListener pEventCb);
    virtual INT SetCtrlAnimator_TimeScale(FLOAT fTimeScale);
    virtual INT SetCtrlAnimator_Reversed(BOOL bReversed);
    virtual INT SetCtrlAnimator_Repeat(UINT32 iRepeatTimes);
    virtual INT SetCtrlAnimator_Yoyo(UINT32 iYoyoTimes);
    virtual INT SetCtrlAnimator_Object(InoControl *pCtrl);
    virtual INT SetCtrlAnimator_Easing(EEasingType eEasing);
    virtual INT NotifyListener(EAnimEventType eEventType, WPARAM wParam, LPARAM lParam);
    friend class CtrlAnimManager;
    
protected:
        InoControl *mpAnimCtrl;
        FLOAT mTimeScale;
        UINT32 mDelay;
        UINT32 mDuration;
        UINT32 mCTime;      //current time
        UINT32 mSTime;
        UINT32 mYoyoTimes;
        UINT32 mRepeatTimes;
        EAnimStateType mAnimState;
        EEasingType mEasingV;
        UINT32 mAttYoyoTimes;
        UINT32 mAttRepeatTimes;
        UINT8 mAnimAtt;

        CbCtrlAnimListener mpAnimListener;
}; 

}//end namespace

#endif // __CTRLANIMATOR_H__