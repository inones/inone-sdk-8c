/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __CTRLANIM_MOVE_H__
#define __CTRLANIM_MOVE_H__

#include "inogui.h"
#include "ctrlanimators/ctrlanimator.h"

namespace INONEGUI_FRAMEWORK {

#define CTRLANIM_XRANG              "x-r"
#define CTRLANIM_YRANG              "y-r"

class CtrlAnim_Move:
                                public CtrlAnimator {  
public:
        CtrlAnim_Move();
        virtual ~CtrlAnim_Move();  
        virtual ECtrlAnimType GetCtrlAnimatorType(); 
        virtual INT UpdateCtrlAnimatorParam(FLOAT fPercent); 
        virtual INT SetCtrlAnimator_PrivParam(STRING strName, STRING strVal);
protected:
        INT iFromX;
        INT iFromY;
        INT iEndX;
        INT iEndY;
}; 

}//end namespace

#endif //__CTRLANIM_MOVE_H__