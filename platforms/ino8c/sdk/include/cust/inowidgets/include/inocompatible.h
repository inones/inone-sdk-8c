/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOCOMPATIBLE_H__
#define __INOCOMPATIBLE_H__

#define NEW_SDK

#ifdef NEW_SDK
	#define LTControl InoControl
	#define LTLabel InoLabel
	#define LTButton InoButton
	#define LTEdit InoEdit
	#define LTImage InoImage
	#define LTListItem InoListItem
	#define LTMeter InoMeter
	#define LTRadioGroup InoRadioGroup
	#define LTScrollBar InoScrollBar
	#define LTSeekBar InoSeekBar
	#define LTScrollList InoScrollList
#else
	#define InoControl LTControl
	#define InoLabel LTLabel
	#define InoButton LTButton
	#define InoEdit LTEdit
	#define InoImage LTImage
	#define InoListItem LTListItem
	#define InoMeter LTMeter
	#define InoRadioGroup LTRadioGroup
	#define InoScrollBar LTScrollBar
	#define InoScrollList LTScrollList
	#define InoSeekBar LTSeekBar
#endif

#endif //__INOCOMPATIBLE_H__
