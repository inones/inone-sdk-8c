/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __MOVE_H__
#define __MOVE_H__

#include "inogui.h"
#include "log.h"
#include <vector>

namespace INONEGUI_FRAMEWORK {

//描述物体的运动

// 轨迹
enum EMoveCurve
{
	MOVE_INVALID,		// 非法
	MOVE_LINE,			// 直线
	MOVE_SEMIOVAL,		// 半椭圆
	MOVE_XY,			// XY同时运动(结合MOVE_V_VARYBYDIS)
	MOVE_POINTSET		// 点集 
};

// 速度
enum EVelocityType
{
	MOVE_V_NORMAL,		// 匀速
	MOVE_V_UAM,			// 匀加速
	MOVE_V_VARYBYDIS	// 变速(因距离)
};

/****************************************************
*一次椭圆运动轨迹: x^2/a^2 + y^2/b^2 = 1;
*起点和终点间的直线距离 d = 2a, 起终点连线中心为椭圆圆心
*当中心在原点, 旋转角度为0
*以此类推,要实现其它形状的椭圆曲线,
可控制中心点和旋转角度来得到,实现复合曲线可分多次实现
****************************************************/

typedef struct SCurveParam
{
//
	int		nFrame;			// [通用]几个单位时间刷新一次 >= 1

// 曲线类
	POINT	ptLTStart;		// [曲线通用]起点(物体的左上角)
	POINT	ptLTEnd;		// [曲线通用]终点
	int		v;				// [曲线通用]单位为  像素/frame
	int		rate;			// [椭圆]椭圆长轴/短轴比例差(-99到+99), 0代表a/b=1 比如30代表  a/b = 31; -3 代表 a/b = 1/4;		
	BOOL	bMaxInY;		// [椭圆]固定顺时针旋转, 是否选择Y值方向较大的半椭圆

// 点集
	vector<POINT> vtPtSet;	// [点集] 点集运动的集合, 以与终点的下标就近原则为方向
	int		nCurIndex;		// 从0开始
	int		nDstIndex;

// 速度类型
	EVelocityType	eVType;
	float	fVaryDisRate;	// MOVE_V_VARYBYDIS 有效 0<x<1 因变量x, 与其总距离剩余值的比值
	int		nMinMove;		// MOVE_V_VARYBYDIS 有效
	int		nMaxMove;		// MOVE_V_VARYBYDIS 有效

	int		a;				// 加速度
	
	SCurveParam()
	{
		ptLTStart.x	= 0;
		ptLTStart.y	= 0;
		ptLTEnd.x	= 0;
		ptLTEnd.y	= 0;
		v			= 0;
		nFrame		= 1;
		rate		= 0;
		bMaxInY		= TRUE;
		fVaryDisRate = 0.15f;
		nMinMove	= 5;
		nMaxMove	= 80;

		a			= 5;

		vtPtSet.clear();

		eVType		= MOVE_V_NORMAL;
	}
}SCurveParam;

class CMove
{
public:
	CMove(void);
	~CMove(void);

private:
	EMoveCurve		m_eCurveType;
	POINT			m_ptLTCur;		//当前点
	int				m_nFrameCnt;	//计数器
	BOOL			m_bTerminate;	//结束
	BOOL			m_bStart;
protected:
	SCurveParam		m_sParam;

public:
	void	Start(void);
	void	Stop(void);
	BOOL	AddTick(void);		//在物体所属

protected:
	VOID InitSCurveParam(SCurveParam &rDstParam, SCurveParam *pSrcParam);
	/*********初始化一次运动,外部调用啦.*********************************
	*	eMoveCurve	[in]轨迹曲线类型
	*	ptLTStart	[in]起点
	*	ptLTend		[in]终点
	*	nSpeed		[in]水平或垂直方向的速度,如果起终点差的大小来决定
	*	nFrame		[in]多少个单位时间刷新一次
	*********************************************************************/
	BOOL			InitMove(EMoveCurve eMoveCurve, SCurveParam* pParam);
	virtual void	ReDrawObject(POINT posCur){}		//刷新物体
	virtual void	ArriveTerminate(void){}				//到达终点

private:
	BOOL	GetCurLTPos(POINT& pos);
	BOOL	CacuCurPos(void);					//计算当前点
	BOOL	IsArriveTerminate(POINT& posCur, POINT posE);	//看是否已达终点
	void	SetSpeedDirection(int nDisXorY);
	BOOL	m_bSpeedInX;						//速度是否为X方向的平移速度, FALSE代表Y方向
	BOOL	m_bSpeedPositive;					//正向速度

//直线
private:
	float	m_K;			//斜率k	y=kx+b
	int		m_B;			//常数b
	BOOL	m_bVerLine;		//竖线
	BOOL	m_bHorLine;		//横线
	void	InitLine(void);
	void	CacuLineCurPos(void);
	int		GetMov(int s, int d);	// 根据距离变速
	int		GetMovUAM(int s, int d);

//半椭圆
private:
	int		m_Hv;		//h,k为椭圆心
	int		m_Kv;
	int		m_Av;
	int		m_Bv;
	int		m_Dis;		//直线长
	float	m_cosx;		//旋转角
	float	m_sinx;		//旋转角
	float	m_Cv;		//常数
	void	InitSemiOval(SCurveParam* pSemiOval);
	void	CacuOvalCurPos(void);
	POINT	m_ptTmpS;	//起，终点
	POINT	m_ptTmpE;
	POINT	m_ptLTRealv;

// 点集
private:
	void	InitPtSet(SCurveParam* pPtSet);
	void	CacuPtSetCurPos(void);
	int		m_nCurIndex;		// 起点下标
	int		m_nDstIndex;		// 终点下标
	int		m_bRight;
	vector<POINT>	m_vtPts;

//测试画出曲线
private:
	int		m_nTimes;					//标记
	POINT	m_ptLast;
	void	DrawPoint(POINT pt);		//测试画点
};

}
#endif //__MOVE_H__