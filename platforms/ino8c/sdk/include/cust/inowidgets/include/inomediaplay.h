/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOMEDIAPLAY_H__
#define __INOMEDIAPLAY_H__

#include "inocontrol.h"

namespace INONEGUI_FRAMEWORK {

/**
  *The response of state change notices APP what current state of player.
  */
typedef enum _INOMPNotifyAppType
{
    INOMP_NOTIFY_PREPARED			          	= 0,
    INOMP_NOTIFY_PLAYBACK_COMPLETE       	= 1,
    INOMP_NOTIFY_SEEK_COMPLETE		 	= 2,
    INOMP_NOTIFY_MEDIA_ERROR			 	= 3,
    INOMP_NOTIFY_NOT_SEEKABLE			= 4,
    INOMP_NOTIFY_BUFFER_START			= 5, /*this means no enough data to play*/
    INOMP_NOTIFY_BUFFER_END				= 6, /*this means got enough data to play*/
    INOMP_NOTIFY_DOWNLOAD_START			= 7,//not support now
    INOMP_NOTIFY_DOWNLOAD_END			= 8,//not support now
    INOMP_NOTIFY_DOWNLOAD_ERROR		= 9,//not support now
    INOMP_NOTIFY_MEDIA_VIDEO_SIZE		= 10, /*notified while video size changed*/
    INOMP_NOTIFY_VIDEO_FRAME				= 11,//notify the decoded video frame
    INOMP_NOTIFY_AUDIO_FRAME				= 12,//notify the decoded audio frame
    INOMP_NOTIFY_SUBTITLE_FRAME			= 13,//notify the decoded subtitle frame
    INOMP_NOTYFY_DECODED_VIDEO_SIZE		=14,//notify the decoded video size
}INOMPNotifyAppType;

typedef enum _INOMPErrorType
{
    INOMP_ERROR_UNKNOWN			= 1,
    INOMP_ERROR_OUT_OF_MEMORY	= 2,//not support now
    INOMP_ERROR_IO					= 3,
    INOMP_ERROR_UNSUPPORTED		= 4,
    INOMP_ERROR_TIMED_OUT		= 5,//not support now
}INOMPErrorType;

typedef enum _INOMPSpeedType
{
    INOMP_SPEED_FFX16  		=  0,   /*fast forward 16 times*/
    INOMP_SPEED_FFX8   		=  1,   /*fast forward 8 times*/
    INOMP_SPEED_FFX4   		=  2,   /*fast forward 4 times*/
    INOMP_SPEED_FFX2   		=  3,   /*fast forward 2 times*/
    INOMP_SPEED_1			=  4,   /*normal play*/
    INOMP_SPEED_FBX2   		=  5,   /*fast backward 2 times*/
    INOMP_SPEED_FBX4   		=  6,   /*fast backward  4 times*/
    INOMP_SPEED_FBX8   		=  7,   /*fast backward  8 times*/
    INOMP_SPEED_FBX16  		=  8,   /*fast backward  16 times*/
}INOMPSpeedType;


typedef enum _INOMPStatusType
{
    INOMP_INVALID  			=  -1,  
    INOMP_IDEL  				=  0,  
    INOMP_INITIALIZED   		=  1,   
    INOMP_PREPARING   		=  2,   
    INOMP_PREPARED   		=  3,
    INOMP_STARTING			=  4,
    INOMP_STARTED			=  5,   
    INOMP_PAUSED   			=  6, 
    INOMP_STOPPED   			=  7, 
    INOMP_COMPLETE   		=  8, 
    INOMP_ERROR  			=  9,
}INOMPStatusType;

enum EMpPrivAtt
{
	INOMP_PATT_NOTHING 	= 0x00,
	INOMP_PATT_LOOPING 	= 0x01,
	INOMP_PATT_REQREFR	= 0x02,  //request refresh
	INOMP_PATT_READY 	= 0x04,
};

#define ClrMpPrivAttAll()               	  	    CLRBIT_ALL(mMpPrivAtt)
#define SetMpPrivAttValue(iBitValue)           SETBIT_ALL(mMpPrivAtt,iBitValue)
#define ClrMpPrivAttByBit(iBitValue)            CLRBIT_BY_BIT(mMpPrivAtt,iBitValue)
#define SetMpPrivAttByBit(iBitValue)            SETBIT_BY_BIT(mMpPrivAtt,iBitValue)
#define IsSetMpPrivAttByBit(iBitValue)         ISSET_BY_BIT(mMpPrivAtt,iBitValue)
#define IsSetMpPrivAttByMask(iBitValue)     ISSET_BY_MASK(mMpPrivAtt,iBitValue)

class InoMediaPlay;
class IMediaPlayListener {  
public:  
    virtual ~IMediaPlayListener() { }  
    virtual VOID OnMediaPlayCallback(InoMediaPlay *pMediaPlay, INOMPStatusType eMpStatus, WPARAM wParam = NULL, INT iCurTimeMs = INVALID_VALUE, INT iDurTimeMs = INVALID_VALUE) = 0; 
};  

class api_out InoMediaPlay : public InoControl
{	
	public:
		InoMediaPlay();
		virtual ~InoMediaPlay();
		INT GetControlType();
		virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
		virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
		VOID SetInoMpListener(IMediaPlayListener *pListener); 
		INT InoMpSetDataSource(LPCTSTR pDataPath);
		INT InoMpStart();
		INT InoMpPause();
		INT InoMpStop();
		INT InoMpReset();
		INT InoMpSetLooping(BOOL bLoop);
		BOOL InoMpIsPlaying();
		INT InoMpGetCurPos(INT* pTimeMs);
		INT InoMpGetDuration(INT* pTimeMs);
		INT InoMpSeekTo(INT iSeekTimeMs);
		INT InoMpSetSpeed(INOMPSpeedType iSpeed);		
		INT InoMpSetVolume(INT iVolume);
		INT InoMpGetVolume();
		INT InoMpSetAudioMute(BOOL bMute);
		INT InoMpLayerShow(BOOL bEnable);
		INOMPStatusType InoMpGetStatus(){return mMpStatus;};
		
	private:
		STATIC  INT InoMpCallback(VOID* pUserData, INT iMsg, INT iParam0, VOID* pParam1);
		INT OnTimer(INT iInterval);
		VOID InoMpNotifyListener(INOMPStatusType eStatus, WPARAM wParam = NULL);
		VOID InoMpSetStatus(INOMPStatusType eStatus, WPARAM wParam = NULL);
		
	private:
		IMediaPlayListener *mpMpListener;
		LPVOID mpPlayer;
		LTGUI_MUTEX *mpMutex;
		LTGUI_SEM *mpPreparedSem;
		INOMPStatusType mMpStatus;
		UINT16	mTimerCnt;
		UINT16 mMpRefreshDelay;
		UINT8   mMpDrawCnt;
		UINT8   mMpPrivAtt;
};

}
#endif //__INOMEDIAPLAY_H__