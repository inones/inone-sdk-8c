/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

/**
*Control CONST define
*/
#ifndef __CONTROL_DEF_H__
#define __CONTROL_DEF_H__

//Depend pc tools color selection
#define CTRL_COLOR_HASALPHA	1

//Resource pack define
#define ID_RES_IMG				0x100
#define MAX_DRAWABLE_NUM	128
#define MAX_RAW_NUM			128
#define MAX_LANG_NUM			1024

//Message BOX define TIMEOUT 
#define BOX_TIMEOUT_SEC        5

#define ALPHA_OPA			     0xFF000000
//Color define
#define COLOR_BLACK                    0xFF000000
#define COLOR_WHITE                    0xFFFFFFFF
#define COLOR_RED                         0xFFFF0000
#define COLOR_GREEN                    0xFF00FF00
#define COLOR_BLUE                       0xFF0000FF

//WHITE
#define COLOR_SNOW                     0xFFFFFAFA
#define COLOR_AZURE                    0xFFF0FFFF
#define COLOR_SILVER                   0xFFC0C0C0
#define COLOR_FLORAWHITE         0xFFFFFAF0
#define COLOR_MISTYROSE            0xFFFFE4E1

//GRAY
#define COLOR_GRAY                      0xFF808080
#define COLOR_LIGHTGRAY            0xFFD3D3D3
#define COLOR_SILVER                   0xFFC0C0C0
#define COLOR_DARKGRAY             0xFFA9A9A9
#define COLOR_SLATEGRAY            0xFF708090
#define COLOR_DARKSLATEGRAY   0xFF2F4F4F

//BLUE
#define COLOR_LIGHTBLUE               0xFFADD8E6
#define COLOR_SKYBLUE                   0xFF87CEEB
#define COLOR_ROYALBLUE               0xFF4169E1
#define COLOR_DARKBLUE                0xFF00008B

//FONTSIZE
#define FONTSIZE_SMALL                15
#define FONTSIZE_NORMAL             20
#define FONTSIZE_BIG                     25

//Base Control define
#define CTRL_CST_D_BGIMG      ""
#define CTRL_CST_D_BGCLR       COLOR_DARKSLATEGRAY
#define CTRL_CST_D_BGRAD    0
#define CTRL_CST_D_NAME             "ctrl"
#define CTRL_CST_D_STATE            0

typedef enum{
    CTRL_STATE_NORMAL      = 0x0,
    CTRL_STATE_PRESSED,
    CTRL_STATE_SELECTED,
    CTRL_STATE_PSELECTED,
    CTRL_STATE_DISABLED,
    CTRL_STATE_INVISIBLE,
    MAX_CTRL_STATE,
}ECtrlStateType;

enum{
    CTRL_STATE_ALL = MAX_CTRL_STATE,
    CTRL_STATE_ENABLE = CTRL_STATE_DISABLED,
    CTRL_STATE_VISIBLE = CTRL_STATE_INVISIBLE
};

#define CTRL_DRAWFLAG_ALIGNOFF	16
typedef enum{
	CTRL_DRAWFLAG_NOTHING    = 0x00000000,
	CTRL_DRAWFLAG_BK                = 0x00000001,
	CTRL_DRAWFLAG_CONTENT     = 0x00000002,
	CTRL_DRAWFLAG_BTNIPOS     = 0x00000100,   //using button image pos to draw button size
	CTRL_DRAWFLAG_ALIGNMASK = 0x00FF0000,
	CTRL_DRAWFLAG_ALL               = 0x000000FF,
}ECtrlDrawFlagType;

//For event lParam info
//High 16bit for private
//Low 16bit for common
#define CTRL_EVENTFLAG_PRIVATE_OFF	16
typedef enum{
	CTRL_EVENTFLAG_NOTHING		= 0x0000,
	CTRL_EVENTFLAG_NOSELECTED	= 0x0001,
	CTRL_EVENTFLAG_NOACTION		= 0x0002,
	CTRL_EVENTFLAG_NOFOCUS		= 0x0004,
	CTRL_EVENTFLAG_NOREFRSH		= 0x0008,
}ECtrlEventFlagType;

typedef enum{
	CTRL_ATTRIB_NOTHING			= 0x0000,
	CTRL_ATTRIB_SELECTED		= 0x0001,
	CTRL_ATTRIB_FOCUSED			= 0x0002,
	CTRL_ATTRIB_CONT			= 0x0004,
	CTRL_ATTRIB_MOVE			= 0x0008,   //move control
	CTRL_ATTRIB_TIMER			= 0x0010,    //enable timer
	CTRL_ATTRIB_RETOUCH			= 0x0020,    //recheck touch area for some control,e.g circle seekbar
	CTRL_ATTRIB_OPA             = 0x0040,    //control opacity
	CTRL_ATTRIB_ROT             = 0x0080,    //control rotate
}ECtrlAttribType;

enum{
	ALIGN_NOTHING  	= 0x0,
	ALIGN_LEFT          	= 0x1,
	ALIGN_HCENTER   	= 0x2,
	ALIGN_RIGHT       	= 0x10,
	ALIGN_TOP           	= 0x4,
	ALIGN_VCENTER    	= 0x8,
	ALIGN_BOTTOM    	= 0x20,
	ALIGN_HVCENTER    	= ALIGN_HCENTER|ALIGN_VCENTER,
};
#define ALIGN_H  			(ALIGN_LEFT|ALIGN_RIGHT|ALIGN_HCENTER)
#define ALIGN_V  			(ALIGN_TOP|ALIGN_BOTTOM|ALIGN_VCENTER)

typedef enum{
    CTRL_ACTION_NOTHING			= 0x0000,
    CTRL_ACTION_CLICK				= 0x0001,
    CTRL_ACTION_LONGPRESS			= 0x0002,
    CTRL_ACTION_SB_CHANGE			= 0x0003,
    CTRL_ACTION_RG_CHANGE			= 0x0004,
    CTRL_ACTION_SCL_FOCUS			= 0x0005,
    CTRL_ACTION_SCL_CHANGE		= 0x0006,
    CTRL_ACTION_CAS_EVENT			= 0x0007,
    CTRL_ACTION_CAS_DRAW			= 0x0008,
}ECtrActionType;

typedef enum{
    CTRL_EVENT_NOTHING        = 0x0000,
    CTRL_EVENT_CAPTURE         = 0x0001,
    CTRL_EVENT_REDRAW          = 0x0002,
    CTRL_EVENT_NOTIFY            = 0x0004,
    CTRL_EVENT_OUTSIDE         = 0x0008,
}ECtrEventType;


#define CTRL_ATT_STATE_OFF		0
#define CTRL_ATT_BGRAD_OFF	8
#define CTRL_ATT_PRIOR_OFF	16
enum ECtrlAtt
{
	CTRL_ATT_STATE	     	=	0x00000F,		   //control state
	CTRL_ATT_BGRAD	     	=	0x00FF00,                 //bg raduis
	CTRL_ATT_PRIOR	     	=	0xFF0000,                 //ctrl prior
};

#define CTRL_KEY_S_OBJECT               	"ctrl"
#define CTRL_KEY_S_BGIMG            	"bgimg"
//#define CTRL_KEY_S_BGCLR            	"bgclr"
#define CTRL_KEY_I_BGCLR            	"bgclr"
#define CTRL_KEY_S_NAME                   	"name"
#define CTRL_KEY_I_ID	                     	"id"
#define CTRL_KEY_S_POS                     	"pos"
#define CTRL_KEY_I_ATT                  	"att"
#define CTRL_KEY_S_PAD                     	"pad"     //relative value(left/top,right/bottom)

//Label default define
/*
typedef struct _LblAttField
{
	UINT32 iFontSZ:8;
	UINT32 iAlign:6;
	UINT32 bBold:1;
	UINT32 bItalic:1;
	UINT32 iEllipsis:4;
	UINT32 iMarquee:4;
	UINT32 iAutoWarp:1;
}LblAttField;
typedef LblAttField* PLblAttField;
*/
	
#define LBL_ATT_FONTSZ_OFF			0
#define LBL_ATT_ALIGN_OFF			8
#define LBL_ATT_ELLIPSIS_OFF		16
#define LBL_ATT_MARQUEE_OFF		20
enum ELabelAtt
{
	LBL_ATT_FONTSZ	=	0x00000FF,	 //Font size
	LBL_ATT_ALIGN	     	=	0x0003F00,             //text align
	LBL_ATT_BOLD	     	=	0x0004000,             //text bold
	LBL_ATT_ITALIC     	=	0x0008000,             //text italic
	LBL_ATT_ELLIPSIS     =	0x00F0000,             //text ellipse mask
	LBL_ATT_MARQUEE    =	0x0F00000,             //text Marquee mask
};

//text ellipse mask define
enum ELabelEllipsis
{
	LBL_ATT_ELLIPSIS_NOTHING	     	=	0x0,		//Ellipsis nothing,don't format text
	LBL_ATT_ELLIPSIS_END	     		=	0x1,		//Ellipsis end
	LBL_ATT_ELLIPSIS_MIDDLE		=	0x2,        //Ellipsis middle
	LBL_ATT_ELLIPSIS_START		=	0x3,        //Ellipsis start
	LBL_ATT_ELLIPSIS_MARQUEE		=	0x4,        //Ellipsis marquee
	LBL_ATT_ELLIPSIS_AUTOWARP	=	0x5,        //Ellipsis autowarp
};

//text marquee mask define
enum ELabelMarquee
{
	LBL_ATT_MARQUEE_LTR     	=	0x0,        //Marquee left to right
	LBL_ATT_MARQUEE_RTL        	=	0x1,        //Marquee right to left
	LBL_ATT_MARQUEE_UTD        =	0x2,        //Marquee up to down
	LBL_ATT_MARQUEE_DTU        =	0x3,        //Marquee down to up
};

/*
typedef struct _LblAtt1Field
{
	UINT32 iRefInterV:8;
	UINT32 iRefStepV:8;
}LblAtt1Field;
*/

#define LBL_ATT1_RINTERV_OFF	0
#define LBL_ATT1_RSTEPV_OFF	8
enum ELabelAtt1
{
	LBL_ATT1_RINTERV	     	=	0x00FF,			//slide refresh interval
	LBL_ATT1_RSTEPV	     	=	0xFF00,                 //slide refresh step	
};

#define LBL_CST_D_TEXTCOLOR_NOR     COLOR_WHITE
#define LBL_CST_D_TEXTCOLOR_PRE      COLOR_WHITE
#define LBL_CST_D_TEXTCOLOR_SEL       COLOR_WHITE
#define LBL_CST_D_TEXTCOLOR_PSEL     COLOR_WHITE
#define LBL_CST_D_TEXTCOLOR_DIS       COLOR_WHITE
#define LBL_CST_D_FONTSZ       		   FONTSIZE_NORMAL
#define LBL_CST_D_ALIGN       		   ALIGN_HVCENTER

#define LBL_KEY_S_OBJECT              "lbl"
#define LBL_KEY_S_LBL                     "lbl" 
#define LBL_KEY_S_LBLCLR              "lblclr"
#define LBL_KEY_S_LBLSET               "lblset"
#define LBL_KEY_I_LBLATT               "lblatt"
#define LBL_KEY_I_LBLATT1             "lblatt1"
 
//Button default value
#define BTN_ATT_LPDT_OFF		0
#define BTN_ATT_LPRT_OFF		8
#define BTN_ATT_ALIGN_OFF		16
enum EButtonAtt
{
	BTN_ATT_LPDT	     	=	0x0000FF,		//long press durtion time unit: 50ms
	BTN_ATT_LPRT	     	=	0x00FF00,             //long press repeat time unit: 50ms
	BTN_ATT_ALIGN	     	=	0x3F0000,             //button image align value
	BTN_ATT_BEEP	     	=	0x400000,             //button beep: 1:on / 0: off
};

#define BTN_CST_D_BTNCOLOR_NOR      0
#define BTN_CST_D_BTNCOLOR_PRE       0
#define BTN_CST_D_BTNCOLOR_SEL        0
#define BTN_CST_D_BTNCOLOR_PSEL     0
#define BTN_CST_D_BTNCOLOR_DIS       0
#define BTN_CST_D_BTNLP                       0
#define BTN_CST_D_BTNREPT                  200
#define BTN_CST_D_RADIUS                    20

#define BTN_KEY_S_OBJECT              "btn"
#define BTN_KEY_S_BTNIMG             "btnimg"
#define BTN_KEY_S_BTNIPOS           "btnipos"     //btn image pos
#define BTN_KEY_S_BTNCLR              "btnclr"
#define BTN_KEY_I_BTNATT              "btnatt"

//Scrolllist define
#define SCL_ATT_ROW_OFF		16
#define SCL_ATT_COLUM_OFF		24
enum EScrollListAtt
{
	SCL_ATT_ROLLER	     	=	0x00000001,
	SCL_ATT_DIRH	     		=	0x00000002,
	SCL_ATT_ALIGNEDGE	=     0x00000004,
	SCL_ATT_SHOWSB		=	0x00000008,
	SCL_ATT_DRAGANIM		=	0x00000010,
	SCL_ATT_ALIGNPAGE	=	0x00000020,
	SCL_ATT_STATICLOAD	=	0x00000040,
	SCL_ATT_VARLENLIST	=	0x00000080,
	SCL_ATT_ROW			=	0x00FF0000,
	SCL_ATT_COLUM			=	0xFF000000,
};

#define SCL_ATT1_MDRAGDIST_OFF	0
#define SCL_ATT1_SCLBARWH_OFF	8
#define SCL_ATT1_RMARGIN_OFF		16
#define SCL_ATT1_CMARGIN_OFF		24
enum EScrollListAtt1
{
	SCL_ATT1_MDRAGDIST	     		=	0x000000FF,	// max drag distance
	SCL_ATT1_SCLBARWH	     		=	0x0000FF00,	//scroll bar width/high
	SCL_ATT1_RMARGIN				=     0x00FF0000,	//row margin
	SCL_ATT1_CMARGIN				=	0xFF000000,  //colum margin
};

//Scrolllist default value
#define SCL_CST_D_ROW                   4 
#define SCL_CST_D_COLUMN             1
#define SCL_CST_D_FIRSTINDEX      0
#define SCL_CST_D_ITEMGAP            2
#define SCL_CST_D_DATACOUNT       3
#define SCL_CST_D_SCLBARWH        15
#define SCL_CST_D_SCLBARIMAGE   ""
#define SCL_CST_D_MDRAGDIST		100
#define SCL_CST_D_SCLBARCOLOR     COLOR_SILVER
#define SCL_CST_D_ATT     		  (SCL_ATT_SHOWSB|SCL_ATT_DIRH|SCL_ATT_ALIGNEDGE|SCL_ATT_ALIGNPAGE)

#define SCL_KEY_S_OBJECT              "scl"
#define SCL_KEY_I_SCLFIND            "sclfind"	      //scrollist first index
#define SCL_KEY_S_SCLBARIMG       "sclbimg"   //scrollbar image
#define SCL_KEY_I_SCLBARCLR        "sclbclr"     //scrollbar color
#define SCL_KEY_I_SCLATT               "sclatt"      //scrolllist attrib
#define SCL_KEY_I_SCLATT1             "sclatt1"     //scrolllist attrib1

//listitem default value
#define LI_CST_D_LICOLOR_NOR      COLOR_LIGHTBLUE|0xFF<<24
#define LI_CST_D_LICOLOR_PRE       COLOR_SKYBLUE|0x40<<24
#define LI_CST_D_LICOLOR_SEL        COLOR_ROYALBLUE|0x30<<24
#define LI_CST_D_LICOLOR_DIS       COLOR_DARKBLUE|0x80<<24
#define LI_CST_D_RADIUS              20

#define LI_KEY_S_OBJECT              "li"


//edit define
#define ED_ATT_PWDRCHAR_OFF		0
#define ED_ATT_CHARTYPE_OFF		8
enum EEditAtt
{
	ED_ATT_PWDRCHAR		= 	0x00FF,	//password replace char
	ED_ATT_CHARTYPE		= 	0x0F00,   //chartype mask
	ED_ATT_EDPWD			=	0x1000,   //is edit passwd?
};

enum EEditAttCharStyle{
	ED_ATT_CHARTYPE_TEXT	     		=	0x0000,
	ED_ATT_CHARTYPE_DIGITAL	     	=	0x0001,
	ED_ATT_CHARTYPE_ALPHA	     	=	0x0002,
	ED_ATT_CHARTYPE_PUNCT		=     0x0003,
};

#define ED_KEY_S_OBJECT                "ed"
#define ED_KEY_S_EDHT                	   "edht"   //edit hint message
#define ED_KEY_I_EDATT                   "edatt"

//edit default value
#define ED_CST_D_EDHT             		"Input"
#define ED_CST_D_PWDRCHAR              '*'

//seekbar define
#define SB_ATT_STYLE_OFF		0
#define SB_ATT_RDIFF_OFF		4
#define SB_ATT_SANGLE_OFF		12
#define SB_ATT_EANGLE_OFF		21
enum ESeekBarAtt
{
	SB_ATT_STYLE	     		=	0x0000000F,       //style mask
	SB_ATT_RDIFF	     		=	0x00000FF0,       //outside-inside radius diff value (0 ~ 255)
	SB_ATT_SANGLE	     		=	0x001FF000,       //circle start angle (0 ~ 360) horiz-left:0 clock wise
	SB_ATT_EANGLE	     		=	0x3FE00000,       //circle end angle (0 ~ 360) horiz-left:0 clock wise
	SB_ATT_TRDIFF			=	0x40000000,       //for circle touch only inside radius diff 	
};

enum ESeekBarStyle
{
	SB_ATT_STYLE_HLR	     		=	0x0000,   //horiz left->right
	SB_ATT_STYLE_HRL     		=	0x0001,	//horiz right->left
	SB_ATT_STYLE_VBT     		=	0x0002,  //vertial bottom->top
	SB_ATT_STYLE_VTB     		=	0x0003,  //vertial top->bottom
	SB_ATT_STYLE_CCW     		=	0x0004,  //circle clock wise
	SB_ATT_STYLE_CACW     		=	0x0005,  //circle anti-clock wise
	SB_ATT_STYLE_CURTAIN		=     0x0006, //circle curtain mode
};

#define SB_ATT1_ORADIUS_OFF		0
enum ESeekBarAtt1
{
	SB_ATT1_ORADIUS	     =	0x0000FFFF,       //outside radius mask
};

//seekbar default value
#define SB_CST_D_SBFC              0;//COLOR_LIGHTBLUE|0xFF<<24

#define SB_KEY_S_OBJECT              "sb"
#define SB_KEY_I_SBDMV		"sbdmv"	    //seekbar def value(High 16 bits)+max value(low 16 bits)
#define SB_KEY_S_SBFIMG		"sbfimg"      //seekbar fill image
#define SB_KEY_I_SBFCLR		"sbfclr"       //seekbar fill color
#define SB_KEY_I_SBATT			"sbatt"    	  //seekbar attrib
#define SB_KEY_I_SBATT1		"sbatt1"    //seekbar attrib

//meter
#define MT_KEY_S_OBJECT              "mt"   	//meter
#define MT_KEY_I_MTOP                 "mtop"   //meter original point coord (low 16 bits) X / (High 16 bits) Y
#define MT_KEY_I_MTIRP                "mtirp"   //meter pointer image rotate point(low 16 bits) X / (High 16 bits) Y

//radio group
#define RG_KEY_S_OBJECT             "rg"   		//radio group
#define RG_KEY_I_OGATT			"rgatt"    	      //rg attrib

#define RG_ATT_INITVAL_OFF		0
enum ERadioGroupAtt
{
	RG_ATT_INITVAL	     		=	0x000000FF,       //initial selected index
};

//image
#define IMG_KEY_S_OBJECT             "img"   		//image
#define IMG_KEY_I_IMGATT               "imgatt"

#define IMG_ATT_IMGTYPE_OFF		0
enum EImgaeAtt
{
	IMG_ATT_IMGTYPE	     	=	0x00000F,		   //image type 
};

//subpage
#define SPG_KEY_S_OBJECT      	"spg"   		//subpage

//canvas
#define CAS_KEY_S_OBJECT      	"cas"   		//canvas

//qrcode
#define QR_KEY_S_OBJECT      	"qr"   		//qrcode
#define QR_KEY_S_QRTXT      		"qrtxt"   		//qrcode content text

//curve window
#define CW_KEY_S_OBJECT      	"cw"   		//curve window
#define CW_KEY_I_CWOFF		"cwoff"        	//cw zero off
#define CW_KEY_I_CWV			"cwv"        	//cw max/min value
#define CW_KEY_I_CWATT		"cwatt"        	//cw attrib

#define CW_ATT_SPCNT_OFF			0
#define CW_ATT_RINTERV_OFF		16
enum ECurveWndAtt
{
	CW_ATT_SPCNT	     		=	0x0000FFFF,       //sample count
	CW_ATT_RINTERV	     	=	0x000F0000,       //refresh interval
};

#define CW_CWV_MINV_OFF			0
#define CW_CWV_MAXV_OFF			16
enum ECurveWndCwv
{
	CW_CWV_MINV    		=	0x0000FFFF,       //min value
	CW_CWV_MAXV	     		=	0xFFFF0000,       //max value
};

#define CW_CWOFF_YOFF_OFF		0
#define CW_CWOFF_XOFF_OFF		16
enum ECurveItemOff
{
	CW_CWOFF_YOFF	     		=	0x0000FFFF,   		//Y OFFSET
	CW_CWOFF_XOFF     			=	0xFFFF0000,			//X OFFSET
};

//curve item
#define CI_KEY_S_OBJECT      		"ci"   	//curve item
#define CI_KEY_I_CIOFF			"cioff"        	//ci zero off
#define CI_KEY_I_CICLR			"ciclr"        	//ci clr
#define CI_KEY_I_CIATT			"ciatt"    	      //ci attrib
#define CI_ATT_CIWIDTH_OFF	0
#define CI_ATT_CIRATIO_OFF		8
#define CI_ATT_CISTYLE_OFF		16
#define CI_ATT_CIAA_OFF		20
enum ECurveItemAtt
{
	CI_ATT_CIWIDTH	     	=	0x000000FF,       //ci line width
	CI_ATT_CIRATIO	     		=	0x0000FF00,       //value ratio
	CI_ATT_CISTYLE	     		=	0x000F0000,       //style
	CI_ATT_CIAA	     		=	0x00100000,       //anti-aliased style
};

enum ECurveItemStyle
{
	CI_ATT_CISTYLE_SINCV	     	=	0x0000,   //sina curve
	CI_ATT_CISTYLE_LINECV     	=	0x0001,	//line curve
};

//mediaplay
#define MP_KEY_S_OBJECT      		"mp"   	//mediaplay

//subpagegroup
#define SPGG_KEY_S_OBJECT 			"spgg"

//windowpage
#define WPG_KEY_S_OBJECT 			"wpg"

//window
#define WND_KEY_S_OBJECT 			"wnd"
#define WND_KEY_I_WNDATT			"wndatt"  //wnd attrib

#define WND_ATT_ROTANG_OFF		    0
#define WND_ATT_XOFF_OFF		    6
#define WND_ATT_YOFF_OFF		    19
enum EWindowAtt
{
    WND_ATT_YOFF	     		=	0xFFF80000,   		//Y OFFSET  13bit
    WND_ATT_XOFF	     		=	0x0007FFA0,   		//X OFFSET  13bit
    WND_ATT_ROTANG	     	    =	0x00000003,         //wnd rotate angle 2bit
};

//============================================
//Put End
//============================================
#define MAX_WIDGET_KEYNAME_LEN		8
//control type
enum{
	WIDGET_TYPE_NULL    = 0,
	WIDGET_TYPE_LALEL,
	WIDGET_TYPE_BUTTON,
	WIDGET_TYPE_SCROLLLIST,
	WIDGET_TYPE_LISTITEM,
	WIDGET_TYPE_CHECK,
	WIDGET_TYPE_EDIT,
	WIDGET_TYPE_SEEKBAR,
	WIDGET_TYPE_METER,
	WIDGET_TYPE_RGROUP,
	WIDGET_TYPE_IMAGE,
	WIDGET_TYPE_SUBPAGE,
	WIDGET_TYPE_CANVAS,
	WIDGET_TYPE_QRCODE,
	WIDGET_TYPE_CURVEWND,
	WIDGET_TYPE_CURVEITEM,
	WIDGET_TYPE_MEDIAPLAY,
	WIDGET_TYPE_SUBPAGEGROUP,
	WIDGET_TYPE_WINDOWPAGE,
	WIDGET_TYPE_WINDOW,
	MAX_WIDGET_TYPE
};

typedef struct _WidgetMapType{
	CHAR	cWdKeyName[MAX_WIDGET_KEYNAME_LEN+1];
	INT 		eWdType;
}WidgetMapType;

CONST  WidgetMapType gWidgetMapArray[MAX_WIDGET_TYPE] =
{
	{LBL_KEY_S_OBJECT, 	WIDGET_TYPE_LALEL},
	{BTN_KEY_S_OBJECT, 	WIDGET_TYPE_BUTTON},
	{SCL_KEY_S_OBJECT, 	WIDGET_TYPE_SCROLLLIST},
	{LI_KEY_S_OBJECT, 	WIDGET_TYPE_LISTITEM},
	{ED_KEY_S_OBJECT, 	WIDGET_TYPE_EDIT},
	{SB_KEY_S_OBJECT, 	WIDGET_TYPE_SEEKBAR},
	{MT_KEY_S_OBJECT,	WIDGET_TYPE_METER},
	{RG_KEY_S_OBJECT,	WIDGET_TYPE_RGROUP},
	{IMG_KEY_S_OBJECT,	WIDGET_TYPE_IMAGE},
	{SPG_KEY_S_OBJECT,	WIDGET_TYPE_SUBPAGE},
	{CAS_KEY_S_OBJECT,	WIDGET_TYPE_CANVAS},
	{QR_KEY_S_OBJECT,	WIDGET_TYPE_QRCODE},
	{CW_KEY_S_OBJECT,	WIDGET_TYPE_CURVEWND},
	{CI_KEY_S_OBJECT,	WIDGET_TYPE_CURVEITEM},
	{MP_KEY_S_OBJECT,	WIDGET_TYPE_MEDIAPLAY},
	{SPGG_KEY_S_OBJECT,	WIDGET_TYPE_SUBPAGEGROUP},
	{WPG_KEY_S_OBJECT,	WIDGET_TYPE_WINDOWPAGE},
	{WND_KEY_S_OBJECT,	WIDGET_TYPE_WINDOW},
};
//============================================

#endif //__CONTROL_DEF_H__