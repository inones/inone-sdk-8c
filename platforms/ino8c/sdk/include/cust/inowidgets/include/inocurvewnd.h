/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOCURVEWND_H__
#define __INOCURVEWND_H__

#include "container.h"

namespace INONEGUI_FRAMEWORK {

class api_out InoCurveWnd : public Container
{
public:
	InoCurveWnd();
	virtual ~InoCurveWnd();
	INT GetControlType();
	VOID SetAttribute(STRING strName, DWORD iValue);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);


	CONST UINT8& GetCurveWndRefreshInterval();
	CONST FLOAT& GetCurveWndXStep();
	CONST FLOAT& GetCurveWndYStep();
	CONST SINT16& GetCurveWndXoff();
	CONST SINT16& GetCurveWndYoff();
	CONST UINT& GetCurveWndFrameIndex();
	CONST UINT16& GetCurveWndSampleCnt();
	CONST INT& GetCurveMaxValue();
	CONST INT& GetCurveMinValue();

private:
	VOID InitCurveDisplayParam();
	INT OnTimer();
	
private: 
	UINT mCwCurSp;
	UINT mCwTickCnt;
	UINT8 mCwRefreshInterv;    //Unit:50ms interval
	UINT16 mCwSpCnt;
	FLOAT mCwYStep;
	FLOAT mCwXStep;
	SINT16 mCwZeroXOff;
	SINT16 mCwZeroYOff;
	INT mCwMaxV;
	INT mCwMinV;
};

}//end namespace
#endif	//__INOCURVEWND_H__


