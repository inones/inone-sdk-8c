/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOSUBPAGEGROUP_H__
#define __INOSUBPAGEGROUP_H__

#include "inogui.h"
#include "inoscrolllist.h"

namespace INONEGUI_FRAMEWORK
{
	class api_out InoSubPageGroup : public InoScrollList
	{
	private:


	public:
		InoSubPageGroup();
		virtual ~InoSubPageGroup();

		virtual LRESULT OnEvent(UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, DWORD dwTime = NULL);
		virtual INT OnDraw(PHDC pHDc, RECT stArea, LPVOID pResAdapter = NULL, INT iFlag = CTRL_DRAWFLAG_ALL);
		INT GetControlType();
	};

} //end namespace
#endif //__INOSUBPAGEGROUP_H__
