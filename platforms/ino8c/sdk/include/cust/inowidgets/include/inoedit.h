/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOEDIT_H__
#define __INOEDIT_H__

#include "inogui.h"
#include "inolabel.h"

namespace INONEGUI_FRAMEWORK {

class api_out InoEdit : public InoLabel
{
public:
        InoEdit();
        virtual ~InoEdit();
        INT GetControlType();
        VOID SetAttribute(STRING strName, STRING strValue);
        VOID SetAttribute(STRING strName, DWORD iValue);
        virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);

private:
        STATIC INT ParserEditAttParam(InoEdit *pEdit, DWORD iValue);
protected:
	 
private:
        STRING mEditHint;
        STRING mEditInput;
        //edatt-S
        CHAR mAttRepPwdChar; 
        UINT8 mAttCharType;
        BOOL  mAttIsPwd;
        //edatt-E
};

}//end namespace
#endif	//__InoEdit_H__
