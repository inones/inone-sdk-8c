/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOIMAGE_H__
#define __INOIMAGE_H__

//#include "inogui.h"
#include "inolabel.h"

namespace INONEGUI_FRAMEWORK {

enum EImgAttrib
{
	IMG_PATT_NOTHING         = 0x0000,       //nothing
	IMG_PATT_INITIALOK       = 0x0001,         //image inital ok
};

typedef enum _EImageType
{
	IMG_TYPE_SPIC = 0,		 	//static png / jpg	
	IMG_TYPE_GIF,				//gif
}EImageType;

class api_out InoImage : public InoLabel
{
public:
	InoImage();
	virtual ~InoImage();
	INT GetControlType();
	virtual VOID SetAttribute(STRING strName, STRING strValue);
	virtual VOID SetAttribute(STRING strName, DWORD iValue);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
private:
	InoAniHandleType* mpAniHandle;
	EImageType mImgType;
};

}//end namespace
#endif	//__INOIMAGE_H__