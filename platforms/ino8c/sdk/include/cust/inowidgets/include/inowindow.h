/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOWINDOW_H__
#define __INOWINDOW_H__
#include "inogui.h"
#include "log.h"
#include "container.h"

namespace INONEGUI_FRAMEWORK {

//InoWindow attr bit 
typedef enum EWndPrivAtt{
    WND_PATT_NONE     = 0x00,    //none
    WND_PATT_INITIALOK  = 0x01, 
    WND_PATT_RDST     = 0x02,    //wnd render status
    WND_PATT_ROT      = 0x04,    //wnd rotate enable
}EWndPrivAttType;

#define ClrWndPrivAttAll()               	  	    CLRBIT_ALL(mWndPrivAtt)
#define SetWndPrivAttValue(iBitValue)               SETBIT_ALL(mWndPrivAtt,iBitValue)
#define ClrWndPrivAttByBit(iBitValue)               CLRBIT_BY_BIT(mWndPrivAtt,iBitValue)
#define SetWndPrivAttByBit(iBitValue)               SETBIT_BY_BIT(mWndPrivAtt,iBitValue)
#define IsSetWndPrivAttByBit(iBitValue)             ISSET_BY_BIT(mWndPrivAtt,iBitValue)

class api_out InoWindow:public Container{
public:
	InoWindow();
	virtual ~InoWindow();
	INT GetControlType();
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = 0);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL, INT iFlag = CTRL_DRAWFLAG_ALL);
	virtual VOID SetAttribute(STRING strName, STRING strValue);
	virtual VOID SetAttribute(STRING strName, DWORD iValue);
    INT AddRefreshCtrl(InoControl *pCtrl = NULL, BOOL bNowPaint = FALSE, BOOL bSkipCheck = FALSE);
    INT AddRefreshArea(RECT &stArea, BOOL bNowPaint = FALSE, BOOL bSkipCheck = FALSE); 
    BOOL IsWndRotate();
    RECT GetWndRotRect();
    
private:
    VOID DoWindowInit();
    INT RenderWindow();
    VOID SetWndChildAttrib(INT iBit);
    
private:
    LTGUI_SURFACE   *mpWndDc;
    LTGUI_MUTEX	    *mpRdMutex;
    UINT8           mWndAngle;
    RECT            mWndRotRect;  //wnd rotate wnd rect
    UINT8           mWndPrivAtt;
};

}
#endif //__INOWINDOW_H__
