/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOSEEKBAR_H__
#define __INOSEEKBAR_H__

#include "inogui.h"
#include "inobutton.h"

namespace INONEGUI_FRAMEWORK {

enum ESeekBarPrivAtt
{
	SB_PATT_INITIALOK          	= 0x0001,         //sb inital ok
	SB_PATT_MOVING			= 0x0002,
	SB_PATT_BARDC				= 0x0004,        //bar dc ready
};
#define MAX_CIRCLE_ANGLE            (360+1)

typedef struct _SbDataType{
	UINT16 iSbMaxValue;
	UINT16 iSbCurValue;
	_SbDataType(){
		iSbMaxValue = 1;
		iSbCurValue = 0;
	};
}SbDataType;

#define ClrSbPrivAttAll()               	  	    CLRBIT_ALL(mSbPrivAtt)
#define SetSbPrivAttValue(iBitValue)           SETBIT_ALL(mSbPrivAtt,iBitValue)
#define ClrSbPrivAttByBit(iBitValue)            CLRBIT_BY_BIT(mSbPrivAtt,iBitValue)
#define SetSbPrivAttByBit(iBitValue)            SETBIT_BY_BIT(mSbPrivAtt,iBitValue)
#define IsSetSbPrivAttByBit(iBitValue)         ISSET_BY_BIT(mSbPrivAtt,iBitValue)
#define IsSetSbPrivAttByMask(iBitValue)     ISSET_BY_MASK(mSbPrivAtt,iBitValue)

#define GETX(ANGLE,RADIUS)           ((lt_trigo_sin(ANGLE +90) * RADIUS) >> LT_TRIGO_SHIFT)  
#define GETY(ANGLE,RADIUS)           ((lt_trigo_sin(ANGLE ) * RADIUS) >> LT_TRIGO_SHIFT)
//SeekBar listener
class InoSeekBar;
class ISeekBarListener {   
	public:      
		virtual ~ISeekBarListener() { }      
		virtual VOID OnSeekBarChanged(InoSeekBar *pSeekBar, INT iMaxValue, INT iCurValue) = 0;
};

class api_out InoSeekBar : public InoButton
{
public:
	InoSeekBar();
	virtual ~InoSeekBar();
	INT GetControlType();
	VOID SetAttribute(STRING strName, STRING strValue);
	VOID SetAttribute(STRING strName, DWORD iValue);
	VOID SetSeekBarListener(ISeekBarListener *pListener); 
	VOID SetSeekBarValue(INT iCurValue, INT iMaxValue = INVALID_VALUE);
	VOID GetSeekBarValue(INT &iCurValue, INT &iMaxValue);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);

private:
	STATIC INT ParserSeekBarAttParam(InoSeekBar *pSb, DWORD iValue);
	STATIC INT ParserSeekBarAtt1Param(InoSeekBar *pSb, DWORD iValue);
	BOOL UpdateSeekBarThumbPos(UINT16 iSbFillLen);
	BOOL IsInsideTouchPoint(POINT &sPt);
	INT TouchDown( UINT iMsg,WPARAM wParam , LPARAM lParam);
	INT TouchMove( UINT iMsg,WPARAM wParam , LPARAM lParam);
	BOOL CheckMoveIsOK(POINT stPt);
	
protected:
	VOID DoSeekBarInit();
	virtual INT SetSeekBarData(SbDataType *pData,BOOL bUpdateDisVal = TRUE ,BOOL bForce = FALSE);
	VOID SyncSeekBarData();
	VOID DrawSeekBarCurtainThumb(PHDC pHDc, LPVOID pResAdapter,RECT &rSrcRc, RECT &rDstRc,INT iAngle);
	STATIC UINT16 CalcAngleToUserAngle(UINT16 iCalcAngle, UINT16 iStartAngle, UINT16 iEndAngle, UINT16 iMaxUserAngle);
	STATIC UINT16 UserAngleToDrawAngle(UINT16 iAngle, UINT16 iStartAngle){return ((iAngle + iStartAngle)%MAX_CIRCLE_ANGLE);};
	STATIC BOOL IsValidDrawCircleAngle(UINT16 iVerifyV, UINT16 iMinV, UINT16 iMaxV);
	VOID NotifyListener(ECtrActionType eAction, INT iValue = INVALID_VALUE);

protected:
	SbDataType mSbData;
	UINT16 mSbMaxFillLen;
	UINT16 mSbFillOffXY;
	UINT16 mSbFillLen;
	STRING mSbFillImage;
	DWORD  mSbFillColor;  
	
	//For Circle Var
	POINT   mCircleCPt;         //releation center pt for Circle
	UINT16 mOutRadius;
	UINT16 mInRadius;
	
	//sbatt-S
	UINT8 mAttStyle;
	UINT8 mAttRadDiff;
	UINT16 mAttStartAngle;
	UINT16 mAttEndAngle;
	//sbatt-E
	
	//sbatt1-S
	//UINT16 mAtt1OutRadius;
	//sbatt1-E
	
	UINT8 mSbPrivAtt;

	LTGUI_SURFACE* mpBarDc;
	LTGUI_SURFACE* mpBlendBarDc;
	LTGUI_SURFACE* mpMaskDc;
	
private:	
	//listener
	ISeekBarListener *mpSbListener;
};

}//end namespace
#endif	//__INOSEEKBAR_H__

