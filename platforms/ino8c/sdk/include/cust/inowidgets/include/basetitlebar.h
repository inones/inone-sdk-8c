/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __BASETITLEBAR_H__
#define __BASETITLEBAR_H__

#include "inogui.h"
#include "basewindow.h"
#include "inowidget_header.h"
#include "timermanage.h"
#include "log.h"
#include "layer.h"

namespace INONEGUI_FRAMEWORK {
class api_out BaseTitlebar:public BaseWindow,                           
                                                    public IClickListener,
                                                    public ILongClickListener,
                                                    public ITimerListener,
                                                    public IWindowListener,
                                                    public ILayerHook{
    public:
        BaseTitlebar();
        BaseTitlebar(STRING strName);
        virtual ~BaseTitlebar(); 
        STRING& GetTitlebarName(){return mTitlebarName;};
        INT InvalidateRectArea(RECT stArea);
        VOID OnTimer();
        INT GoBack();
        INT GoHome();
        virtual VOID OnClick(InoControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
        virtual VOID OnLongClick(InoControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
	//IWindowListener
	STRING GetWindowName();
	HWND GetWindowHandle(); 
        LRESULT OnDerivedWndProc(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        //UserApp interface
        virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL) = 0;
        virtual STRING GetLayoutByID(INT iLayoutID) = 0;
        virtual INT OnCreate(Page *pPage) = 0;
        virtual INT OnStart(Page *pPage) = 0;
        virtual INT OnStop(Page *pPage) = 0;
        virtual INT OnDestroy(Page *pPage) = 0;
         //HOOK Callback Start
        INT PreOnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        INT OnDraw(PHDC pHDc, RECT rtPaint); 
        STRING GetName();  
        INT GetLayerPrior(); 
        RECT* GetLayerRect();
        BOOL SetLayerStatus(ELayerFlagType eFlag, BOOL bSet);
        BOOL CheckLayerStatus(ELayerFlagType eFlag);
        //HOOK Callback End          
    private:
        INT CreatePage(INT iLayoutID);
        INT DestroyPage(INT iLayoutID); 
        VOID SetTitlebarStatus(BOOL IsShow);
        INT Stop();
        INT Start();
        INT Create(LPCTSTR strName);
        INT Destroy();
        
    private:
        STRING mTitlebarName;
        INT mLayerStatus;
        INT mLayerHandle;
        Page *mTitlebarPage;
        LTGUI_MUTEX *mpMutex;
};

}
#endif //__BASETITLEBAR_H__

