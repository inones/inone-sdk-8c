/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOSCROLLLIST_H__
#define __INOSCROLLLIST_H__
#include "inogui.h"
#include "log.h"
#include "container.h"
#include "inolistitem.h"
#include "objmove.h"
#include "inoscrollbar.h"

namespace INONEGUI_FRAMEWORK {
class InoScrollList;

#define ClrSclAttAll()               	  		   CLRBIT_ALL(mSclAtt)
#define SetSclAttValue(iBitValue)                SETBIT_ALL(mSclAtt,iBitValue)
#define ClrSclAttByBit(iBitValue)                  CLRBIT_BY_BIT(mSclAtt,iBitValue)
#define SetSclAttByBit(iBitValue)                 SETBIT_BY_BIT(mSclAtt,iBitValue)
#define IsSetSclAttByBit(iBitValue)              ISSET_BY_BIT(mSclAtt,iBitValue)
#define IsSetSclAttByMask(iBitValue)          ISSET_BY_MASK(mSclAtt,iBitValue)
	
#define ClrSclPrivAttAll()               	  	    CLRBIT_ALL(mSclPrivAtt)
#define SetSclPrivAttValue(iBitValue)           SETBIT_ALL(mSclPrivAtt,iBitValue)
#define ClrSclPrivAttByBit(iBitValue)            CLRBIT_BY_BIT(mSclPrivAtt,iBitValue)
#define SetSclPrivAttByBit(iBitValue)            SETBIT_BY_BIT(mSclPrivAtt,iBitValue)
#define IsSetSclPrivAttByBit(iBitValue)         ISSET_BY_BIT(mSclPrivAtt,iBitValue)
#define IsSetSclPrivAttByMask(iBitValue)     ISSET_BY_MASK(mSclPrivAtt,iBitValue)

class IScrollListListener {
public:
        virtual ~IScrollListListener() { }
        virtual INT GetListItemCount(InoScrollList *pScrollList)  = 0;
        virtual INT BuildListItemData(InoScrollList *pScrollList, InoListItem *pListItem, INT iIndex) = 0;
        virtual VOID NotifySelectedListItem(InoScrollList *pScrollList, InoListItem *pListItem, INT iIndex) = 0;
        /**
        *  @brief UserNotifyDataChanged
        *             if pListItem != null, reload this listitem
        *             else reload this scrolllist
        *  @param iFocusIndex, -1: auto calc focus by pos; other valid value,find item by index 
        *  @return focus item or null
        *  @details More details
        */
        VOID UserNotifyDataChanged(InoScrollList *pScrollList, InoListItem *pListItem = NULL, INT iSelectedIndex = 0) ;
};

//scrolllist private attrib
#define _SL_KEY_I_SMALLLIST      "SRLIST"   //for small  list < colum or row, don't scroll

#define LISTITEMVIEW_MULTI       3 //max cached item(row*colum*multi)
#define CYCLELIST_DEFAUT_POS	0

//scroll acc mode const value
#define MIN_DIST_MOVE_TH                10
#define MIN_DIST_SCROLLACC_TH       50
#define MAX_TIME_SCROLLACC_TH       500
#define MIN_DIST_MOVING_TH       	10
#define MAX_SPEED_MULTI       		2	//for listw/h

#define DISPINDEX_INVALID                 0xFF
#define MAX_DISPINDEX_NUM              128

enum EScrollListPrivAtt
{
        SCL_PATT_INITIALOK 	= 0x01,
        SCL_PATT_MOVING		= 0x02,
        SCL_PATT_CNTZERO		= 0x04,
        SCL_PATT_SRLIST		= 0x08,            //for small roller list < colum or row, don't roller
        SCL_PATT_MOVING_RD	= 0x10,            //for moving dir,right /down
        SCL_PATT_DRAG               = 0x20,            //for drag mode
};

typedef struct _VarLenListItemInfo
{
        UINT16 iIndex;
        RECT stRect;
}VarLenListItemInfo;

typedef vector<VarLenListItemInfo*>					VTVLListItemInfo;
typedef vector<VarLenListItemInfo*>::iterator			IterVTVLListItemInfo;
typedef list<InoControl*>::iterator						IterListControl;
typedef vector<InoListItem*>::iterator					IterVectorListItem;

class api_out InoScrollList:public Container{
public:
        InoScrollList();
        virtual ~InoScrollList();
        LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
        VOID SetScrollListListener(IScrollListListener *pListener);
        INT SetScrollListIndex(INT iFirstIndex);
        INT SetScrollListPageIndex(INT iPageIndex);
        INT GetScrollListPageIndex(INT &iPageIndex, INT &iPageTotal);
        INT SetScrollListDragMode(BOOL bDrag);
        BOOL NextPage();
        BOOL PrevPage();
#if D_USE_JSONCPP  
        virtual INT SetContAttribute(Json::Value v);
#elif D_USE_CJSON
        VOID SetAttribute(STRING strName, STRING strValue);
        VOID SetAttribute(STRING strName, DWORD iValue);
#endif
        friend VOID IScrollListListener::UserNotifyDataChanged(InoScrollList *pScrollList, InoListItem *pListItem, INT iSelectedIndex) ;

private:
        INT InitViewListItemRect(INT Index, RECT &rRect);
        VOID ReleaseListItemInfo();
        INT ReInitViewListItemRect();
        VOID UpdateListItemRect(InoListItem *pCurItem,InoListItem *pPrevItem);
        INT ConfigScrollListParam();
        INT DoScrollListInit();
        INT DoScrollListStaticInit();
        INT DoScrollListReInit(INT iSelectedIndex = 0);
        BOOL SetScrollListItemData(InoListItem *pListItem,INT iIndex, BOOL bReset = TRUE);
        INT SetScrollListInternalIndex(INT iFirstIndex, BOOL bCachedMove = TRUE);
        /**
        *  @brief FindScrollListFocusItem
        *             For roller list, odd item return center item; even item return first item
        *             For normal list, return first item  
        *  @param iFocusIndex, -1: auto calc focus by pos; other valid value,find item by index 
        *  @return focus item or null
        *  @details More details
        */
        InoListItem* FindScrollListFocusItem(INT iFocusIndex = INVALID_VALUE);
        InoListItem* FindListItemForViewlistByIndex(INT iIndex);
        vector<InoListItem*> FindListItemVtForViewlistByIndex(INT iIndex);
        BOOL IsCachedItemViewForIndex(INT iIndex);
        BOOL NotifyScrollListSelectedItem(INT iFocusIndex = INVALID_VALUE, BOOL bForce = TRUE);
        BOOL CheckMoveIsOK(INT iDx, INT iDy);
        VOID CheckListItemViewCacheForNormal(INT iScrollPos);
        VOID CheckListItemViewCacheForRoller(void);
        INT DoScrollListViewDataReInit(INOUT INT &iFirstIndex,OUT INT &iFirstPos);
        INT TouchMove( UINT iMsg,WPARAM wParam , LPARAM lParam);
        INT TouchDown(UINT iMsg,WPARAM wParam , LPARAM lParam);
        INT TouchUp(UINT iMsg,WPARAM wParam , LPARAM lParam);

        VOID StartMove(VOID);
        VOID StopMove(VOID);
        INT OnAnimation(void);
        BOOL CheckListItemPos(void);
        INT ScrollAcc2EndByV(INT iSpeed, BOOL bMovPrev);
        STATIC UINT ScrollAccEnded(VOID* pObj, VOID* pParam);
        STATIC UINT AutoResetEnded(VOID* pObj, VOID* pParam);
        STATIC UINT RedrawCallback(VOID* pObj, VOID* pParam);
        STATIC INT ParserScrollListAttParam(InoScrollList *pScrolllist, DWORD iValue);
        STATIC INT ParserScrollListAtt1Param(InoScrollList *pScrolllist, DWORD iValue); 
        BOOL CheckWhenScrollAccEnded(VOID);
        VOID AutoResetToListEdge(BOOL bToStart);
        BOOL InitialListItemInfo(INT iSelIndex);
        BOOL InitialScrollbarInfo(INT iSelIndex);
        BOOL InitialIndexToScrollPos(INT iReqDataIndex, INT &iActBestIndex, INT &iActBestPos);
        INT CalcBestFirstCachedIndexForFirstShow(IN INT iFirstShowIndex);
        INT IndexToScrollPos(INT iDataIndex);
        INT ScrollPosToIndex(INT iScrollPos);
        
        //Drag mode
        STATIC BOOL CompDispIndex(InoControl *pCtrla, InoControl *pCtrlb);
        STATIC INT SortListItemIndexOnList(list<InoControl*> &pList);
        STATIC UINT DragToEnded(VOID* pObj, VOID* pParam);
        STATIC UINT DragRedrawCallback(VOID* pObj, VOID* pParam);
        STATIC INT SetDispIndexKeyValue(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj, STRING strKeyName);
        INT DragToExchagePos(InoControl *pExObj, POINT *pNewPt);
        InoControl *GetDragExchangeObj(InoControl *pDragObj, INT iExFactor);
        INT OnDragAction(POINT &rPt);
        INT SaveScrollListDispIndex();
        INT ReadScrollListDispIndex();
private:
        INT	mMinPageEdge;
        INT	mMaxPageEdge;
        INT mMaxPageEnd; //don't include mDisNoPage
        SINT16 mDisNoPage;
        UINT16 mCntPerPage;
        UINT16 mPageTotal;
        UINT16 mListWH;

        UINT8 mListColums;
        UINT8 mListRows;
        UINT8 mListItemCGap;
        UINT8 mListItemRGap;
        UINT16 mListItemW;
        UINT16 mListItemH;
        UINT16 mListItemViewCount;
        UINT mListDataCount;

        BOOL		mIsMoving;
        CObjMove	mObjMov;

        InoListItem *mpListItem;
        //InoListItem *mpSelectedListItem;
        list<InoControl*> mListItemViewlist;

        UINT mReqSelectedDataIndex;
        UINT mSelectedDataIndex;

        InoScrollBar *mpScrollBar;
        STRING mSBImg;
        DWORD mSBColor;
        RECT mSBAbsRect;
        UINT8 mScrollbarWH;
        VTVLListItemInfo mListItemInfolist;

        IScrollListListener *mpScrollListListener;
        UINT32	mSclAtt;
        UINT8	mSclPrivAtt;

        //Drag Mode
        POINT mDragOrgPt; //drag orginal point
        InoControl *mpDragControl;
        InoControl *mpExchControl;
        INT mExchDispIndex;
        UINT32 mDragPageTick;
        UINT8 mItemDispArray[MAX_DISPINDEX_NUM];
};

}
#endif //__INOSCROLLLIST_H__

