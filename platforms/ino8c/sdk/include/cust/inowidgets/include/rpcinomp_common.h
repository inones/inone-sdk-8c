/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __RPCINOMP_COMMON_H__
#define __RPCINOMP_COMMON_H__

#define MP_SERVERNAME       "inomediaplay"
#define MP_NOTIFYNAME       "inompnotify"
#define UBUS_SOCK           "/var/ubus.sock"
#define MP_MSGQ           	"/var/mp.msgq"
#define MP_MSGQ_ID          88
#define MSGQ_TYPE_MPCB      1

typedef struct _InoMpNotifyMsg{
	long type;
    int msg;
    int param0;
    int param1;
}InoMpNotifyMsg;

//Default_policy
static const struct blobmsg_policy Default_policy[] = {
};

//InoMpNotify
enum {
	NOTIFY_MSG,
	NOTIFY_PARAM0,
	NOTIFY_PARAM1,
	__NOTIFY_MAX
};

static const struct blobmsg_policy InoMpNotify_policy[] = {
	[NOTIFY_MSG] = { .name = "msg", .type = BLOBMSG_TYPE_INT32 },
    [NOTIFY_PARAM0] = { .name = "param0", .type = BLOBMSG_TYPE_INT32 },
    [NOTIFY_PARAM1] = { .name = "param1", .type = BLOBMSG_TYPE_INT32 },
};

//InoMpDoInit
enum {
	DOINIT_X,
	DOINIT_Y,
    DOINIT_W,
    DOINIT_H,
	__DOINIT_MAX
};

static const struct blobmsg_policy InoMpDoInit_policy[] = {
	[DOINIT_X] = { .name = "x", .type = BLOBMSG_TYPE_INT32 },
    [DOINIT_Y] = { .name = "y", .type = BLOBMSG_TYPE_INT32 },
    [DOINIT_W] = { .name = "w", .type = BLOBMSG_TYPE_INT32 },
    [DOINIT_H] = { .name = "h", .type = BLOBMSG_TYPE_INT32 },
};

//InoMpSetDisplayRect
enum {
	SETDISPLAYRECT_X,
	SETDISPLAYRECT_Y,
    SETDISPLAYRECT_W,
    SETDISPLAYRECT_H,
	__SETDISPLAYRECT_MAX
};

static const struct blobmsg_policy InoMpSetDisplayRect_policy[] = {
	[SETDISPLAYRECT_X] = { .name = "x", .type = BLOBMSG_TYPE_INT32 },
    [SETDISPLAYRECT_Y] = { .name = "y", .type = BLOBMSG_TYPE_INT32 },
    [SETDISPLAYRECT_W] = { .name = "w", .type = BLOBMSG_TYPE_INT32 },
    [SETDISPLAYRECT_H] = { .name = "h", .type = BLOBMSG_TYPE_INT32 },
};

//InoMpSetDataSource
enum {
	SETDATASOURCE_DATAPATH,
	__SETDATASOURCE_MAX
};

static const struct blobmsg_policy InoMpSetDataSource_policy[] = {
	[SETDATASOURCE_DATAPATH] = { .name = "datapath", .type = BLOBMSG_TYPE_STRING },
};

//InoMpSetLooping
enum {
	SETLOOPING_LOOP,
	__SETLOOPING_MAX
};

static const struct blobmsg_policy InoMpSetLooping_policy[] = {
	[SETLOOPING_LOOP] = { .name = "loop", .type = BLOBMSG_TYPE_BOOL },
};

//InoMpSeekTo
enum {
	SEEKTO_TIMEMS,
	__SEEKTO_MAX
};

static const struct blobmsg_policy InoMpSeekTo_policy[] = {
	[SEEKTO_TIMEMS] = { .name = "timems", .type = BLOBMSG_TYPE_INT32 },
};

//InoMpSetSpeed
enum {
	SETSPEED_SPEED,
	__SETSPEED_MAX
};

static const struct blobmsg_policy InoMpSetSpeed_policy[] = {
	[SETSPEED_SPEED] = { .name = "speed", .type = BLOBMSG_TYPE_INT8 },
};

//InoMpSetVolume
enum {
	SETVOLUME_VOLUME,
	__SETVOLUME_MAX
};

static const struct blobmsg_policy InoMpSetVolume_policy[] = {
	[SETVOLUME_VOLUME] = { .name = "volume", .type = BLOBMSG_TYPE_INT32 },
};

//InoMpSetAudioMute
enum {
	SETAUDIOMUTE_MUTE,
	__SETAUDIOMUTE_MAX
};

static const struct blobmsg_policy InoMpSetAudioMute_policy[] = {
	[SETAUDIOMUTE_MUTE] = { .name = "mute", .type = BLOBMSG_TYPE_INT8 },
};

//common return
enum {
	RETURN_CODE,
	__RETURN_MAX,
};

static const struct blobmsg_policy return_policy[] = {
	[RETURN_CODE] = { .name = "rc", .type = BLOBMSG_TYPE_INT32 },
};

#endif //__RPCINOMP_COMMON_H__