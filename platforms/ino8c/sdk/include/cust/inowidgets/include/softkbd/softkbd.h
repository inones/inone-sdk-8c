/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __SOFTKBD_H__
#define __SOFTKBD_H__

#include "inowidget_header_min.h"

#define SCANCODE_TONUM   SCANCODE_USER + 1
#define SCANCODE_TOPY       SCANCODE_USER + 2
#define SCANCODE_TOEN       SCANCODE_USER + 3
#define SCANCODE_TOOP       SCANCODE_USER + 4

enum _KeyStyle{
	KEY_PAD_CHAR = 0,	/* indicate that this key is normal character key */
	KEY_PAD_FUNC,	/* indicate that this key is functional key */
	MAX_KEY_STYPE
};

typedef enum _KBDStyle{
	EN_KBD = 0,
	NUM_KBD,
	PINYIN_KBD,
	PUNCT_KBD,
	MAX_KBD
}KBDStyle;

typedef struct _KeyMapType {

    /* the corrosponding rect of the key pad */
    STRING strKeyName;

    /* the corresponging character of this key pad
     * such as 'a','b' etc
     */
    CHAR cKeyChar;

    /* the correcponging scancode of this key pad.
     * such as SCANCODE_F1~F3,BACKSPACE,ENTER,UP,DOWN,LEFT,RIGHT...
     */
    INT iScanCode;

    /* the style of this key pad.
     * KEY_CHAR indaces the keypad is a character key pad
     * KEY_FUNC indaces the keypad is a functional key pad
     */
    INT iStyle;
} KeyMapType;


typedef struct _SoftKBDMapType{

    STRING strSoftKBDName;

    /* pointer to keys[] */
    KBDStyle eKBDStyle;
	
    /* pointer to keys[] */
    KeyMapType* pKeyMap;

    /* the number of the keys[] */
    UINT8 iKeyNum;

} SoftKBDMapType;

typedef SoftKBDMapType* PSoftKBDMapType;
typedef KeyMapType* PKeyMapType;

#define AC_NULL             0
#define AC_SEND_MSG         1
#define AC_SEND_EN_STRING   2
#define AC_SEND_CN_STRING   3
#define AC_CHANGE_KBD       4


#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

INT  InitEnKeyboard(_SoftKBDMapType *pSoftKBDMap);
INT  InitNumKeyboard(SoftKBDMapType *pKeyBD);
INT  InitPinyinKeyboard(SoftKBDMapType *pKeyBD);
INT  InitPunctKeyboard(SoftKBDMapType *pKeyBD);
#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif // __SOFTKBD_H__

