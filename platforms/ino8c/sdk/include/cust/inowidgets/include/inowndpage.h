/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOWNDPAGE_H__
#define __INOWNDPAGE_H__
#include "inogui.h"
#include "log.h"
#include "container.h"

namespace INONEGUI_FRAMEWORK {
class Page;
class BaseActivity;
class api_out InoWndPage:public Container{
public:
	InoWndPage();
	virtual ~InoWndPage();
	INT GetControlType();
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = 0);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL, INT iFlag = CTRL_DRAWFLAG_ALL);
	virtual VOID SetAttribute(STRING strName, STRING strValue);
	virtual VOID SetAttribute(STRING strName, DWORD iValue);
	VOID SetDefaultLayoutId(INT iLayoutId);
	INT LoadPageById(INT iLayoutId, LPVOID pParam = NULL);
	INT LoadPageByName(STRING strLayoutName, LPVOID pParam = NULL);
	Page *GetLoadPage();

private:
	INT Stop(Page *pPage);
	INT Start(Page *pPage);
	
private:
	INT 			mActLayoutId;
	Page * 		mpPage;
	BaseActivity*	mpActivityContext;
	LPVOID	       mpPageParam;

private:
};

}
#endif //__INOWNDPAGE_H__

