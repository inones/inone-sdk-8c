/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __BASEWINDOW_H__
#define __BASEWINDOW_H__
#include "inogui.h"
#include "window.h"
#include "page.h"
#include "log.h"

namespace INONEGUI_FRAMEWORK {

class api_out BaseWindow:public Window{
public:
        BaseWindow();
        BaseWindow(BaseWindow* pParam);
        virtual ~BaseWindow();
        INT PageFactory(Page **ppPage, LPCTSTR pName, LPCTSTR lpRes, LPCTSTR pJson);
        INT PageRelease(LPCTSTR pName);
        INT PageObjRelease(Page *pPage);
        INT AutoPageRelease(Page **ppPage);
        friend class InoWndPage;
        
private:
        LPVOID FindPage(LPCTSTR pName);

private:
        STATIC CPtrArray mPageList;
        STATIC LTGUI_MUTEX *mpPageListMutex;
};

}
#endif //__WINDOW_H__