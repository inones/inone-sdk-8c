/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOSCROLLBAR_H__
#define __INOSCROLLBAR_H__
#include "inogui.h"
#include "log.h"
#include "container.h"

namespace INONEGUI_FRAMEWORK {

enum EScrollBarType
{
	SCROLLBAR_DIR_LR,
	SCROLLBAR_DIR_TB,
};

#define SB_MIN_WH            50
typedef struct _tag_ScrollbarInfo
{
	EScrollBarType	eScrollbarType;
       DWORD   iSBColor;
       STRING   strSBImg;
       RECT       sSBRect;
	INT		iCurIndex;
	INT		iTotalCnt;	
	INT		iPageCnt;
       LPVOID    pResAdapter;
       INT          iTimeCntForHideBar;
    
	_tag_ScrollbarInfo()
	{
		eScrollbarType      = SCROLLBAR_DIR_TB;
		iSBColor			= MAKERGBA(0xff,0xff, 0, 0xff);
               sSBRect                 = {0};
		strSBImg		        = "";
		iCurIndex		= 0;
		iTotalCnt		        = 0;
               pResAdapter          = NULL;
               iTimeCntForHideBar = MS2TICK(2000); 
		iPageCnt	                = 0;
        
	}
}ScrollbarInfoType;

class api_out InoScrollBar{
    public:
        InoScrollBar();
       virtual ~InoScrollBar();
       
	BOOL Init(ScrollbarInfoType& sInfo);
	VOID SetScrollbarPos(INT iCurIndex);
	INT OnAnimation(void);
       virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
	VOID Unit(VOID);
       VOID Show(BOOL bShow);
       VOID SetAlpha(void);
       VOID ResetAlpha(void);
       
    private:
        INT  mShowTimeCnt;
        ScrollbarInfoType mSBInfo;
        RECT mDrawAbsRect;
        LTGUI_SURFACE *mpSBDc;
        BOOL     mShow;
        BYTE     mAlphaPercent;
       
    private:
        LPVOID mResHandle;
};

}
#endif //__INOSCROLLBAR_H__