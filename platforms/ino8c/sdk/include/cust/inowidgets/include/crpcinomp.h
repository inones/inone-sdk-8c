/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __CRPCINOMP_H__
#define __CRPCINOMP_H__

#include "libubus.h"

#ifdef __cplusplus
extern "C" {
#endif
typedef  INT (*InoMpCb)(VOID* pUserData, INT iMsg, INT iParam0, VOID* pParam1);

INT CRpcSetMpCallback(LPVOID pObj, InoMpCb cb);
INT CRpcDoInit();
INT CRpcDeInit();
INT CRpcInoMpDoInit(INT x, INT y, INT w, INT h);
INT CRpcInoMpDeInit();
INT CRpcInoMpSetDataSource(LPCTSTR pDataPath);
INT CRpcInoMpSetDisplayRect(INT x, INT y, INT w, INT h);
INT CRpcInoMpPrepare();
INT CRpcInoMpPrepareAsync();
INT CRpcInoMpStart();
INT CRpcInoMpPause();
INT CRpcInoMpStop();
INT CRpcInoMpReset();
INT CRpcInoMpSetLooping(BOOL bLoop);
BOOL CRpcInoMpIsPlaying();
INT CRpcInoMpGetCurPos(INT* pTimeMs);
INT CRpcInoMpGetDuration(INT* pTimeMs);
INT CRpcInoMpSeekTo(INT iSeekTimeMs);
INT CRpcInoMpSetSpeed(UINT8 iSpeed);
INT CRpcInoMpSetVolume(INT iVolume);
INT CRpcInoMpGetVolume();
INT CRpcInoMpSetAudioMute(BOOL bMute);
#ifdef __cplusplus
}
#endif

#endif //__CRPCINOMP_H__