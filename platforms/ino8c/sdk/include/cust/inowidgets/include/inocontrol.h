/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOCONTROL_H__
#define __INOCONTROL_H__

#include "inogui.h"
#include "log.h"
#include "hwnd.h"
#include "myrender.h"
#include "inopubfun.h"
#include "invalidrectmanage.h"
#include "activitymanage.h"
#include "cursormanage.h"
#include "langparser.h"

namespace INONEGUI_FRAMEWORK {

class InoControl;
class IClickListener {  
public:  
    virtual ~IClickListener() { }  
    virtual VOID OnClick(InoControl *pBase, WPARAM wParam = NULL, LPARAM lParam = NULL) = 0;  
};  

class ILongClickListener { 
public:  
    virtual ~ILongClickListener() { }  
    virtual VOID OnLongClick(InoControl *pBase, WPARAM wParam = NULL, LPARAM lParam = NULL) = 0;  
}; 

class api_out InoControl:public HandleWnd,
						   public IWindowListener
{
public:
        InoControl();
        virtual ~InoControl();
        STRING& GetName();
        INT GetCtrlId(){return mCtrlId;};
        VOID SetName(STRING name);	
        VOID SetCtrlRect(RECT sRect);
        RECT& GetCtrlRect();
        STATIC BOOL GetCtrlCenterPt(InoControl *pCtrl, POINT &rCPt);
        INT IsEnabled();
        VOID SetEnabled(BOOL bEnable);
        BOOL IsVisible();
        VOID SetVisible(BOOL bEnable);
        BOOL IsSelected();
        VOID SetSelected(BOOL bEnable);
        BOOL IsFocused();
        VOID SetFocused(BOOL bEnable);
        BOOL IsDisableOrInvisible();
        VOID SetState(ECtrlStateType state);
        ECtrlStateType GetState();
        INT ClrCtrlAttribByBit(INT iBitValue);
        INT SetCtrlAttribByBit(INT iBitValue);
        BOOL IsSetCtrlAttribByBit(INT iBitValue);
        VOID SetParent(InoControl *pCtrl){mpParent = pCtrl;};
        InoControl* GetParent(){return mpParent;};
        InoControl* GetRootParent(InoControl *pCtrl = NULL);
        VOID SetCtrlPos(POINT sPoint);
        VOID SetCtrlPosX(INT iX);
        VOID SetCtrlPosY(INT iY);
        RECT CtrlRRectToDcARect( InoControl *pCtrl, PHDC pHDc = NULL, RECT *pCtrlRect = NULL);
        BOOL GetReleationRectForDc(PHDC pSrcHDc, CONST RECT &stARect, RECT &stOutRRect, PHDC pRefHDc = NULL );
        BOOL GetCtrlIntersectInfoForDc(PHDC pHDc, InoControl *pSrcCtrl, CONST RECT &stRect, RECT &stOutRect);
        virtual INT GetControlType();
        virtual VOID SetAttribute(STRING strName, STRING strValue);
        virtual VOID SetAttribute(STRING strName, DWORD iValue);
        virtual VOID SetAttribute(STRING strName, cJSON *cJsonV){};
        virtual BOOL IsInsideTouchPoint(POINT &sPt){return FALSE;};
        //set bg interface
        VOID SetBgImage(STRING strName);
        VOID SetBgColor(DWORD iColor);

        //GET
        STRING& GetBgImage();
        DWORD GetBgColor();
        UINT8 GetCtrlPrior(){ return mCtrlPrior;}

        //IWindowListener
        STRING GetWindowName();
        HWND GetWindowHandle(); 
        LRESULT OnDerivedWndProc(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);   

        virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
        VOID SetClickListener(IClickListener *pListener); 
        VOID SetLongClickListener(ILongClickListener *pListener); 
        INT AddRefreshCtrl(InoControl *pCtrl = NULL, BOOL bNowPaint = FALSE, BOOL bSkipCheck = FALSE);
        INT AddRefreshArea(RECT &stArea, BOOL bNowPaint = FALSE, BOOL bSkipCheck = FALSE);
        LPVOID GetResAdapter(){return mHResAdapter;}; 
        VOID SetResAdapter(LPVOID pRes){mHResAdapter = pRes;}; 

        //friend class 
        friend class CtrlAnimManager;
protected:
        INT CtrlPointToOrginPoint(POINT sContPt ,POINT &sOrginPt,InoControl *pCtrl = NULL);
        INT OrginPointToCtrlPoint(POINT sOrginPt,POINT &sCtrlPt,InoControl *pCtrl = NULL);
        BOOL IsInsideTouchEvent(WPARAM wParam);
        VOID NotifyListener(ECtrActionType eAction);

private:
       //Ctrl animator
       VOID SetCtrlAnimator(LPVOID pCtrlAnim);
       
protected:
        STATIC POINT mLastTouchPoint;
        STATIC POINT mLastTouchDownPoint;
        STATIC UINT32 mLastTouchDownTime;

        LPVOID mHResAdapter;
        STRING   mCtrlName;
        INT         mCtrlId;
        DWORD  mBgColor;
        UINT8     mBgRadius;
        UINT8     mCtrlPrior;
        STRING  mBgImg;
        RECTPAD mCtrlRectPad; 
        RECT      mCtrlRect;
        HWND      mpHWnd;
        InoControl *mpParent;

        IClickListener *mpClickListener;
        ILongClickListener *mpLongClickListener;
        
private:
        LPVOID mpCtrlAnim;
        UINT8     mCtrlAttrib;
        UINT8     mCtrlState;
    
};

}//end namespace

#endif	//__INOCONTROL_H__
