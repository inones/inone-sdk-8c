/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __BASEIMEUI_H__
#define __BASEIMEUI_H__

#include "inogui.h"
#include "basewindow.h"
#include "inolabel.h"
#include "timermanage.h"
#include "inoscrolllist.h"
#include "log.h"
#include "layer.h"
#include "ime-pinyin.h"
#include "softkbd/softkbd.h"

namespace INONEGUI_FRAMEWORK {
typedef struct _KeyBoardType {
    SoftKBDMapType stImeKeyLayout;
    Ime *pIme;
    //struct construct
    _KeyBoardType(){
    		stImeKeyLayout.strSoftKBDName = "";
		stImeKeyLayout.eKBDStyle = MAX_KBD;
		stImeKeyLayout.pKeyMap = NULL;
		stImeKeyLayout.iKeyNum = 0;
		pIme = NULL;
    }
} KeyBoardType;

class api_out  BaseIMEUI:public BaseWindow,                           
                                                                    public IClickListener,
                                                                    public ILongClickListener,
                                                                    public ITimerListener,
                                                                    public IWindowListener,
                                                                    public IScrollListListener,
                                                                    public IListItemClickListener,
                                                                    public ILayerHook{
    public:
        BaseIMEUI();
        BaseIMEUI(STRING strName);
        virtual ~BaseIMEUI(); 
	 STATIC BOOL DelLastInputWords(STRING &strWords);
	
        INT InvalidateRectArea(RECT stArea);
        VOID OnTimer();
        virtual VOID OnClick(InoControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
        virtual VOID OnLongClick(InoControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
	
	//IWindowListener
	STRING GetWindowName();
	HWND GetWindowHandle();
        LRESULT OnDerivedWndProc(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        
        //UserApp interface
        virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        virtual STRING GetLayoutByID(INT iLayoutID);
        virtual INT OnCreate(Page *pPage);
        virtual INT OnStart(Page *pPage);
        virtual INT OnStop(Page *pPage);
        virtual INT OnDestroy(Page *pPage);
        
         //HOOK Callback Start
        INT PreOnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        INT OnDraw(PHDC pHDc, RECT rtPaint); 
        STRING GetName();  
        INT GetLayerPrior(); 
        RECT* GetLayerRect();
        BOOL SetLayerStatus(ELayerFlagType eFlag, BOOL bSet);
        BOOL CheckLayerStatus(ELayerFlagType eFlag);
        //HOOK Callback End    


	//SCROLLLIST LISTENER		
	INT GetListItemCount(InoScrollList *pScrollList) ;
	INT BuildListItemData(InoScrollList *pScrollList, InoListItem *pListItem, INT iIndex);
	VOID NotifySelectedListItem(InoScrollList *pScrollList, InoListItem *pListItem, INT iIndex){};
	
	//ListItem listener
	VOID ListItemClick(InoListItem *pListItem, INT iIndex, BOOL bLongClick);
    private:
	BOOL RegisterIMEWindow();
	BOOL UnregisterIMEWindow();
	BOOL SettingKBDAndIME(KBDStyle eSettingKBD);
	VOID UpdateInputWordsFromUser();
        BOOL UpdateSoftKBDDisplayChar(SoftKBDMapType *pKeyBD);
        INT SendTextToTarget(HWND pHWnd,STRING strText);
	 VOID Process_IMEUpdateData(DWORD dwFlag);
	 BOOL SendWordsToInputLbl(STRING strIncWord);
	 STRING TranslateChar(SoftKBDMapType *pKeyBD, KeyMapType *pKeyMap);
	 VOID ResetIMEInput();
	 CHAR* GetIMEInputKey();
	 STATIC KeyMapType* GetKey(SoftKBDMapType *pKeyBD, STRING strKeyName);
        INT CreatePage(INT iLayoutID);
        INT DestroyPage(INT iLayoutID); 
        INT Create(LPCTSTR strName);
        INT Stop();
        INT Start(KBDStyle  eKBDStyle = EN_KBD);
        INT Destroy();
        
    private:
	 STRING mInputWords;
	 HWND mpEditTarget;
	 KeyBoardType *mpActionSoftkb;
	 InoLabel *mpLblStrokeChars;
	 InoLabel *mpLblInputWords;
	 InoScrollList*mpSclWordVW;
	 InoButton *mpLeftShift;
	 InoButton *mpRightShift;
        INT mLayerStatus;
        INT mLayerHandle;
        INT mBoxTimeoutByTimerTick;   //default 50ms
        INT mTimerHandle;
        Page *mpImePage;
        LPVOID mpImeDc;
        LTGUI_MUTEX *mpMutex;
};

#define IMEUI                (BaseIMEUI::GetInstance())
}
#endif //__BASEIMEUI_H__


