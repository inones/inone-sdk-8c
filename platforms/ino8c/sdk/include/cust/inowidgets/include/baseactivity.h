/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __BASEACTIVITY_H__
#define __BASEACTIVITY_H__

#include "inogui.h"
#include "basewindow.h"
#include "timermanage.h"
#include "log.h"
#include "layer.h"
#include "basemessagebox.h"
#include "ctrlanimators/ctrlanimmanager.h"
#include "inowidget_header.h"

namespace INONEGUI_FRAMEWORK {

class BaseActivity;
class IPageCtrl{  
public:  
        virtual ~IPageCtrl() { }  
        virtual VOID OnPageCreate() = 0;
        virtual VOID OnPageDestroy() = 0;
        virtual VOID OnPageStart() = 0;
        virtual VOID OnPageStop() = 0;
        virtual VOID OnPagePreCreate(BaseActivity *pActivity, Page *pPage) = 0;
        virtual VOID OnPageTimer(UINT32 iInterval) = 0 ;
        virtual LRESULT OnPageTouchEvent(UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, DWORD dwTime = NULL) = 0;
        virtual LRESULT OnPageKeyEvent(UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, DWORD dwTime = NULL) = 0;
}; 

class api_out BaseActivity:public BaseWindow,                           
                                                    public IClickListener,
                                                    public ILongClickListener,
                                                    public ITimerListener,
                                                    public IWindowListener,
                                                    public ILayerHook,
                                                    public IUserTimer{
public:
        BaseActivity();
        BaseActivity(STRING strName, EWindowType eWindwType = WINDOW_TYPE_ACTIVITY);
        virtual ~BaseActivity(); 
        STRING& GetActivityName(){return mActivityName;};
        Page* GetFocusPage(){return (Page *)mPageStack.GetAt(0);};
        INT InvalidateRectArea(RECT stArea);
        VOID OnTimer();
        INT GoBack();
        INT GoHome();
        STRING GetResById(INT iResId);
        INT GetFocusPageId();
        virtual VOID OnClick(InoControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
        virtual VOID OnLongClick(InoControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
        //Set Lang
        STATIC INT SetLangId(INT iLangId);
        STATIC INT GetLangId();

        //MessageBox
        STATIC INT MessageBox(LPCTSTR strTitle,LPCTSTR strContent,INT iTimeoutSec = BOX_TIMEOUT_SEC, BOOL IsModal = TRUE);

        //IWindowListener
        STRING GetWindowName();
        HWND GetWindowHandle(); 
        LRESULT OnDerivedWndProc(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        //UserApp interface
        virtual LRESULT OnEvent(Page *pPage, UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL) = 0;
        virtual STRING GetLayoutByID(INT iLayoutID) = 0;
        virtual INT OnCreate(Page *pPage) = 0;
        virtual INT OnStart(Page *pPage) = 0;
        virtual INT OnStop(Page *pPage) = 0;
        virtual INT OnDestroy(Page *pPage) = 0;
        //HOOK Callback Start
        INT PreOnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        INT OnDraw(PHDC pHDc, RECT rtPaint); 
        STRING GetName();  
        INT GetLayerPrior(); 
        RECT* GetLayerRect();
        BOOL SetLayerStatus(ELayerFlagType eFlag, BOOL bSet);
        BOOL CheckLayerStatus(ELayerFlagType eFlag);
        //HOOK Callback End 

        //UserTimer
        BOOL StartUserTimer(LPVOID pUser,UINT32 iInterval);
        VOID StopUserTimer(LPVOID pUser,UINT32 iInterval);
        virtual VOID OnUserTimer(LPVOID pUser,UINT32 iInterval) ;

private:
        INT PushPage(Page *pNewPage);
        INT PopPage();
        VOID ChangeLanguage();
        INT CreatePage(INT iLayoutID, LPVOID pParam = NULL);
        INT CreatePage(STRING *pStrLayoutID, LPVOID pParam = NULL);
        INT DestroyPage(); 
        INT Create(LPCTSTR strName);
        INT Stop();
        INT Start();
        INT Destroy();
        INT ParserResPack();
        VOID ClearPageStack();
        STATIC LPVOID SetResObject(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj);  
        STATIC INT SetResKeyValue(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj, STRING strKeyName);

private:
        LPVOID mHResPackAdapter;
        STRING mResPackName;
        STRING mActivityName;
        UINT16 mDrawableArray[MAX_DRAWABLE_NUM];
        UINT16 mRawArray[MAX_RAW_NUM];
        UINT16 mLangArray[MAX_LANG_NUM];
        CPtrArray mPageStack;
        EWindowType mWndType;
        LHANDLE mTimerHandle;
        INT mLayerStatus;
        INT mLayerHandle;
        LTGUI_MUTEX *mpMutex;
};

}
#endif //__BASEACTIVITY_H__
