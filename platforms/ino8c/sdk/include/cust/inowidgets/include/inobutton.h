/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOBUTTON_H__
#define __INOBUTTON_H__

#include "inogui.h"
#include "inolabel.h"
#include "inopubfun.h"

namespace INONEGUI_FRAMEWORK {

#define BTN_LONGPRESSTIME_MIN       (1000/LTGUI_TIMER_TICKMS)

enum EBtnAttrib
{
	BTN_PATT_NOTHING          = 0x0000,       //nothing
	BTN_PATT_INITIALOK          = 0x0001,         //btn inital ok
	BTN_PATT_ENABLELP          = 0x0002,      //enable longpress?
	BTN_PATT_KEYLP                = 0x0004,      //KEY longpresss
};

#define ClrBtnPrivAttAll()               	  	    CLRBIT_ALL(mBtnPrivAtt)
#define SetBtnPrivAttValue(iBitValue)           SETBIT_ALL(mBtnPrivAtt,iBitValue)
#define ClrBtnPrivAttByBit(iBitValue)            CLRBIT_BY_BIT(mBtnPrivAtt,iBitValue)
#define SetBtnPrivAttByBit(iBitValue)            SETBIT_BY_BIT(mBtnPrivAtt,iBitValue)
#define IsSetBtnPrivAttByBit(iBitValue)         ISSET_BY_BIT(mBtnPrivAtt,iBitValue)
#define IsSetBtnPrivAttByMask(iBitValue)     ISSET_BY_MASK(mBtnPrivAtt,iBitValue)

class api_out InoButton : public InoLabel 
{
public:
	InoButton();
	virtual ~InoButton();
	INT GetControlType();
	VOID SetAttribute(STRING strName, STRING strValue);
	VOID SetAttribute(STRING strName, DWORD iValue);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
	VOID SetImageRect(RECT &rImgRect);
	RECT& GetImageRect();
	
	VOID SetImage(ECtrlStateType iType, STRING strName);
	STRING& GetImage(ECtrlStateType iType);
	
protected:
	INT TouchReset(WPARAM wParam , LPARAM lParam, INT &rFlag, ECtrActionType &rAction);
	INT OnTimer(WPARAM wParam , LPARAM lParam, INT &rFlag, ECtrActionType &rAction);
	INT TouchUp(WPARAM wParam , LPARAM lParam, INT &rFlag, ECtrActionType &rAction);
	INT TouchMove(WPARAM wParam , LPARAM lParam, INT &rFlag, ECtrActionType &rAction);
	INT TouchDown(WPARAM wParam , LPARAM lParam, INT &rFlag, ECtrActionType &rAction);
	
private:
	INT DrawBtn(PHDC pHDc, LPVOID pResAdapter, STRING *pBtnImg, DWORD *pBtnColor, RECT &rSrcRc, RECT &rDstRc);
	STATIC INT ParserButtonAttParam(InoButton *pButton, DWORD iValue);
	
protected:
	DWORD mBtnColor[MAX_CTRL_STATE];
	STRING mBtnImg[MAX_CTRL_STATE]; 
	RECT mBtnImgRect;        //image rect
	//btnatt-S
	BOOL mAttIsBeep;
	UINT8 mAttBtnImgAlign;	
private:
	UINT8 mAttLPCnt;
	UINT8 mAttLPRepCnt;
	//btnatt-E
	UINT16 mPressCount;
	UINT8 mBtnPrivAtt;
};

}//end namespace
#endif	//__INOBUTTON_H__