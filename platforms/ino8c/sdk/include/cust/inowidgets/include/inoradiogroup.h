/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INORADIOGROUP_H__
#define __INORADIOGROUP_H__
#include "inogui.h"
#include "log.h"
#include "container.h"

namespace INONEGUI_FRAMEWORK {
#define ClrRgPrivAttAll()               	  	       CLRBIT_ALL(mRgPrivAtt)
#define SetRgPrivAttValue(iBitValue)            SETBIT_ALL(mRgPrivAtt,iBitValue)
#define ClrRgPrivAttByBit(iBitValue)             CLRBIT_BY_BIT(mRgPrivAtt,iBitValue)
#define SetRgPrivAttByBit(iBitValue)            SETBIT_BY_BIT(mRgPrivAtt,iBitValue)
#define IsSetRgPrivAttByBit(iBitValue)         ISSET_BY_BIT(mRgPrivAtt,iBitValue)
#define IsSetRgPrivAttByMask(iBitValue)     ISSET_BY_MASK(mRgPrivAtt,iBitValue)

enum ERgPrivAtt
{
	RG_PATT_INITIALOK          	= 0x01,         //RG inital ok
	RG_PATT_MOVING			= 0x02,
};

class InoRadioGroup;
class IRadioGroupListener {  
public:  
    virtual ~IRadioGroupListener() { }  
    virtual VOID OnRadioGroupChanged(InoRadioGroup *pRadioGroup, INT iSelectedId) = 0;  
};  

class api_out InoRadioGroup:public Container{
public:
	InoRadioGroup();
	virtual ~InoRadioGroup();
	INT GetControlType();
	VOID SetAttribute(STRING strName, STRING strValue);
	VOID SetAttribute(STRING strName, DWORD iValue);
	BOOL SetRadioGroupSelectedById(INT iCtrlId);
	BOOL SetRadioGroupSelectedByIndex(INT iindex);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = 0);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL, INT iFlag = CTRL_DRAWFLAG_ALL);
	VOID SetRadioGroupListener(IRadioGroupListener *pListener); 
private:
        VOID DoRadioGroupInit();
        VOID NotifyListener(InoControl *pActiveCtrl, ECtrActionType eAction);
        INT SetRadioGroupSelectedCtrl(InoControl *pSelectedCtrl);
        InoControl* TouchUp(UINT iMsg, WPARAM wParam , LPARAM lParam, INT &rFlag, ECtrActionType &rAction, DWORD dwTime);
        STATIC INT ParserRadioGroupAttParam(InoRadioGroup *pRadioGroup, DWORD iValue);
private:
	IRadioGroupListener *mpRgListener;
	UINT8 mInitialIndex;
	InoControl *mpSelectedCtrl;
	UINT8 mRgPrivAtt;
};

}
#endif //__LTOPTIONGROUP_H__

