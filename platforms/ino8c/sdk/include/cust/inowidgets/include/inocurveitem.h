/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOCURVEITEM_H__
#define __INOCURVEITEM_H__

#include "inocontrol.h"

namespace INONEGUI_FRAMEWORK {

#define MIN_DRAWCURVE_NUM		3

//
#define CI_QA_SPEED					2
#define CI_QA_AA					10

typedef enum _ECurveType{
	CI_SINE_WAVE,
	CI_BKLINE_WAVE
}ECurveType;

class InoCurveWnd;
class api_out InoCurveItem : public InoControl
{
public:
	InoCurveItem();
	virtual ~InoCurveItem();
	INT GetControlType();
	VOID SetAttribute(STRING strName, DWORD iValue);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);

	INT PushCurveData(INT *piData, INT iLen = 1);
private:
	friend class InoCurveWnd;
	INT RenderCurveData();
	INT GetCurveXPos(INT iIndexOff);
	INT GetCurveYPos(INT iValue);
	INT PopCurveData(INT iIndex, INT &rPopData, DOUBLE &rXVal, DOUBLE &rYVal);
	INT MoveArray(DOUBLE *pArray, INT iLen, BOOL bLeftM = TRUE);
	VOID ResetCurveXYData(INT iEndIndex);
private: 
	InoCurveWnd *mpCurveWnd;
	DWORD mCiColor;
	SINT16 mCiRatio;
	SINT16 mCiZeroOff;
	UINT8   mCiWidth;
	ECurveType mCiStyle;
	UINT8 mCiQaV;    //quliaty value
	DOUBLE mXData[MIN_DRAWCURVE_NUM];
	DOUBLE mYData[MIN_DRAWCURVE_NUM];
	LTGUI_SURFACE *mpCiDc;
	LTGUI_MUTEX	*mpMutex;
	list<INT> mCurveData;
};

}//end namespace
#endif	//__INOCURVEITEM_H__



