/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

/**
* __LTWIDGET_HEADER_MIN_H__
*/
#ifndef __LTWIDGET_HEADER_MIN_H__
#define __LTWIDGET_HEADER_MIN_H__

#include "inogui.h"
#include "log.h"
#include "hwnd.h"

using namespace INONEGUI_FRAMEWORK;
#endif //__LTGUI_HEADER_MIN_H__