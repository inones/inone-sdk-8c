/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

//Object move class
#ifndef __OBJMOVE_H__
#define __OBJMOVE_H__

#include "move.h"

namespace INONEGUI_FRAMEWORK {
//pObj:Move object
typedef UINT (*WhenMoveToEnd)(void* pObj, void* pParam);
typedef UINT (*RedrawCallback)(void* pObj, void* pParam);

struct SObjMoveInfo
{
	void*				pObj;
	void*				pParam;
	EMoveCurve			eCurveType;
	WhenMoveToEnd		fnWhenEndMove;
	RedrawCallback		fnRedrawCallback;

	SObjMoveInfo()
	{
		pObj			= NULL;
		pParam			= NULL;
		eCurveType		= MOVE_INVALID;
		fnWhenEndMove	= NULL;
		fnRedrawCallback  = NULL;
	}

	~SObjMoveInfo()
	{
	}
};

class CObjMove : public CMove
{
public:
	CObjMove(void);
	~CObjMove(void);

//重写
protected:
	void	ReDrawObject(POINT ptCur);
	void	ArriveTerminate(void);

private:
	void*				m_pObj;
	void*				m_pParam;
	WhenMoveToEnd		m_fnWhenArriveEnd;
	RedrawCallback 		m_fnRedrawCallback;

// 重新加载时用到
	EMoveCurve			m_eCurveType;

public:
	BOOL InitObjectMove(SObjMoveInfo *pInfo, SCurveParam* pParam);
        void  MoveTerminate(void);
	void	ReLoadMove(void);	// 重新加载上一次运动
};

}//namespace INONEGUI_FRAMEWORK 

#endif //__OBJMOVE_H__