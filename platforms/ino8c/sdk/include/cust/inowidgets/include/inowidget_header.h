/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

/**
* __INOWIDGET_HEADER_H__
*/
#ifndef __INOWIDGET_HEADER_H__
#define __INOWIDGET_HEADER_H__
#include "inocontrol.h"
#include "page.h"
#include "inobutton.h"
#include "inolabel.h"
#include "inoscrolllist.h"
#include "inolistitem.h"
#include "inoedit.h"
#include "inoseekbar.h"
#include "inometer.h"
#include "inoradiogroup.h"
#include "inoimage.h"
#include "inosubpage.h"
#include "inocanvas.h"
#include "inoqrcode.h"
#include "inocurvewnd.h"
#include "inocurveitem.h"
#include "inomediaplay.h"
#include "inosubpagegroup.h"
#include "inowndpage.h"
#include "inowindow.h"

#include "inocompatible.h"
#endif //__INOWIDGET_HEADER_H__

