/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __CONTAINER_H__
#define __CONTAINER_H__
#include "inogui.h"
#include "inobutton.h"
#include "log.h"

namespace INONEGUI_FRAMEWORK {

class api_out Container:public InoButton{
    public:
        Container();
        virtual ~Container();
        virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
        InoControl* FindControlByName(STRING strName);
        InoControl* FindControlById(INT iCtrlId);
        InoControl* GetControlByIndex(INT iIndex);
        InoControl* FindControlByType(INT iType);
        STATIC WidgetMapType* GetWidgetMapByKeyName(LPCTSTR pCtrlKeyName);
        STATIC InoControl* NewControlByKeyName(LPCTSTR pCtrlKeyName);
        STATIC InoControl* NewControlByType(INT iType, InoControl *pCopyObj = NULL);
        INT GetContCtrlCount(){return mControlList.GetSize();};    
        INT AddControlToCont(InoControl *pCtrl);
        VOID ClearContCtrlList(BOOL bReleaseCtrl = TRUE);
        INT ContainerFactory(LPCTSTR pName, LPCTSTR pRes, LPCTSTR pJson);
        INT SetControlListClickListener(IClickListener *lpListener);
        INT SetControlListLongClickListener(ILongClickListener *lpListener);
        RECT& GetContRect(){return mCtrlRect;};
        INT MoveCtrlPosOnCont(INT iDiffX, INT iDiffY, BOOL bRecurse = FALSE,Container *pCont = NULL);
        InoControl * FindHitControl(POINT sPt, INT iFlag,BOOL bRecurse = FALSE);
        BOOL IsTopCont(){return (mpParent = NULL)?TRUE:FALSE;};
        VOID DumpContCtrlListString(Container *pCont,INT iDepth = 0);
        VOID InitialContDisplayRect(RECT sRect);
        VOID SetContDisplayPos(POINT sPt);
        RECT GetContDisplayRect(){return mContDisplayRect;};
        POINT GetContDisplayPos(){return {mContDisplayRect.x,mContDisplayRect.y};};
        INT GetContDisplayPosX(){return mContDisplayRect.x;};
        INT GetContDisplayPosY(){return mContDisplayRect.y;}; 
#if D_USE_JSONCPP  
        virtual INT SetContAttribute(Json::Value v);
#endif

    protected:
        INT ContainerStop();
        VOID SetFocusControl(InoControl *pCtrl);

    private:
        VOID DoContInit();
        BOOL IsMove(POINT &sPt);
        InoControl * FindMoveControl(InoControl *pCtrl);
        STATIC BOOL Comp(InoControl *pCtrla, InoControl *pCtrlb);
        STATIC INT SortControlPriorOnCont(Container *pCont);
        InoControl * FindNeighborControlByDir(InoControl *pCtrl, EDirType  eDir);
#if D_USE_JSONCPP  
        INT ParserContLayout(Json::Value v, Container *pCont,InoControl *pCtrl = NULL);
        INT ParserControlInfo( InoControl *p, Json::Value v, STRING strKey);
#elif D_USE_CJSON
        INT ParserContLayout(cJSON *v, Container *pCont,STRING strArrayName = "", InoControl *pCtrl = NULL);
        INT ParserControlInfo( InoControl *p, cJSON *v, STRING strKey);
#endif
        INT CalcFirstDrawCtrlIndex(CPtrArray *pContList, RECT &rtPaint);

   protected:
        InoControl *mpActiveControl;
        InoControl *mpMoveControl;
        InoControl *mpFocusControl;
        LTGUI_MUTEX *mpMutex;
        
    private:
        CPtrArray mControlList; 
        RECT mContDisplayRect;
};

}
#endif //__CONTAINER_H__

