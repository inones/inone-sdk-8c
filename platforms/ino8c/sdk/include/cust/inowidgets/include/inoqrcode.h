/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOQRCODE_H__
#define __INOQRCODE_H__

#include "inocontrol.h"
#include "qrcode.h"

namespace INONEGUI_FRAMEWORK {

// Error Correction Code Levels
typedef enum{
	QR_ECC_L 	=0,
	QR_ECC_M	=1,
	QR_ECC_Q   	=2,
	QR_ECC_H    =3,
}EQrEccType;

class api_out InoQrcode : public InoControl
{
public:
	InoQrcode();
	virtual ~InoQrcode();
	INT GetControlType();
	VOID SetAttribute(STRING strName, STRING strValue);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);

	CONST STRING& GetQRContent();
	INT SetQRContent(CONST STRING &strQRText, BOOL bForce = FALSE, EQrEccType iEccLevel =QR_ECC_L, INT iQRVer = 3);
private:
	
private: 
	LTGUI_SURFACE *mpQRDc;
	QRCode mQRcode;
	STRING mQRContent;
};

}//end namespace
#endif	//__INOQRCODE_H__

