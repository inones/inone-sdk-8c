/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOLABEL_H__
#define __INOLABEL_H__

#include "inogui.h"
#include "inocontrol.h"
#include "charset_convert.h"

namespace INONEGUI_FRAMEWORK {

#define LTWDT_LBL_BEGIN            			   (MSG_GUI + 0x200)
#define LTWDT_LBL_UPDATE_DISTEXT           (LTWDT_LBL_BEGIN + 1)

#define ClrLblPrivAttAll()               	  	    CLRBIT_ALL(mLblPrivAtt)
#define SetLblPrivAttValue(iBitValue)           SETBIT_ALL(mLblPrivAtt,iBitValue)
#define ClrLblPrivAttByBit(iBitValue)            CLRBIT_BY_BIT(mLblPrivAtt,iBitValue)
#define SetLblPrivAttByBit(iBitValue)            SETBIT_BY_BIT(mLblPrivAtt,iBitValue)
#define IsSetLblPrivAttByBit(iBitValue)         ISSET_BY_BIT(mLblPrivAtt,iBitValue)

enum ELblAttrib
{
	LBL_PATT_NOTHING         = 0x0000,       //nothing
	LBL_PATT_INITIALOK       = 0x0001,         //lbl inital ok
	LBL_PATT_REDRAW         	 = 0x0002,         //lbl redraw txt dc when state or content change
};

#define ERR_OK_DRAWFULLTXT	(ERR_OK + 1)

#define MAX_CHARSET_NUM			16
#define MAX_CHARSET_IMGSRC_LEN	32
typedef struct _tagImgCharsetType
{
	CHAR cCharCode;
	CHAR aImgSrc[MAX_CHARSET_IMGSRC_LEN];
	SIZE  stImgSize;
}ImgCharsetType;

class api_out InoLabel : public InoControl
{
public:
	InoLabel();
	virtual ~InoLabel();
	INT GetControlType();
	VOID SetAttribute(STRING strName, STRING strValue);
	VOID SetAttribute(STRING strName, DWORD iValue);
	VOID SetAttribute(STRING strName, cJSON *cJsonV);
	VOID SetState(ECtrlStateType state);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
	VOID SetText(STRING strText, BOOL bSyncDisText = TRUE, BOOL bRefresh=TRUE);
	STRING& GetText(){return mText;};
	VOID SetFontSize(UINT8 iFontSZ);
	VOID SetBold(BOOL bEnable);
	VOID SetItalic(BOOL bEnable);
	VOID SetColor(DWORD iColor, ECtrlStateType eState = (ECtrlStateType)INVALID_VALUE);
	UINT8& GetFontSize(){return mAttFontSZ;};
	
private:
	STATIC INT ParserLabelAttParam(InoLabel *pLabel, DWORD iValue);
	STATIC INT ParserLabelAtt1Param(InoLabel *pLabel, DWORD iValue);
	INT OnTimer();
	INT SetDisplayTextByLang();
	INT UpdateDisplayText();
	VOID GetTextDisplayRect(RECT &stDisRect);
	INT GetTextLenByCharIndex(IN CB_LEN_FIRST_CHAR pFirstCharFun, IN STRING &strText, IN INT iCharIndex, OUT INT &iOffset);
	INT GetTextTotalDisplayWH(STRING &strTxt, INT iFontSize, INT &iTxtWidth, INT &iTxtHigh);
	INT FormatTextForAutoWarp(INOUT STRING &strTxt,IN RECT stDisRect);
	INT FormatTextForEllipsisEnd(INOUT STRING &strTxt, IN RECT stDisRect);
	INT FormatTextForEllipsisMiddle(INOUT STRING &strTxt, IN RECT stDisRect);
	INT FormatTextForEllipsisStart(INOUT STRING &strTxt, IN RECT stDisRect);
	INT FormatTextForEllipsisMarquee(INOUT STRING &strTxt, IN RECT stDisRect);
	INT FormatTextByStyle(INOUT STRING &strTxt, IN RECT stDisRect);
	
	INT PaserStringToCharsetMap(STRING strVal);
	INT DrawTxtByPrivCharset(PHDC pHDc, LPCTSTR pUtf8Txt,RECT stSrcRect, RECT stDstRect, INT iStyle,LPVOID pResAdapter = NULL);
	ImgCharsetType *GetCharsetInfoByChar(CHAR cCharset);
	INT GetCharCount(LPCTSTR pText, INT iIndex);
	
protected:
	VOID SetDisplayText(STRING &strText);
	
private: 
	ImgCharsetType *mpCharsetMap[MAX_CHARSET_NUM];
	UINT8 mCharsetCnt;

	DWORD mTextColor[MAX_CTRL_STATE];
	STRING mText;
	STRING mDisplayText;	
	UINT16 mTickCount;
	POINT mLblDispOffPt;
	INT		mLblDisOffMinV;
	INT		mLblDisOffMaxV;
	LTGUI_SURFACE *mpLblDc;

	UINT8	mLblPrivAtt;
	//lblatt-S
	UINT8 	mAttFontSZ;
	UINT8 	mAttAlign;
	BOOL 	mAttIsBold;
	BOOL 	mAttIsItalic;
	UINT8 	mAttEllipsisMode;
	UINT8 	mAttMarqueeMode;
	//lblatt-E
	//lblatt1-S
	UINT16 mAtt1RefInterV;
	UINT16 mAtt1RefStepV;
	//lblatt1-E
};

}//end namespace
#endif	//__INOLABEL_H__