/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOLISTITEM_H__
#define __INOLISTITEM_H__
#include "inogui.h"
#include "log.h"
#include "inolabel.h"
#include "container.h"

namespace INONEGUI_FRAMEWORK {

#define ClrLiPrivAttAll()               	  	    CLRBIT_ALL(mLiPrivAtt)
#define SetLiPrivAttValue(iBitValue)           SETBIT_ALL(mLiPrivAtt,iBitValue)
#define ClrLiPrivAttByBit(iBitValue)            CLRBIT_BY_BIT(mLiPrivAtt,iBitValue)
#define SetLiPrivAttByBit(iBitValue)            SETBIT_BY_BIT(mLiPrivAtt,iBitValue)
#define IsSetLiPrivAttByBit(iBitValue)         ISSET_BY_BIT(mLiPrivAtt,iBitValue)
#define IsSetLiPrivAttByMask(iBitValue)     ISSET_BY_MASK(mLiPrivAtt,iBitValue)


#define LISTITEM_INDEX_INITIAL_V        (-1)
class InoListItem;
class IListItemClickListener {  
public:  
    virtual ~IListItemClickListener() { }  
    virtual VOID ListItemClick(InoListItem *pListItem, INT iIndex, BOOL bLongClick) = 0;
}; 

typedef enum{
	ITEM_RENDER_INVALID,
	ITEM_RENDER_READY,	
	ITEM_RENDER_OK,	
}EItemRdType;

//Listitem attr bit 
typedef enum ELiPrivAtt{
	LI_PATT_NOCACHE 	= 0x01,
}ELiPrivAttType;

class api_out InoListItem:public Container{
public:
        InoListItem();
        virtual ~InoListItem();
        InoListItem(CONST InoListItem &rListItem);
        INT GetControlType();
        VOID SetListItemClickListener(IListItemClickListener *pListener);
        INT NotifyListItemClickListener(BOOL bLongClick);
        virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = 0);
        virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
        VOID SetListItemIndex(UINT uIndex);
        VOID SetListItemDispIndex(INT iIndex);
        INT GetListItemDispIndex();
        VOID ResetListItemIndex();
        UINT GetListItemIndex(){return mItemIndex;};
        STRING& GetListItemName(){return mCtrlName;};
        RECT& GetListItemRect(){return mCtrlRect;};
        VOID SetListItemRenderSt(EItemRdType eSt);
        VOID SetListItemPrivAtt(ELiPrivAttType iAtt);
private:
	VOID InitialListItem();
	INT RenderListItem(PHDC pDc = NULL, RECT stArea = {0},LPVOID pResAdapter = NULL ,INT iFlag = NULL);
		
private:
        INT mItemIndex;
        INT mItemDispIndex;
        EItemRdType mItemRenderSt;
        LTGUI_SURFACE *mpItemDc;
        LTGUI_MUTEX	*mpRdMutex;
        IListItemClickListener *mpListItemListener;
        UINT8 mLiPrivAtt;
};

}
#endif //__INOLISTITEM_H__

