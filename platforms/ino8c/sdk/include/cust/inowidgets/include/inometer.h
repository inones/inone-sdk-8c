/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOMETER_H__
#define __INOMETER_H__

#include "inogui.h"
#include "inoseekbar.h"
#include "objmove.h"

namespace INONEGUI_FRAMEWORK {

#define ClrMtPrivAttAll()               	  	       CLRBIT_ALL(mSbPrivAtt)
#define SetMtPrivAttValue(iBitValue)            SETBIT_ALL(mSbPrivAtt,iBitValue)
#define ClrMtPrivAttByBit(iBitValue)             CLRBIT_BY_BIT(mSbPrivAtt,iBitValue)
#define SetMtPrivAttByBit(iBitValue)            SETBIT_BY_BIT(mSbPrivAtt,iBitValue)
#define IsSetMtPrivAttByBit(iBitValue)         ISSET_BY_BIT(mSbPrivAtt,iBitValue)
#define IsSetMtPrivAttByMask(iBitValue)     ISSET_BY_MASK(mSbPrivAtt,iBitValue)

enum EMtPrivAtt
{
	MT_PATT_INITIALOK          	= 0x01,         //mt inital ok
	MT_PATT_MOVING			= 0x02,
	MT_PATT_BARDC			= 0x04,        //bar dc ready
	MT_PATT_POINTDC			= 0x08,        //point dc ready
	MT_PATT_SYNPBAR	       		= 0x10,		//sync paint colorbar
	MT_PATT_SYNPPOINT	       = 0x20,		//sync paint point
	MT_PATT_THREADEXIT	       = 0x40,		//thread exit
	MT_PATT_BESTMETER	       = 0x80,		//best meter
};

//Meter private attrib
#define _MT_KEY_I_BESTMETER      "BESTMETER"
#define _MT_KEY_S_MTMVCFG         "MTMVCFG"

//_MT_KEY_S_MTMVCFG         "MTMVCFG"
#define MTMVCFG_MV_TYPE_OFF		29
#define MTMVCFG_MV_ACC_OFF		24
#define MTMVCFG_MV_SPEED_OFF		16
#define MTMVCFG_MV_MINV_OFF		12
#define MTMVCFG_MV_MAXV_OFF		0
enum EMtMvCfg{
       MTMVCFG_MV_EN			 =	0x80000000,
	MTMVCFG_MV_TYPE			 =	0x60000000,
	MTMVCFG_MV_ACC		        =	0x1F000000,
	MTMVCFG_MV_SPEED		 =	0x00FF0000,
	MTMVCFG_MV_MINV		        =	0x0000F000,
	MTMVCFG_MV_MAXV		        =	0x00000FFF,
};
#define IsSetMtMvCfgtByBit(iBitValue)            			ISSET_BY_BIT(mMeterMovCfg,iBitValue)
#define SetMtMvCfgtByBit(iBitValue)            			       SETBIT_BY_BIT(mMeterMovCfg,iBitValue)
#define GetValueMtMvCfgByOffset(mask,offset)  		GETVAL_BY_OFFSET(mMeterMovCfg,mask,offset)
#define SetValueMtMvCfgByOffset(mask,offset,val)  		SETVAL_BY_OFFSET(mMeterMovCfg,mask,offset,val)

//Meter listener
STATIC INT MeterThreadTask(VOID *para);
class api_out InoMeter : public InoSeekBar
{
public:
	InoMeter();
	virtual ~InoMeter();
	INT GetControlType();
	VOID SetAttribute(STRING strName, STRING strValue);
	VOID SetAttribute(STRING strName, DWORD iValue);
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
       friend INT MeterThreadTask(VOID *para);
private:
	STATIC UINT AccEnded(VOID* pObj, VOID* pParam);
	STATIC UINT RedrawCallback(VOID* pObj, VOID* pParam);
	VOID DoMeterInit();
	INT OnTimer();
	VOID NowPaint(RECT &rPaintRect, INT iType);
       VOID ResetMeterUpdateRect();
	INT PutMeterColorBar(INT iNewAngle,BOOL bPutColor = TRUE);
	INT PutMeterPointer(INT iNewAngle, BOOL bSmooth = TRUE);
	INT Acc2EndByV(INT iStartV,INT iEndV, INT iSpeed);
	VOID StopMove(void);
	VOID StartMove(void);	
	INT SetSeekBarData(SbDataType *pData,BOOL bUpdateDisVal = TRUE ,BOOL bForce = FALSE);
	
private:
        RECT 		mPointerOldRt;    	//abs rect for old Point update
        RECT              mMeterUpdateRt;       //Meter Update Rect
        RECT 		mMeterAbsRt;            //abs rect for meter
        POINT 		mMeterCPt;              	  //releation center pt for meter
        POINT		mPointerRPt;	         //Pointer rotate point
        UINT16		mPointerR;			 //Pointer rotate point - pointer center 
        LTGUI_SURFACE* mpBgDc;                //backgroud dc
        LTGUI_SURFACE* mpPointerDc;
        LTGUI_SURFACE* mpBlendPointerDc;
        LTGUI_MUTEX *mpPMutex;
        LTGUI_MUTEX *mpBMutex;
        LTGUI_SEM 	*mpPPaintSem;
        LTGUI_THREAD *mMeterThread;
        CObjMove	mObjMov;
        UINT32          mMeterMovCfg;        //meter move cfg,eg, type/speed/acc/minmov/maxmov
        INT                mMeterNewAngle;

};

}//end namespace

#endif	//__INOMETER_H__
