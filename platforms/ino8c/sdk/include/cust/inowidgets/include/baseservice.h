/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __BASESERVCIE_H__
#define __BASESERVCIE_H__

#include "inogui.h"
#include "basewindow.h"
#include "timermanage.h"
#include "log.h"
namespace INONEGUI_FRAMEWORK {

typedef enum _ESerStateType{
	SER_STATE_UNINITIAL 	= 0,
	SER_STATE_INITIAL,
	SER_STATE_CREATED,
	SER_STATE_STARTING,	
	SER_STATE_STARTED,	
	SER_STATE_STOPING,
	SER_STATE_STOPED,
	SER_STATE_DESTROYING,
	SER_STATE_DESTROYED,
}ESerStateType;

#define MIN_JOB_DELAY                    1000
#define MIN_JOB_DELAY_STARTED   200
#define MIN_JOB_DELAY_STOPED     1000
		
class api_out BaseService:public BaseWindow,
                                                    public IWindowListener,
							 public IUserTimer{
    public:
        BaseService();
        BaseService(STRING strName);
        virtual ~BaseService(); 
	STRING& GetServiceName(){return mServiceName;};
	ESerStateType GetServiceState(){return mServiceState;};
	STATIC  INT OnBaseJobProc(VOID *pParam);
	
        //IWindowListener
	STRING GetWindowName();
	HWND GetWindowHandle(); 
        LRESULT OnDerivedWndProc(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);  
		
        //UserService interface
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL) = 0;
	virtual INT OnCreate() = 0;
	virtual INT OnStart() = 0;
	virtual INT OnStop() = 0;
	virtual INT OnDestroy() = 0;
	virtual INT OnUserJobProc() = 0;

        //UserTimer
	BOOL StartUserTimer(LPVOID pUser,UINT32 iInterval);
	VOID StopUserTimer(LPVOID pUser,UINT32 iInterval);
	virtual VOID OnUserTimer(LPVOID pUser,UINT32 iInterval) = 0 ;
	
    private:
	INT Init(LPCTSTR strName);
	INT Create();
	INT Stop();
	INT Start();
	INT Destroy();
	INT Deinit();
        
    private:
	STRING mServiceName;
	INT mJobThID;
	ESerStateType mServiceState;
	LTGUI_THREAD *mpJobTh;
	LTGUI_MUTEX *mpMutex;
};

}
#endif //__BASESERVCIE_H__

