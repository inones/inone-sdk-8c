/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __PAGE_H__
#define __PAGE_H__
#include "inogui.h"
#include "log.h"
#include "container.h"
#include "wndanimators/wndanimmanager.h"

namespace INONEGUI_FRAMEWORK {

#define DEF_PAGEWND_ANIM            USING_SYS_WNDANIM                //using globe anim
typedef enum _EPageStateType{
	PG_STATE_INITIAL 	= 0,
	PG_STATE_CREATED,
	PG_STATE_STARTING,	
	PG_STATE_STARTED,	
	PG_STATE_STOPING,
	PG_STATE_STOPED,
	PG_STATE_DESTROYING,
	PG_STATE_DESTROYED,
}EPageStateType;

enum EPageAttrib
{
	PG_PATT_NOTHING            = 0x0000,       //nothing
	PG_PATT_ENDRAW             = 0x0001,      //page enable draw
	PG_PATT_ENCACHED          = 0x0002,      //enable cached
	PG_PATT_ENBGWP              = 0x0004,      //enable page background on wallpager
};

#define ClrPagePrivAttAll()               	  	   CLRBIT_ALL(mPagePrivAtt)
#define SetPagePrivAttValue(iBitValue)           SETBIT_ALL(mPagePrivAtt,iBitValue)
#define ClrPagePrivAttByBit(iBitValue)            CLRBIT_BY_BIT(mPagePrivAtt,iBitValue)
#define SetPagePrivAttByBit(iBitValue)            SETBIT_BY_BIT(mPagePrivAtt,iBitValue)
#define IsSetPagePrivAttByBit(iBitValue)         ISSET_BY_BIT(mPagePrivAtt,iBitValue)
#define IsSetPagePrivAttByMask(iBitValue)     ISSET_BY_MASK(mPagePrivAtt,iBitValue)

class api_out Page:public Container{
public:
        Page();
        virtual ~Page();
        LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = 0);
        INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL, INT iFlag = CTRL_DRAWFLAG_ALL);
        INT GeneratePage(LPCTSTR pName, LPCTSTR pRes, LPCTSTR pJson);        
        STRING& GetPageName(){return mCtrlName;};
        RECT& GetPageRect(){return mCtrlRect;};
        EPageStateType& GetPageState(){return mPageState;};
        VOID SetPageCached(BOOL bCached);
        BOOL GetPageCached();
        BOOL SetPageDrawRect(RECT *pDrawRect = NULL);
        VOID SetPageDrawBgWpp(BOOL bEnable);
        BOOL SetPageWndAnim(EWndAnimatorType eWndAnim);
        INT AddPageRef();
        INT DecPageRef();
        INT GetPageRef(){return mPageRef;};
        INT GetPageTimestamp(){return mPageTimestamp;};
        ELangType& GetPageLang(){return mPageLang;};
        LPVOID GetPageParam(){return mpPageParam;};
        LPVOID GetPageOwner(){return mpPageOwner;};
        EWndAnimatorType GetPageWndAnim(){return mWndAnim;};
        friend class BaseActivity;
        friend class BaseWindow;
        friend class InoWndPage;
        
private:
        BOOL SetPageLang(ELangType eLang){mPageLang = eLang;return TRUE;};
        BOOL SetPageState(EPageStateType eSetState);
        VOID SetPageParam(LPVOID pParam){mpPageParam = pParam;};
        VOID SetPageOwner(LPVOID pParam){mpPageOwner = pParam;};
        
private:
        INT mPageRef;
        EWndAnimatorType mWndAnim;
        UINT32 mPageTimestamp;
        RECT mPageDrawRect;
        ELangType mPageLang;
        EPageStateType mPageState;
        LPVOID mpPageParam;
        LPVOID mpPageOwner;
        UINT8 mPagePrivAtt;
};

}
#endif //__PAGE_H__
