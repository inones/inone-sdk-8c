/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOSUBPAGE_H__
#define __INOSUBPAGE_H__

#include "inolistitem.h"

namespace INONEGUI_FRAMEWORK {

class api_out InoSubpage : public InoListItem
{
public:
	InoSubpage();
	virtual ~InoSubpage();
	INT GetControlType();
	virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = 0);
	virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
};

}//end namespace
#endif	//__INOSUBPAGE_H__