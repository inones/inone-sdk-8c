/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __BASEMESSAGEBOX_H__
#define __BASEMESSAGEBOX_H__

#include "inogui.h"
#include "basewindow.h"
#include "inowidget_header.h"
#include "timermanage.h"
#include "log.h"
#include "layer.h"


namespace INONEGUI_FRAMEWORK {

class api_out  BaseMessageBox:public BaseWindow,                           
                                                                    public IClickListener,
                                                                    public ILongClickListener,
                                                                    public ITimerListener,
                                                                    public IWindowListener,
                                                                    public ILayerHook{
    public:
        BaseMessageBox();
        BaseMessageBox(STRING strName);
        virtual ~BaseMessageBox(); 
        STATIC BaseMessageBox* GetInstance();
        STRING& GetTitlebarName(){return mBoxName;};
        virtual VOID OnClick(InoControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
        virtual VOID OnLongClick(InoControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
		
	//IWindowListener
	STRING GetWindowName();
	HWND GetWindowHandle();
        LRESULT OnDerivedWndProc(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);		
        INT DoMessageBox(LPCTSTR strTitle,LPCTSTR strContent,INT iTimeoutSec = BOX_TIMEOUT_SEC,BOOL IsModal = TRUE);
        
        //UserApp interface
        virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        virtual STRING GetLayoutByID(INT iLayoutID) ;
        virtual INT OnCreate(Page *pPage);
        virtual INT OnStart(Page *pPage);
        virtual INT OnStop(Page *pPage);
        virtual INT OnDestroy(Page *pPage);
        
         //HOOK Callback Start
        INT PreOnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        INT OnDraw(PHDC pHDc, RECT rtPaint); 
        STRING GetName();  
        INT GetLayerPrior(); 
        RECT* GetLayerRect();
        BOOL SetLayerStatus(ELayerFlagType eFlag, BOOL bSet);
        BOOL CheckLayerStatus(ELayerFlagType eFlag);
        //HOOK Callback End          
    private:
	INT InvalidateRectArea(RECT stArea);
	VOID OnTimer();
        INT SendTextToLabel(InoLabel *pLabel,STRING strText);
        INT CreatePage(INT iLayoutID);
        INT DestroyPage(INT iLayoutID); 
        INT Create(LPCTSTR strName);
        INT Stop();
        INT Start();
        INT Destroy();
        
    private:
        STATIC BaseMessageBox* mpInstance;
        STRING mBoxName;
        INT mLayerStatus;
        INT mLayerHandle;
        INT mBoxTimeoutByTimerTick;   //default 50ms
        LHANDLE mTimerHandle;
        STRING mBoxTitle;
        STRING mBoxContent;
        BOOL mIsModal;
        Page *mBoxPage;
        LPVOID mpBoxDc;
        LTGUI_MUTEX *mpMutex;
};

#define MSGBOX                (BaseMessageBox::GetInstance())
}
#endif //__BASEMESSAGEBOX_H__

