/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOCANVAS_H__
#define __INOCANVAS_H__

#include "inocontrol.h"

namespace INONEGUI_FRAMEWORK {

class InoCanvas;
class ICanvasListener {  
public:  
    virtual ~ICanvasListener() { }  
    virtual INT OnCanvasEvent(InoCanvas *pCanvas, UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL) = 0; 
    VOID UserNotifyDataChanged(InoCanvas *pCanvas, RECT *pDirtyRc = NULL);
};  

class api_out InoCanvas : public InoControl
{
public:
        InoCanvas();
        virtual ~InoCanvas();
        INT GetControlType();
        virtual LRESULT OnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        virtual INT OnDraw(PHDC pHDc, RECT stArea,LPVOID pResAdapter = NULL,INT iFlag = CTRL_DRAWFLAG_ALL);
        VOID SetCanvasListener(ICanvasListener *pListener); 
        friend VOID ICanvasListener::UserNotifyDataChanged(InoCanvas *pCanvas, RECT *pDirtyRc);
        VOID DrawPixel(POINT stPt1,UINT32 iColor = COLOR_RED);
        VOID DrawLine(POINT stPt1, POINT stPt2, UINT32 iColor = COLOR_RED, UINT8 iWidth = 1);
        VOID DrawRect(RECT stRect, DWORD iColor = COLOR_RED ,UINT8 iWidth = 1);
        VOID DrawTrigon(POINT stPt1, POINT stPt2, POINT stPt3, DWORD iColor= COLOR_RED,UINT8 iWidth = 1);
        VOID DrawCurve(DOUBLE *vX, DOUBLE *vY, INT iCnt, DWORD iColor= COLOR_RED,UINT8 iWidth = 1, INT iStep = 4);
        VOID DrawPolygon(SINT16 *vX, SINT16 *vY, INT iCnt, DWORD iColor = COLOR_RED,UINT8 iWidth = 1);
        VOID DrawArc(INT iCx, INT iCy,INT iRadius, INT iSAngle, INT iEAngle, DWORD iColor = COLOR_RED ,UINT8 iWidth = 1);
        VOID DrawCircle(INT iCx, INT iCy,INT iRadius ,DWORD iColor= COLOR_RED ,UINT8 iWidth = 1);
        VOID DrawPie(INT iCx, INT iCy,INT iRadius, INT iSAngle, INT iEAngle,DWORD iColor= COLOR_RED,UINT8 iWidth = 1);
        BOOL DrawImage( STRING strImage, POINT stOffPt1 = {0,0}, INT iImgAlign = ALIGN_HVCENTER, INT iBlendMode = RD_BLENDMODE_BLEND);
        BOOL InitalRotoZoomImage(STRING strImage);
        BOOL InitalRotoZoomImageByBuffer(BYTE *pBuf, INT iSize);
        LTGUI_SURFACE* GetRotoZoomImageSurface();
        BOOL ReleaseRotoZoomImage();
        BOOL RotoZoomCanvas(POINT stCasCPt,POINT stImgRPt,DOUBLE fRotoAngle, DOUBLE fZoom = 1.0f, INT iSmooth = RD_SMOOTHING_ON, INT iImgInitAngle = -90);
        VOID MutexCanvasSurface(BOOL bLock);
        LTGUI_SURFACE* GetCanvasSurface();
        VOID ClearCanvas();
private:
	
private: 
	ICanvasListener *mpCasListener;
	LTGUI_SURFACE *mpCanvasDc;
	LTGUI_SURFACE *mpImgCanvasDc; //rota img org dc
	LTGUI_SURFACE *mpRzCanvasDc; //roto zoom dc
	LTGUI_MUTEX	*mpMutex;
};

}//end namespace
#endif	//__InoCanvas_H__
