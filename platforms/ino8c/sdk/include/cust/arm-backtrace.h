/**
 *  @file arm-backtrace.h
 *  @brief Brief description
 */

#ifndef ARM_BACKTRACE_H__
#define ARM_BACKTRACE_H__
#include <unistd.h>
#include <signal.h>	    /* for signal */
#include <unwind.h>
#include <stdint.h>

#ifdef __cplusplus
	extern "C"
	{
#endif	// __cplusplus
void signal_handler(int signo);
#ifdef __cplusplus
	}
#endif	// __cplusplus

#endif
