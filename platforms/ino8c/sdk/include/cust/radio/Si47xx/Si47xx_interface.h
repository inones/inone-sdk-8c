
#ifndef __SI47XX_INTERFACE__
#define __SI47XX_INTERFACE__

#ifdef __cplusplus
extern "C" {
#endif
/**************************************

int Si47XX_init(void)

***************************************/
int Si47XX_init(void);
/**************************************

int Si47XX_deinit(void)

***************************************/
int Si47XX_deinit(void);
/**************************************

int Si47XX_FM_Tune_i

***************************************/
int Si47XX_FM_Tune_i(unsigned short iFreq);

/**************************************

Si47XX_FM_Seek()

***************************************/

int Si47XX_FM_Seek_i(int seekmode, unsigned short* pseek_channel_freq);

/**************************************

Si47XX_AM_Tune

***************************************/
int Si47XX_AM_Tune_i(unsigned short iFreq);

/**************************************

int Si47XX_AM_Seek

***************************************/

int Si47XX_AM_Seek_i(int seekmode, unsigned short* pseek_channel_freq);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //SI47XX_interface










