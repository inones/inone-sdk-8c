/********************************************************
Si47XX driver function claim
********************************************************/

#include "Si47xx_common.h"

/* common for RX and TX*/
void ResetSi47XX_2w(void);
T_ERROR_OP Si47XX_Power_Up(T_POWER_UP_TYPE power_up_type);
T_ERROR_OP Si47XX_Power_Down(void);
T_ERROR_OP Si47XX_Get_Rev(unsigned char *pREV_data, unsigned char numBytes);
T_ERROR_OP Si47XX_Set_Property_RCLK_PRESCALE(void);
T_ERROR_OP Si47XX_Set_Property_REFCLK_FREQ(void);


/*for FM RX*/
T_ERROR_OP Si47XX_Set_Property_FM_DEEMPHASIS(void);
T_ERROR_OP Si47XX_Set_Property_FM_Seek_Band_Top(void);  //default is 10790, set to 10800
T_ERROR_OP Si47XX_Set_Property_FM_Seek_Space(void);
T_ERROR_OP Si47XX_Set_Property_FM_SNR_Threshold(void);
T_ERROR_OP Si47XX_Set_Property_FM_RSSI_Threshold(void);
T_ERROR_OP Si47XX_Set_FM_Frequency(unsigned short channel_freq);
T_ERROR_OP Si47XX_FM_Seek(T_SEEK_MODE seek_mode, unsigned short *pChannel_Freq, unsigned char *SeekFail);
T_ERROR_OP Si47XX_FM_Seek_All(unsigned short *pChannel_All_Array, unsigned char Max_Length, unsigned char *pReturn_Length);
T_ERROR_OP Si47XX_FM_Get_RSSI(unsigned char *pRSSI);
T_ERROR_OP Si47XX_FM_Seek_MTK(unsigned short channel_number, unsigned char *Return_RSSI);

/*for AM RX*/
T_ERROR_OP Si47XX_Set_Property_AM_DEEMPHASIS(void);
T_ERROR_OP Si47XX_Set_Property_AM_Seek_Space(void);
T_ERROR_OP Si47XX_Set_Property_AM_SNR_Threshold(void);
T_ERROR_OP Si47XX_Set_Property_AM_RSSI_Threshold(void);
T_ERROR_OP Si47XX_Set_AM_Frequency(unsigned short channel_freq);
T_ERROR_OP Si47XX_AM_Seek(T_SEEK_MODE seek_mode, unsigned short *pChannel_Freq, unsigned char *SeekFail);
T_ERROR_OP Si47XX_AM_Seek_All(unsigned short *pChannel_All_Array, unsigned char Max_Length, unsigned char *pReturn_Length);

/*for FM TX*/
T_ERROR_OP Si47XX_Set_Property_INPUT_LEVEL(void);
T_ERROR_OP Si47XX_Set_Property_TX_PREEMPHASIS(void);
T_ERROR_OP Si47XX_Set_Property_TX_ACOMP_ENABLE(void);
T_ERROR_OP Si47XX_Set_TX_Power(unsigned short tune_power);
T_ERROR_OP Si47XX_Set_TX_Frequency(unsigned short tune_freq);
T_ERROR_OP Si47XX_Tune_Measure_RPS(unsigned short tune_freq, unsigned char *noise_level);  //only for Si4712/13/20/21
T_ERROR_OP Si47XX_Min_RNL_Channel_RPS(unsigned short start_channel, unsigned short stop_channel, unsigned short *Min_RNL_Channel);	//only for Si4712/13/20/21

/*for TX FCC requirements*/
T_ERROR_OP Si47XX_Set_Property_TX_ASQ_Level_Low(void);
T_ERROR_OP Si47XX_Set_Property_TX_ASQ_Level_High(void);
T_ERROR_OP Si47XX_Set_Property_TX_ASQ_Duration_Low(void);
T_ERROR_OP Si47XX_Set_Property_TX_ASQ_Duration_High(void);
T_ERROR_OP Si47XX_Set_Property_TX_ASQ_Int_Select(void);  //level low detection enable in the beginning
T_ERROR_OP Si47XX_Set_Property_TX_ASQ_Int_Enable(void); 
T_ERROR_OP Si47XX_TX_Audio_Level_Detect_ISR(void);

/*for RDS RX*/
T_ERROR_OP Si47XX_RDS_ISR(void);
T_ERROR_OP Si47XX_Set_Property_RDS_int_source(void);
T_ERROR_OP Si47XX_Set_Property_RDS_int_FIFO_count(void);
T_ERROR_OP Si47XX_Set_Property_RDS_config(void);
T_ERROR_OP Si47XX_Set_Property_GPO_IEN(void);

/*for RDS TX*/
T_ERROR_OP Si47XX_Set_Property_TX_Component_Enable(void);
T_ERROR_OP Si47XX_Set_Property_TX_RDS_int_source(void);
T_ERROR_OP Si47XX_Set_Property_TX_RDS_PI(unsigned short pi_code);
T_ERROR_OP Si47XX_Set_Property_TX_RDS_PS_MIX(void);
T_ERROR_OP Si47XX_Set_Property_TX_RDS_PS_MISC(unsigned char Force_B, unsigned char TP_code, unsigned char PTY);
T_ERROR_OP Si47XX_Set_Property_TX_RDS_PS_Repeat_Count(void);
T_ERROR_OP Si47XX_Set_Property_TX_RDS_PS_Message_Count(void);
T_ERROR_OP Si47XX_Set_Property_TX_RDS_PS_AF(void);
T_ERROR_OP Si47XX_Set_Property_TX_RDS_FIFO_Size(void);

T_ERROR_OP Si47XX_TX_RDS_PS(unsigned char psid, unsigned char *PS_string);  //4 charaters
T_ERROR_OP Si47XX_TX_RDS_Circular(unsigned char empty_circular_flag, unsigned char *RDS_group_string);  //3 blocks, B,C,D
T_ERROR_OP Si47XX_TX_RDS_FIFO(unsigned char empty_fifo_flag, unsigned char clear_int_flag, unsigned char *RDS_group_string);  //3 blocks, B,C,D
T_ERROR_OP Si47XX_TX_RDS_Clear_Circular(void);
T_ERROR_OP Si47XX_TX_RDS_FIFO_Status_Check(unsigned char clear_int_flag, unsigned char *fifo_available_number, unsigned char *int_status);
T_ERROR_OP Si47XX_RDS_TX_ISR(void);














