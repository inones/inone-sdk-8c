 /******************************************************
START condition:

SCLK: -----------|______
		     
SDIO:-----|_____________
       1  |   2  |  3

STOP condition:

SCLK: ____|--------------
		     
SDIO:____________|-------
      	1 |  2   |   3

DATA:

SCLK:_______________|---|________|----|_______
		     
SDIO:___________|------------|____________|----
	            	|<==========>|


RESET:

SENB:__|---------------------------------------------
SDIO:_________________________________________|------
RST:  ___________|------------------------------------
SCLK:__|---------------------------------------------
       |    1    |	     2	          	      |   3

********************************************************/

/********************************************************
following macro should be defined by customer
********************************************************/
#ifndef __SI47XX_COMMON_H__
#define __SI47XX_COMMON_H__

#include <unistd.h>
#if 0
#define DURATION_INIT_1 	100us mininum
#define DURATION_INIT_2	  100us minimum
#define DURATION_INIT_3 	100us minimum

#define DURATION_START_1	600ns minimum
#define DURATION_START_2	600ns minimum
#define DURATION_START_3	800ns minimum

#define DURATION_STOP_1  	800ns minimum
#define DURATION_STOP_2	  600ns minimum
#define DURATION_STOP_3	  1300ns minimum

#define DURATION_HIGH		900ns minimum
#define DURATION_LOW		1600ns minimum

#define POWER_SETTLING		110ms
#define RST_PIN				XX
#define SDIO_PIN			XX
#define SCLK_PIN			XX

#else //

#define DURATION_INIT_1 	1
#define DURATION_INIT_2	  	1
#define DURATION_INIT_3 	1

#define DURATION_START_1	6
#define DURATION_START_2	6
#define DURATION_START_3	8

#define DURATION_STOP_1  	8
#define DURATION_STOP_2	  	6
#define DURATION_STOP_3	  	1

#define DURATION_HIGH		9
#define DURATION_LOW		1

#define POWER_SETTLING		110

#define RST_PIN			"PF5"
#define SDIO_PIN		2
#define SCLK_PIN		3

#define NULL			((void*)0)
#define I2C_PATH		"/dev/i2c-2"
#define SI47XX_I2C_ADDR	0x63    //7bit address
#endif

/********************************************************
following macro related to MTK solution
********************************************************/

#define GPIO_INPUT		0		/* IO in input */
#define GPIO_OUTPUT		1		/* IO in output */

#define RST_LOW		GPIO_WriteIO(0, RST_PIN)	//set RST to low			
#define RST_HIGH	GPIO_WriteIO(1, RST_PIN)	//set RST to high
#define SDIO_DIR_OUT	GPIO_InitIO(GPIO_OUTPUT, SDIO_PIN)	//set SDIO to output mode
#define	RST_DIR_OUT	GPIO_InitIO(GPIO_OUTPUT, RST_PIN)	//set RST to output mode
#define	SCLK_DIR_OUT	GPIO_InitIO(GPIO_OUTPUT, SCLK_PIN)	//set SCLK to output mode
#define SDIO_DIR_IN	GPIO_InitIO(GPIO_INPUT, SDIO_PIN)	//set SDIO to input mode
#define SDIO_LOW	GPIO_WriteIO(0, SDIO_PIN)	//set SDIO to low
#define SDIO_HIGH	GPIO_WriteIO(1, SDIO_PIN)	//set SDIO to high
#define READ_SDIO	GPIO_ReadIO(SDIO_PIN)  		//read SDIO routine, should return 0 or 1
#define SCLK_LOW	GPIO_WriteIO(0, SCLK_PIN)	//set SCLK to low
#define SCLK_HIGH	GPIO_WriteIO(1, SCLK_PIN)	//set SCLK to high

#define	RST_PIN_INIT	GPIO_ModeSetup(RST_PIN, 0)	//init GPIO RST
#define	SDIO_PIN_INIT	GPIO_ModeSetup(SDIO_PIN, 0)	//init GPIO SDIO
#define	SCLK_PIN_INIT	GPIO_ModeSetup(SCLK_PIN, 0)	//init GPIO SCLK 

#define DELAY(DURATION)		{volatile unsigned short i; for(i = 1; i <= DURATION/10; i++){usleep(10*1000);}}

/********************************************************
typedef and function claim
********************************************************/

typedef enum OPERA_MODE {
    READ = 1,
    WRITE = 2
} T_OPERA_MODE;

typedef enum ERROR_OP {
    OK = 1,
    I2C_ERROR = 2,
    LOOP_EXP_ERROR = 3,
    ERROR = 4
} T_ERROR_OP;

typedef enum POWER_UP_TYPE {
    FM_RECEIVER = 1,
    FM_TRNSMITTER = 2,
    AM_RECEIVER = 3
} T_POWER_UP_TYPE;

typedef enum SEEK_MODE {
    SEEKDOWN_HALT = 1,
    SEEKDOWN_WRAP = 2,
    SEEKUP_HALT = 3,
    SEEKUP_WRAP = 4
} T_SEEK_MODE;


/*Si4720 internal use routine claim*/
unsigned char OperationSi47XX_2w(T_OPERA_MODE operation, unsigned char *data, unsigned char numBytes);
T_ERROR_OP Si47XX_Wait_STC(void);

#endif //__SI47XX_COMMON_H__

