/**
 *  @file CRadioHelper.h
 *  @brief Radio helper class
 */
#ifndef _CRadioHelper_H_
#define _CRadioHelper_H_
#include "global.h"
#include "Si47xx_interface.h"

namespace middle_layer {

typedef enum {
	SI47XX_RADIO = 1,
} radiomodule_t;

class dll_export CRadioHelper
{
public:
	/**
	 *  @brief Brief description
	 *  
	 *  @return Return description
	 *  
	 *  @details More details
	 */
	~CRadioHelper();
	CRadioHelper(radiomodule_t radiomod);
	static CRadioHelper* getInstance(radiomodule_t radiomod=SI47XX_RADIO);
	int radio_FM_Tune(unsigned short iFreq);
	int radio_FM_Seek(int iSeekMode,unsigned short* iFreq);
	int radio_AM_Tune(unsigned short iFreq);
	int radio_AM_Seek(int iSeekMode,unsigned short* iFreq);
	radiomodule_t radio_getmodule_type();
	
private:
	radiomodule_t m_radiomodule;
	static CRadioHelper *s_pInstance;
};

}
#endif