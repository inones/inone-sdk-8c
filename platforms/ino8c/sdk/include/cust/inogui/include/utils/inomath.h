/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

/**
 * @file inomath.h
 *
 */
#ifndef __LTMATH_H__
#define __LTMATH_H__


/*********************
 *      INCLUDES
 *********************/
#include "inogui_header_min.h"

/*********************
 *      DEFINES
 *********************/
#define LT_MATH_MIN(a, b) ((a) < (b) ? (a) : (b))
#define LT_MATH_MIN3(a, b, c) (LT_MATH_MIN(LT_MATH_MIN(a,b), c))
#define LT_MATH_MIN4(a, b, c, d) (LT_MATH_MIN(LT_MATH_MIN(a,b), LT_MATH_MIN(c,d)))

#define LT_MATH_MAX(a, b) ((a) > (b) ? (a) : (b))
#define LT_MATH_MAX3(a, b, c) (LT_MATH_MAX(LT_MATH_MAX(a,b), c))
#define LT_MATH_MAX4(a, b, c, d) (LT_MATH_MAX(LT_MATH_MAX(a,b), LT_MATH_MAX(c,d)))

#define LT_MATH_ABS(x) ((x) > 0 ? (x) : (-(x)))

#define LT_MATH_UDIV255(x) ((UINT32)((UINT32) (x) * 0x8081) >> 0x17)

#define LT_IS_SIGNED(t) (((t)(-1)) < ((t) 0))
#define LT_UMAX_OF(t) (((0x1ULL << ((sizeof(t) * 8ULL) - 1ULL)) - 1ULL) | (0xFULL << ((sizeof(t) * 8ULL) - 4ULL)))
#define LT_SMAX_OF(t) (((0x1ULL << ((sizeof(t) * 8ULL) - 1ULL)) - 1ULL) | (0x7ULL << ((sizeof(t) * 8ULL) - 4ULL)))
#define LT_MAX_OF(t) ((unsigned long) (LT_IS_SIGNED(t) ? LT_SMAX_OF(t) : LT_UMAX_OF(t)))

#define LT_TRIGO_SIN_MAX 32767
#define LT_TRIGO_SHIFT 15 /**<  >> LT_TRIGO_SHIFT to normalize*/

#define LT_BEZIER_VAL_MAX 1024 /**< Max time in Bezier functions (not [0..1] to use integers) */
#define LT_BEZIER_VAL_SHIFT 10 /**< log2(LT_BEZIER_VAL_MAX): used to normalize up scaled values*/



/**********************
 *      TYPEDEFS
 **********************/

typedef struct {
    UINT16 i;
    UINT16 f;
} LtSqrtType;


/**********************
 * GLOBAL PROTOTYPES
 **********************/

//! @cond Doxygen_Suppress
/**
 * Return with sinus of an angle
 * @param angle
 * @return sinus of 'angle'. sin(-90) = -32767, sin(90) = 32767
 */
 SINT16 lt_trigo_sin(SINT16 angle);
 extern "C" SINT16 c_lt_trigo_sin(SINT16 angle);
//! @endcond

/**
 * Calculate a value of a Cubic Bezier function.
 * @param t time in range of [0..BEZIER_VAL_MAX]
 * @param u0 start values in range of [0..BEZIER_VAL_MAX]
 * @param u1 control value 1 values in range of [0..BEZIER_VAL_MAX]
 * @param u2 control value 2 in range of [0..BEZIER_VAL_MAX]
 * @param u3 end values in range of [0..BEZIER_VAL_MAX]
 * @return the value calculated from the given parameters in range of [0..BEZIER_VAL_MAX]
 */
SINT32 lt_bezier3(UINT32 t, SINT32 u0, SINT32 u1, SINT32 u2, SINT32 u3);

/**
 * Calculate the atan2 of a vector.
 * @param x
 * @param y
 * @return the angle in degree calculated from the given parameters in range of [0..360]
 */
UINT16 lt_atan2(int x, int y);


//! @cond Doxygen_Suppress

/**
 * Get the square root of a number
 * @param x integer which square root should be calculated
 * @param q store the result here. q->i: integer part, q->f: fractional part in 1/256 unit
 * @param mask: optional to skip some iterations if the magnitude of the root is known.
 * Set to 0x8000 by default.
 * If root < 16: mask = 0x80
 * If root < 256: mask = 0x800
 * Else: mask = 0x8000
 */
 void lt_sqrt(UINT32 x, LtSqrtType * q, UINT32 mask);

//! @endcond

/**
 * Calculate the integer exponents.
 * @param base
 * @param power
 * @return base raised to the power exponent
 */
SINT64 lt_pow(SINT64 base, SINT8 exp);

/**
 * Get the mapped of a number given an input and output range
 * @param x integer which mapped value should be calculated
 * @param min_in min input range
 * @param max_in max input range
 * @param min_out max output range
 * @param max_out max output range
 * @return the mapped number
 */
SINT16 lt_map(SINT32 x, SINT32 min_in, SINT32 max_in, SINT32 min, SINT32 max);

/**********************
 *      MACROS
 **********************/
#endif  //__LTMATH_H__

