/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

/**
*Base header include
*Base uitle define
*/
#ifndef __INOGUI_H__
#define __INOGUI_H__

#define __CPU64__

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <malloc.h>
#include <stdio.h>
#include <fstream>

#ifdef WIN32
#include <windows.h>
#include <direct.h>
#ifdef LTGUILIB_EXPORTS
#define api_out __declspec(dllexport)
#else
#define api_out __declspec(dllimport)
#endif
#else
#include <dlfcn.h>
#include <unistd.h>
#include <sys/time.h>
#define api_out
#endif
#include <sys/syscall.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <linux/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

#include <string.h>
#include <string>
#include <vector>
#include<algorithm>
#include <list>
using namespace std;

#if D_USE_JSONCPP
#include "json/json.h"
typedef Json::Writer JsonWriter;
typedef Json::Reader JsonReader;
typedef Json::Value  JsonValue;
#ifdef WIN32
#pragma comment(lib, "json.lib")
#endif
#elif D_USE_CJSON
#include "cjson/cJSON.h"
#endif//D_USE_JSONCPP

#include "sysport.h"

namespace INONEGUI_FRAMEWORK {
#include "inoneguikeys.h"
#include "scancode.h"
#include "common.h"
#include "control_def.h"

#ifndef CopyMemory
#define CopyMemory memcpy
#endif

#ifndef ZeroMemory
#define ZeroMemory(d, l) memset(d, 0, l)
#endif

#define SAFE_DELETE(x)	\
if (x != NULL)		\
{					\
	delete x;		\
	x = NULL;		\
}

#define SAFE_DELETE_ARRAY(x)	\
if (x != NULL)		        \
{					        \
	delete []x;		        \
	x = NULL;		        \
}

//for public call
VOID InoSleep(INT iMs);
DWORD InoGetTickCount();

BOOL InoIsTouchMsg(UINT iMsg);
BOOL InoIsKeyMsg(UINT iMsg);
/** 
*  @brief Brief description 
*   		Get focus app name and layout id(page id),
               Notice:Don't loop inovke it when piLayoutID != NULL, maybe have too cpu loading.
*  @param [in] strActivityName: app name
*  @param [in] iLayoutID: app layout id
*  @return Return description 
*   		FALSE(0): FAIL, TRUE:OK   
*  @details More details 
*/
BOOL InoGetFocusAppNameLayoutId(STRING &strActivityName, INT *piLayoutID = NULL);
/** 
*  @brief Brief description 
*   		start activity for no activity/page stack app
*  @param [in] strActivityName: app name
*  @param [in] iLayoutID: app layout id
*  @param [in] pParam: pass to app page paramter
*  @param [in] iActFlag: launch page flag,see to ActFlag
*  @return Return description 
*   		ERR_INVALID(-1): FAIL, the other value is OK    
*  @details More details 
*/
INT InoStartSimpleActivity(STRING strActivityName, INT iLayoutID = ACTIVITY_LAYOUT_MAIN, LPVOID pParam = NULL , INT iActFlag = 0);
/** 
*  @brief Brief description 
*   		Stop activity for no activity/page stack app
*  @param [in] strActivityName: app name
*  @param [in] iActFlag: launch page flag,see to ActFlag
*  @return Return description 
*   		ERR_INVALID(-1): FAIL, the other value is OK    
*  @details More details 
*/
INT InoStopSimpleActivity(STRING strActivityName, INT iActFlag = 0);
/** 
*  @brief Brief description 
*   		Launch activity, with activity/page stack manager
*  @param [in] strActivityName: app name
*  @param [in] iLayoutID: app layout id
*  @param [in] pParam: pass to app page paramter
*  @param [in] iActFlag: launch page flag,see to ActFlag
*  @return Return description 
*   		ERR_INVALID(-1): FAIL, the other value is OK    
*  @details More details 
*/
INT InoLaunchActivity(STRING strActivityName,INT iLayoutID = ACTIVITY_LAYOUT_MAIN, LPVOID pParam = NULL,INT iActFlag = 0);
/** 
*  @brief Brief description 
*   		Only Send sync msg to ActivityManager App,then AM pass msg to focus page
*  @param [in] iMsg Description for wParam 
*  @param [in] wParam Description for wParam 
*  @param [in] lParam Description for lParam 
*  @return Return description 
*   		ERR_INVALID(-1): FAIL, the other value is OK    
*  @details More details 
*/
LRESULT InoSendMessage(UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, HWND hWnd = NULL);
/** 
*  @brief Brief description 
*   		Only send async msg to ActivityManager App,then AM pass msg to focus page
*  @param [in] strAppName Description for app name
*  @param [in] iMsg Description for wParam 
*  @param [in] wParam Description for wParam 
*  @param [in] lParam Description for lParam 
*  @return Return description 
*   		ERR_INVALID(-1): FAIL, ERR_OK(0):OK   
*  @details More details 
*/
INT InoPostMessage(UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, HWND hWnd = NULL);
/** 
*  @brief Brief description 
*   		Send sync msg to the other app, be similar to function invoke.
*  @param [in] strAppName Description for app name
*  @param [in] iMsg Description for wParam 
*  @param [in] wParam Description for wParam 
*  @param [in] lParam Description for lParam 
*  @return Return description 
*   		ERR_INVALID(-1): FAIL, the other value is OK, eg. other interesting values     
*  @details More details 
*/
LRESULT InoSendMessage(STRING strAppName, UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL);
/** 
*  @brief Brief description 
*   		Send async msg to the other app
*  @param [in] strAppName Description for app name
*  @param [in] iMsg Description for wParam 
*  @param [in] wParam Description for wParam 
*  @param [in] lParam Description for lParam 
*  @return Return description 
*   		ERR_INVALID(-1): FAIL, ERR_OK(0):OK   
*  @details More details 
*/
INT InoPostMessage(STRING strAppName, UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL);
STRING InoToString(INT ivalue, INT iRadix = 10);
STRING InoToFloatString(DOUBLE dvalue, INT iDigits = 20);

#define LT_CHECK_RET(condition, ret)                        \
            if (!(condition)) return ret

#define LT_CHECK(condition)                                 \
            if (!(condition)) return
                
#define BIT(x) (1UL << (x))
#define CLRBIT_ALL(value)				     			(value=0)
#define SETBIT_ALL(value,bits)		            			(value = (bits))
#define SETBIT_BY_OFFSET(value,offset)         			(value|=BIT(offset))
#define CLRBIT_BY_OFFSET(value,offset)         			(value&=~(BIT(offset)))
#define SETBIT_BY_BIT(value,bit)                     			(value|=(bit))
#define CLRBIT_BY_BIT(value,bit)                    			 (value&=~(bit))
#define ISSET_BY_BIT(value,bit)                       			(((value&(bit)) == (bit))?TRUE:FALSE)
#define ISSET_BY_MASK(value,mask)               			((((value)&(mask)) > 0)?TRUE:FALSE)
#define ISVAL_BY_MASK(value,mask,val)        			(((value&(mask)) == (val))?TRUE:FALSE)
#define ISVAL_BY_OFFSET(value,mask,offset,val)        	((((value&(mask))>>(offset)) == (val))?TRUE:FALSE)
#define GETVAL_BY_MASK(value,mask)            			(value&(mask))
#define GETVAL_BY_OFFSET(value,mask,offset)			((value&(mask))>>(offset))
#define SETVAL_BY_OFFSET(value,mask,offset,val)		(value=((value&(~mask))|((val <<offset)&mask)))

#define MAKEDWORD(q1, q2, q3, q4)                       \
    ((DWORD)(                                           \
        (((DWORD)(QDWORD)(q1))) |                       \
        (((DWORD)((QDWORD)(q2))) << QDWORD_SHIFT) |     \
        (((DWORD)((QDWORD)(q3))) << (QDWORD_SHIFT*2)) | \
        (((DWORD)((QDWORD)(q4))) << (QDWORD_SHIFT*3))   \
    ))

#define FIRST_QDWORD(dw)    ((QDWORD)(((DWORD)(dw))))
#define SECOND_QDWORD(dw)   ((QDWORD)((((DWORD)(dw)) >> QDWORD_SHIFT)))
#define THIRD_QDWORD(dw)    ((QDWORD)((((DWORD)(dw)) >> (QDWORD_SHIFT*2))))
#define FOURTH_QDWORD(dw)   ((QDWORD)((((DWORD)(dw)) >> (QDWORD_SHIFT*3))))

#define LOBYTE(w)           ((BYTE)(w))
#define HIBYTE(w)           ((BYTE)(((WORD)(w) >> NR_BITS_BYTE) & BITMASK_BYTE))

#define MAKEWORD(low, high)     \
    ((WORD)(((BYTE)(low)) | (((WORD)((BYTE)(high))) << NR_BITS_BYTE)))

#define MAKEWORD16(low, high)   \
    ((WORD16)(((BYTE)(low)) | (((WORD16)((BYTE)(high))) << 8)))


#define MAKEDWORD32(first, second, third, fourth)   \
    ((DWORD32)(                                     \
        ((BYTE)(first)) |                           \
        (((DWORD32)((BYTE)(second))) << 8) |        \
        (((DWORD32)((BYTE)(third))) << 16) |        \
        (((DWORD32)((BYTE)(fourth))) << 24)         \
    ))

#define MAKEWPARAM(first, second, third, fourth)    \
    ((WPARAM)(                                      \
        ((BYTE)(first)) |                            \
        (((WPARAM)((BYTE)(second))) << 8) |         \
        (((WPARAM)((BYTE)(third))) << 16) |         \
        (((WPARAM)((BYTE)(fourth))) << 24)          \
    ))

#define FIRSTBYTE(w)        ((BYTE)(w))
#define SECONDBYTE(w)        ((BYTE)(((DWORD32)(w)) >> 8))
#define THIRDBYTE(w)        ((BYTE)(((DWORD32)(w)) >> 16))
#define FOURTHBYTE(w)        ((BYTE)(((DWORD32)(w)) >> 24))

#define LOWORD(l)           ((WORD)(DWORD)(l))
#define LOWORD32(l)         ((WORD16)(DWORD32)(l))

#define HIWORD(l)           \
    ((WORD)((((DWORD)(l)) >> NR_BITS_WORD) & BITMASK_WORD))
#define HIWORD32(l)           \
    ((WORD16)((((DWORD32)(l)) >> NR_BITS_WORD16) & BITMASK_WORD16))

#define LOSWORD(l)          ((SWORD)(DWORD)(l))
#define LOSWORD32(l)         ((SWORD16)(DWORD32)(l))

#define HISWORD(l)          \
    ((SWORD)((((DWORD)(l)) >> NR_BITS_WORD) & BITMASK_WORD))
#define HISWORD32(l)          \
    ((SWORD16)((((DWORD32)(l)) >> NR_BITS_WORD16) & BITMASK_WORD16))

#define MAKELONG32(low, high)   \
    ((DWORD32)(((WORD16)(low)) | (((DWORD32)((WORD16)(high))) << 16)))
    
#define MAKELONG(low, high)     \
    ((DWORD)(((WORD)(low)) | (((DWORD)((WORD)(high))) << NR_BITS_WORD)))

//Color
#define GETBVALUE(rgba)      ((BYTE)(rgba))
#define GETGVALUE(rgba)      ((BYTE)(((DWORD32)(rgba)) >> 8))
#define GETRVALUE(rgba)      ((BYTE)((DWORD32)(rgba) >> 16))
#define GETAVALUE(rgba)      ((BYTE)((DWORD32)(rgba) >> 24))

#define MAKERGBA(r, g, b, a)                    \
                (((DWORD32)((BYTE)(b)))         \
                | ((DWORD32)((BYTE)(g)) << 8)   \
                | ((DWORD32)((BYTE)(r)) << 16)  \
                | ((DWORD32)((BYTE)(a)) << 24)) 

#define MAKERGB(r, g, b) MAKERGBA((r), (g), (b), 255)

#define STRINGRECT(rect)                                 (rect).x,(rect).y,(rect).w,(rect).h
#define STRINGRECTPAD(rectpad)                    (rectpad).l,(rectpad).t,(rectpad).r,(rectpad).b

inline POINT GetPoint(WPARAM wParam){
        POINT pt;
        pt.x = HISWORD32(wParam);
        pt.y = LOSWORD32(wParam);
        return pt;
}

inline WPARAM PutPoint(INT x, INT y){
        WPARAM wparam;
        wparam = MAKELONG32(y, x);
        return wparam;
}

inline BOOL IsRectEmpty (const RECT* pRc)
{
    LT_CHECK_RET(pRc, FALSE);
    if( pRc->w == 0 ) return TRUE;
    if( pRc->h == 0 ) return TRUE;
    return FALSE;
}

#define MS2TICK(X)                  (X/LTGUI_TIMER_TICKMS)
#define MIN(X,Y)                        ((X)<(Y)?(X):(Y))
#define MAX(X,Y)                        ((X)>(Y)?(X):(Y))
#define TOSTRING(A)			#A
#define TABLESIZE(table)    (sizeof(table)/sizeof(table[0]))

}//end namespace

#endif // __INOGUI_H__