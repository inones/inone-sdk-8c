/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INONEGUIKEYS_H__
#define __INONEGUIKEYS_H__

/** What we really want is a mapping of every raw key on the keyboard.
 *  To support international keyboards, we use the range 0xA1 - 0xFF
 *  as international virtual keycodes.  We'll follow in the footsteps of X11...
 *  @brief The names of the keys
 */
typedef enum {
        /** @name ASCII mapped keysyms
         *  The keyboard syms have been cleverly chosen to map to ASCII
         */
        /*@{*/
	INO_KEY_UNKNOWN		= 0,
	INO_KEY_FIRST		= 0,
	INO_KEY_BACKSPACE		= 8,
	INO_KEY_TAB		= 9,
	INO_KEY_CLEAR		= 12,
	INO_KEY_RETURN		= 13,
	INO_KEY_PAUSE		= 19,
	INO_KEY_ESCAPE		= 27,
	INO_KEY_SPACE		= 32,
	INO_KEY_EXCLAIM		= 33,
	INO_KEY_QUOTEDBL		= 34,
	INO_KEY_HASH		= 35,
	INO_KEY_DOLLAR		= 36,
	INO_KEY_AMPERSAND		= 38,
	INO_KEY_QUOTE		= 39,
	INO_KEY_LEFTPAREN		= 40,
	INO_KEY_RIGHTPAREN		= 41,
	INO_KEY_ASTERISK		= 42,
	INO_KEY_PLUS		= 43,
	INO_KEY_COMMA		= 44,
	INO_KEY_MINUS		= 45,
	INO_KEY_PERIOD		= 46,
	INO_KEY_SLASH		= 47,
	INO_KEY_0			= 48,
	INO_KEY_1			= 49,
	INO_KEY_2			= 50,
	INO_KEY_3			= 51,
	INO_KEY_4			= 52,
	INO_KEY_5			= 53,
	INO_KEY_6			= 54,
	INO_KEY_7			= 55,
	INO_KEY_8			= 56,
	INO_KEY_9			= 57,
	INO_KEY_COLON		= 58,
	INO_KEY_SEMICOLON		= 59,
	INO_KEY_LESS		= 60,
	INO_KEY_EQUALS		= 61,
	INO_KEY_GREATER		= 62,
	INO_KEY_QUESTION		= 63,
	INO_KEY_AT			= 64,
	/* 
	   Skip uppercase letters
	 */
	INO_KEY_LEFTBRACKET	= 91,
	INO_KEY_BACKSLASH		= 92,
	INO_KEY_RIGHTBRACKET	= 93,
	INO_KEY_CARET		= 94,
	INO_KEY_UNDERSCORE		= 95,
	INO_KEY_BACKQUOTE		= 96,
	INO_KEY_a			= 97,
	INO_KEY_b			= 98,
	INO_KEY_c			= 99,
	INO_KEY_d			= 100,
	INO_KEY_e			= 101,
	INO_KEY_f			= 102,
	INO_KEY_g			= 103,
	INO_KEY_h			= 104,
	INO_KEY_i			= 105,
	INO_KEY_j			= 106,
	INO_KEY_k			= 107,
	INO_KEY_l			= 108,
	INO_KEY_m			= 109,
	INO_KEY_n			= 110,
	INO_KEY_o			= 111,
	INO_KEY_p			= 112,
	INO_KEY_q			= 113,
	INO_KEY_r			= 114,
	INO_KEY_s			= 115,
	INO_KEY_t			= 116,
	INO_KEY_u			= 117,
	INO_KEY_v			= 118,
	INO_KEY_w			= 119,
	INO_KEY_x			= 120,
	INO_KEY_y			= 121,
	INO_KEY_z			= 122,
	INO_KEY_DELETE		= 127,
	/* End of ASCII mapped keysyms */
        /*@}*/

	/** @name International keyboard syms */
        /*@{*/
	INO_KEY_WORLD_0		= 160,		/* 0xA0 */
	INO_KEY_WORLD_1		= 161,
	INO_KEY_WORLD_2		= 162,
	INO_KEY_WORLD_3		= 163,
	INO_KEY_WORLD_4		= 164,
	INO_KEY_WORLD_5		= 165,
	INO_KEY_WORLD_6		= 166,
	INO_KEY_WORLD_7		= 167,
	INO_KEY_WORLD_8		= 168,
	INO_KEY_WORLD_9		= 169,
	INO_KEY_WORLD_10		= 170,
	INO_KEY_WORLD_11		= 171,
	INO_KEY_WORLD_12		= 172,
	INO_KEY_WORLD_13		= 173,
	INO_KEY_WORLD_14		= 174,
	INO_KEY_WORLD_15		= 175,
	INO_KEY_WORLD_16		= 176,
	INO_KEY_WORLD_17		= 177,
	INO_KEY_WORLD_18		= 178,
	INO_KEY_WORLD_19		= 179,
	INO_KEY_WORLD_20		= 180,
	INO_KEY_WORLD_21		= 181,
	INO_KEY_WORLD_22		= 182,
	INO_KEY_WORLD_23		= 183,
	INO_KEY_WORLD_24		= 184,
	INO_KEY_WORLD_25		= 185,
	INO_KEY_WORLD_26		= 186,
	INO_KEY_WORLD_27		= 187,
	INO_KEY_WORLD_28		= 188,
	INO_KEY_WORLD_29		= 189,
	INO_KEY_WORLD_30		= 190,
	INO_KEY_WORLD_31		= 191,
	INO_KEY_WORLD_32		= 192,
	INO_KEY_WORLD_33		= 193,
	INO_KEY_WORLD_34		= 194,
	INO_KEY_WORLD_35		= 195,
	INO_KEY_WORLD_36		= 196,
	INO_KEY_WORLD_37		= 197,
	INO_KEY_WORLD_38		= 198,
	INO_KEY_WORLD_39		= 199,
	INO_KEY_WORLD_40		= 200,
	INO_KEY_WORLD_41		= 201,
	INO_KEY_WORLD_42		= 202,
	INO_KEY_WORLD_43		= 203,
	INO_KEY_WORLD_44		= 204,
	INO_KEY_WORLD_45		= 205,
	INO_KEY_WORLD_46		= 206,
	INO_KEY_WORLD_47		= 207,
	INO_KEY_WORLD_48		= 208,
	INO_KEY_WORLD_49		= 209,
	INO_KEY_WORLD_50		= 210,
	INO_KEY_WORLD_51		= 211,
	INO_KEY_WORLD_52		= 212,
	INO_KEY_WORLD_53		= 213,
	INO_KEY_WORLD_54		= 214,
	INO_KEY_WORLD_55		= 215,
	INO_KEY_WORLD_56		= 216,
	INO_KEY_WORLD_57		= 217,
	INO_KEY_WORLD_58		= 218,
	INO_KEY_WORLD_59		= 219,
	INO_KEY_WORLD_60		= 220,
	INO_KEY_WORLD_61		= 221,
	INO_KEY_WORLD_62		= 222,
	INO_KEY_WORLD_63		= 223,
	INO_KEY_WORLD_64		= 224,
	INO_KEY_WORLD_65		= 225,
	INO_KEY_WORLD_66		= 226,
	INO_KEY_WORLD_67		= 227,
	INO_KEY_WORLD_68		= 228,
	INO_KEY_WORLD_69		= 229,
	INO_KEY_WORLD_70		= 230,
	INO_KEY_WORLD_71		= 231,
	INO_KEY_WORLD_72		= 232,
	INO_KEY_WORLD_73		= 233,
	INO_KEY_WORLD_74		= 234,
	INO_KEY_WORLD_75		= 235,
	INO_KEY_WORLD_76		= 236,
	INO_KEY_WORLD_77		= 237,
	INO_KEY_WORLD_78		= 238,
	INO_KEY_WORLD_79		= 239,
	INO_KEY_WORLD_80		= 240,
	INO_KEY_WORLD_81		= 241,
	INO_KEY_WORLD_82		= 242,
	INO_KEY_WORLD_83		= 243,
	INO_KEY_WORLD_84		= 244,
	INO_KEY_WORLD_85		= 245,
	INO_KEY_WORLD_86		= 246,
	INO_KEY_WORLD_87		= 247,
	INO_KEY_WORLD_88		= 248,
	INO_KEY_WORLD_89		= 249,
	INO_KEY_WORLD_90		= 250,
	INO_KEY_WORLD_91		= 251,
	INO_KEY_WORLD_92		= 252,
	INO_KEY_WORLD_93		= 253,
	INO_KEY_WORLD_94		= 254,
	INO_KEY_WORLD_95		= 255,		/* 0xFF */
        /*@}*/

	/** @name Numeric keypad */
        /*@{*/
	INO_KEY_KP0		= 256,
	INO_KEY_KP1		= 257,
	INO_KEY_KP2		= 258,
	INO_KEY_KP3		= 259,
	INO_KEY_KP4		= 260,
	INO_KEY_KP5		= 261,
	INO_KEY_KP6		= 262,
	INO_KEY_KP7		= 263,
	INO_KEY_KP8		= 264,
	INO_KEY_KP9		= 265,
	INO_KEY_KP_PERIOD		= 266,
	INO_KEY_KP_DIVIDE		= 267,
	INO_KEY_KP_MULTIPLY	= 268,
	INO_KEY_KP_MINUS		= 269,
	INO_KEY_KP_PLUS		= 270,
	INO_KEY_KP_ENTER		= 271,
	INO_KEY_KP_EQUALS		= 272,
        /*@}*/

	/** @name Arrows + Home/End pad */
        /*@{*/
	INO_KEY_UP		= 273,
	INO_KEY_DOWN		= 274,
	INO_KEY_RIGHT		= 275,
	INO_KEY_LEFT		= 276,
	INO_KEY_INSERT		= 277,
	INO_KEY_HOME		= 278,
	INO_KEY_END		= 279,
	INO_KEY_PAGEUP		= 280,
	INO_KEY_PAGEDOWN		= 281,
        /*@}*/

	/** @name Function keys */
        /*@{*/
	INO_KEY_F1			= 282,
	INO_KEY_F2			= 283,
	INO_KEY_F3			= 284,
	INO_KEY_F4			= 285,
	INO_KEY_F5			= 286,
	INO_KEY_F6			= 287,
	INO_KEY_F7			= 288,
	INO_KEY_F8			= 289,
	INO_KEY_F9			= 290,
	INO_KEY_F10		= 291,
	INO_KEY_F11		= 292,
	INO_KEY_F12		= 293,
	INO_KEY_F13		= 294,
	INO_KEY_F14		= 295,
	INO_KEY_F15		= 296,
        /*@}*/

	/** @name Key state modifier keys */
        /*@{*/
	INO_KEY_NUMLOCK		= 300,
	INO_KEY_CAPSLOCK		= 301,
	INO_KEY_SCROLLOCK		= 302,
	INO_KEY_RSHIFT		= 303,
	INO_KEY_LSHIFT		= 304,
	INO_KEY_RCTRL		= 305,
	INO_KEY_LCTRL		= 306,
	INO_KEY_RALT		= 307,
	INO_KEY_LALT		= 308,
	INO_KEY_RMETA		= 309,
	INO_KEY_LMETA		= 310,
	INO_KEY_LSUPER		= 311,		/**< Left "Windows" key */
	INO_KEY_RSUPER		= 312,		/**< Right "Windows" key */
	INO_KEY_MODE		= 313,		/**< "Alt Gr" key */
	INO_KEY_COMPOSE		= 314,		/**< Multi-key compose key */
        /*@}*/

	/** @name Miscellaneous function keys */
        /*@{*/
	INO_KEY_HELP		= 315,
	INO_KEY_PRINT		= 316,
	INO_KEY_SYSREQ		= 317,
	INO_KEY_BREAK		= 318,
	INO_KEY_MENU		= 319,
	INO_KEY_POWER		= 320,		/**< Power Macintosh power key */
	INO_KEY_EURO		= 321,		/**< Some european keyboards */
	INO_KEY_UNDO		= 322,		/**< Atari keyboard has Undo */
        /*@}*/

	/* Add any other keys here */

	INO_KEY_LAST
} EINOKEYS;

/** Enumeration of valid key mods (possibly OR'd together) */
typedef enum {
	KMOD_NONE  = 0x0000,
	KMOD_LSHIFT= 0x0001,
	KMOD_RSHIFT= 0x0002,
	KMOD_LCTRL = 0x0040,
	KMOD_RCTRL = 0x0080,
	KMOD_LALT  = 0x0100,
	KMOD_RALT  = 0x0200,
	KMOD_LMETA = 0x0400,
	KMOD_RMETA = 0x0800,
	KMOD_NUM   = 0x1000,
	KMOD_CAPS  = 0x2000,
	KMOD_MODE  = 0x4000,
	KMOD_RESERVED = 0x8000
} SDLMod;

#define KMOD_CTRL	(KMOD_LCTRL|KMOD_RCTRL)
#define KMOD_SHIFT	(KMOD_LSHIFT|KMOD_RSHIFT)
#define KMOD_ALT	(KMOD_LALT|KMOD_RALT)
#define KMOD_META	(KMOD_LMETA|KMOD_RMETA)

#define INO_KEY_ENTER	INO_KEY_KP_ENTER		
#endif /* __INONEGUIKEYS_H__ */

