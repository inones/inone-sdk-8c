/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __IMEMANAGE_H__
#define __IMEMANAGE_H__
#include "inogui.h"
#include "log.h"
#include "activitymanage.h"
#include "ime.h"
#include "ime-pinyin.h"

namespace INONEGUI_FRAMEWORK {
#define IME_STOP_MSG      	ERR_OK_EVENTDONE
#define IME_CONTINUE_MSG  0

#define LTWDT_IMM_BEGIN            (MSG_GUI + 0x100)
#define LTWDT_IMM_UPDATE           (LTWDT_IMM_BEGIN + 1)
// int update_code = wParam
#define LTWDT_IMM_GETSELWORD       (LTWDT_IMM_BEGIN + 2)
// BOOL braw = wParam
#define LTWDT_IMM_ISSELWORDCTRLACTIVE       (LTWDT_IMM_BEGIN + 3)
#define LTWDT_IMM_SETSELWORDACTIVE       (LTWDT_IMM_BEGIN + 4)

#define LTWDT_IMM_DISABLE  0x01
#define LTWDT_IMM_ACTIVED  0x02
#define LTWDT_IMM_NULLIME  0x04
#define LTWDT_IMM_SETTING_IME 0x80000000

#define IME_SCANCODE_ISDIGIT(s)   ((s) >= SCANCODE_1 && (s) <= SCANCODE_0)
#define IME_SCANCODE_ISALPHA(s)  \
	(   ((s) >= SCANCODE_Q && (s) <= SCANCODE_P)   \
	 || ((s) >= SCANCODE_A && (s) <= SCANCODE_L)   \
	 || ((s) >= SCANCODE_Z && (s) <= SCANCODE_M)   \
	)

#if LTGUI_ISBIG_ENDIAN
#define ARCH_SWAP16(v16)  LTGUI_SwapLE16(v16)
#define ARCH_SWAP32(v32)  LTGUI_SwapLE32(v32)
#else
#define ARCH_SWAP16(v16)  (v16)
#define ARCH_SWAP32(v32)  (v32)
#endif

STATIC UINT32 ImeCallback(UINT32 uInterval, LPVOID lParam);
class api_out ImeManage{
    public:
        virtual ~ImeManage();
	STATIC ImeManage* GetInstance();
	BOOL AddIme(Ime *pIme);
	VOID SetEncoding(LPCTSTR lpEncode, Ime *pSetIme = NULL);
	BOOL SetIme(HWND HWnd, LPCTSTR lpImeName, LPCTSTR lpEncode = "UTF-8");
	Ime* GetCurIme(){return cur_ime;};
	BOOL NextIme();
	BOOL SendWords();
	BOOL SendKeys();
	INT ProcImeMsg(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);

    private:
        ImeManage();

    private:
	STATIC ImeManage *mpInstance;
	CB_LEN_FIRST_CHAR len_first_char; 
	CPtrArray mImeList;
	HWND   hwnd; 
	HWND   hwnd_target;
	Ime *cur_ime;
	DWORD  flags;
	INT    ime_x:16; 
	INT    ime_y:16;
        LTGUI_MUTEX *mpMutex;
};

#define IMM 		(ImeManage::GetInstance())
}
#endif //__IMEMANAGE_H__