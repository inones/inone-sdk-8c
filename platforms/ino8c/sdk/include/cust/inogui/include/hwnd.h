/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __HWND_H__
#define __HWND_H__

#include "inogui.h"

namespace INONEGUI_FRAMEWORK {
//MARCO DEFINE
#define HWND_INVALID		((HWND)-1)

class IWindowListener {  
public:  
    virtual ~IWindowListener() { }  
    virtual STRING GetWindowName() = 0; 
    virtual HWND GetWindowHandle() = 0; 
    virtual LRESULT OnDerivedWndProc (UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL) = 0;	 
}; 

//For HWND base class
class HandleWnd {  
public:  
    virtual ~HandleWnd() { }  
    HandleWnd() {mpWindowListener = NULL; }  
    IWindowListener* GetWindowListener(){return mpWindowListener;};
    INT RegWindowListener(IWindowListener *pListener){mpWindowListener = pListener;return ERR_OK;};
	
protected:
     IWindowListener *mpWindowListener;
}; 

}//end namespace

#endif // __HWND_H__