/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __WNDANIM_FADE_H__
#define __WNDANIM_FADE_H__

#include "inogui.h"
#include "wndanimators/wndanimator.h"

namespace INONEGUI_FRAMEWORK {

class WndAnim_Fade:
                                public IWndAnimator {  
public:
        WndAnim_Fade();
        virtual ~WndAnim_Fade();  
        EWndAnimatorType GetWndAnimatorType(); 
        INT InitialWndAnimator(BOOL bOpen = TRUE, FLOAT fPercent = 0, FLOAT fRatio = 1, EEasingType eEasing = EEASING_POW_IN, UINT32 iDuration = DEF_WNDANIM_DURATION);
        INT UpdateWndStep(); 
        INT DrawPrevWnd(LPVOID pDstDc, LPVOID pPrevDc, RECT sPaintRect);
        INT DrawCurrWnd(LPVOID pDstDc, LPVOID pCurrDc, RECT sPaintRect);
    
protected:
}; 

}//end namespace

#endif //__WNDANIM_FADE_H__