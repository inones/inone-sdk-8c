#ifndef __WNDANIM_BTOT_H__
#define __WNDANIM_BTOT_H__

#include "inogui.h"
#include "wndanimators/wndanimator.h"

namespace INONEGUI_FRAMEWORK {

class WndAnim_Btot:
                                public IWndAnimator {  
public:
        WndAnim_Btot();
        virtual ~WndAnim_Btot();  
        EWndAnimatorType GetWndAnimatorType(); 
        INT InitialWndAnimator(BOOL bOpen = TRUE, FLOAT fPercent = 0, FLOAT fRatio = 1, EEasingType eEasing = EEASING_POW_OUT, UINT32 iDuration = DEF_WNDANIM_DURATION);
        INT UpdateWndStep(); 
        INT DrawPrevWnd(LPVOID pDstDc, LPVOID pPrevDc, RECT sPaintRect);
        INT DrawCurrWnd(LPVOID pDstDc, LPVOID pCurrDc, RECT sPaintRect);
    
protected:
}; 

}//end namespace

#endif //__WNDANIM_BTOT_H__
