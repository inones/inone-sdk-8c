/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __WNDANIMMANAGER_H__
#define __WNDANIMMANAGER_H__

#include "inogui.h"
#include "wndanimators/wndanimator.h"
#include "wndanimators/wndanim_htranslate.h"
#include "wndanimators/wndanim_vtranslate.h"
#include "wndanimators/wndanim_fade.h"
#include "wndanimators/wndanim_centerscale.h"
#include "wndanimators/wndanim_ltor.h"
#include "wndanimators/wndanim_rtol.h"
#include "wndanimators/wndanim_ttob.h"
#include "wndanimators/wndanim_btot.h"
#include "wndanimators/wndanim_slideup.h"
#include "wndanimators/wndanim_slidedown.h"
#include "wndanimators/wndanim_slideleft.h"
#include "wndanimators/wndanim_slideright.h"

namespace INONEGUI_FRAMEWORK {

class WndAnimManager {  
public:  
        virtual ~WndAnimManager();
        STATIC WndAnimManager* GetInstance();
        VOID DoInit();
        VOID DeInit();
        IWndAnimator* CreateWndAnimator(EWndAnimatorType eAnimType, BOOL bOpen);
        INT WndAnimator_OnDraw(IWndAnimator *pWndAnim, LPVOID pDstDc, LPVOID pPrevDc, LPVOID pCurrDc, RECT sPaintRect);    
private:
        WndAnimManager();
        
private:
        STATIC WndAnimManager* mpInstance;
         IWndAnimator* mpWndAnimArray[MAX_EWND_ANIMATOR];
}; 

#define WAM              	(WndAnimManager::GetInstance())
}//end namespace

#endif // __WNDANIMMANAGER_H__

