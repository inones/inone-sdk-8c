/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __WNDANIMATOR_H__
#define __WNDANIMATOR_H__

#include "inogui.h"
#include "utils/easing.h"

namespace INONEGUI_FRAMEWORK {
#define DEF_WNDANIM_DURATION                  300
#define DEF_SLIDEWNDANIM_DURATION        600

#define USING_SYS_WNDANIM              0xFF                //using sys cfg anim
typedef enum _WndAnimatorType{
        EWND_ANIMATOR_NONE,
        EWND_ANIMATOR_HTRANSLATE,
        EWND_ANIMATOR_VTRANSLATE,
        EWND_ANIMATOR_FADE,
        EWND_ANIMATOR_CENTERSCALE,
        EWND_ANIMATOR_LTOR,
        EWND_ANIMATOR_RTOL,
        EWND_ANIMATOR_TTOB,    //top to bottom
        EWND_ANIMATOR_BTOT,
        EWND_ANIMATOR_SLIDEUP,
        EWND_ANIMATOR_SLIDEDOWN,
        EWND_ANIMATOR_SLIDELEFT,
        EWND_ANIMATOR_SLIDERIGHT,
        MAX_EWND_ANIMATOR,
}EWndAnimatorType;

enum _WndAtt{
        EWND_ATT_DRAWPREV_DONE = 0x0001,
        EWND_ATT_DRAWCURR_DONE = 0x0002,
};

class IWndAnimator {  
public:  
    virtual ~IWndAnimator() { }  
    virtual EWndAnimatorType GetWndAnimatorType() = 0; 
    virtual INT InitialWndAnimator(BOOL bOpen = TRUE, FLOAT fPercent = 0, FLOAT fRatio = 1, EEasingType eEasing = EEASING_LINEAR, UINT32 iDuration = DEF_WNDANIM_DURATION) = 0;
    virtual INT UpdateWndStep() = 0; 
    virtual INT DrawPrevWnd(LPVOID pDstDc, LPVOID pPrevDc, RECT sPaintRect) = 0;
    virtual INT DrawCurrWnd(LPVOID pDstDc, LPVOID pCurrDc, RECT sPaintRect) = 0;
    friend class WndAnimManager;
    
protected:
        BOOL mIsOpen;
        FLOAT mPercent;
        FLOAT mTimePercent;
        FLOAT mRatio;
        UINT32 mDuration;
        UINT32 mSTime;
        EEasingType mEasingV;
        UINT8 mWndAtt;
}; 

}//end namespace

#endif // __WNDANIMATOR_H__
