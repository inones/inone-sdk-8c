/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __MYRENDER_H__
#define __MYRENDER_H__

#include "inogui.h"
#include "inoanigif.h"

namespace INONEGUI_FRAMEWORK {
struct Part {
    bool stretchable;
    int pos;
    int len;
    Part()
        : stretchable(false)
        , pos(0)
        , len(0)
    {
    }
    Part(int pos, int len, bool stretchable)
        : stretchable(stretchable)
        , pos(pos)
        , len(len)
    {
    }
};

typedef struct _DcParam{
    INT iDcFlag;
}DcParam;

typedef enum _EDcFlag{
    EDC_FLAG_NONE 	        = 0x000000,
    EDC_FLAG_HWDC 	 = 0x000001,
}EDcFlag;

typedef enum _EDcColorDepth{
    EDC_COLOR_32BIT 	        = 32,
    EDC_COLOR_24BIT 	        = 24,
    EDC_COLOR_16BIT 	        = 16,
    EDC_COLOR_8BIT 	        = 8,
}EDcColorDepth;

/*
typedef struct _RDTxtStyleField
{
    UINT32 iFontSZ:8;
    UINT32 iAlign:6;
    UINT32 bBold:1;
    UINT32 bItalic:1;
}RDTxtStyleField;
typedef RDTxtStyleField* PRDTxtStyleField;
*/

#define RD_TXT_FONTSZ_OFF	0
#define RD_TXT_ALIGN_OFF	8
#define RD_TXT_BOLD_OFF	14
#define RD_TXT_ITALIC_OFF	15
#define RD_TXT_AUTOWARP_OFF	16
enum _ERDTxtStyle{
    RD_TXT_FONTSZ 	= 0x0000FF,
    RD_TXT_ALIGN 		= 0x003F00,
    RD_TXT_BOLD 		= 0x004000,
    RD_TXT_ITALIC 		= 0x008000,
    RD_TXT_AUTOWARP 	 = 0x010000,
};

#define RD_MASK_COLOR		       MAKERGBA(0, 0, 0, 0xff)
#define RD_BLACK_COLOR	       MAKERGBA(0, 0, 0, 0xff)
#define RD_CLEAR_COLOR		       MAKERGBA(0, 0, 0, 0)

//MASK:0x0F dc blend mode
//MASK:0xF0	source alpha scale value (orig alpha>> value)
#define 	RD_BLENDMODE_MASK 		0x0000F
#define 	RD_BLENDMODE_SAMASK	0x000F0	//source alpha scale down value MASK
#define 	RD_BLENDMODE_SAOFF		4		//source alpha scale down value offset
enum _ERDBlendMode{
    RD_BLENDMODE_NONE	= 0x00000,    //copy mode,using src all data
    RD_BLENDMODE_BLEND	= 0x00001,    //blend mode,alpha blend
    RD_BLENDMODE_SRCA	= 0x00002,    //blend mode,alpha using src
    RD_BLENDMODE_DSTA	= 0x00003,    //blend mode,alpha using dst
    RD_BLENDMODE_SRCM	= 0x00004,    //blend mode,src mask mode,remain area
    RD_BLENDMODE_SRCH	= 0x00005,    //blend mode,src hold mode,remove area
};

enum _ERDRotoSmooth{
	
    RD_SMOOTHING_OFF	= 0x00000,    //roto/zoom smooth off
    RD_SMOOTHING_ON		= 0x00001,    //roto/zoom smooth on
};
typedef struct {
    UINT8 *s_pixels;
    INT s_width;
    INT s_height;
    INT s_skip;
    UINT8 *d_pixels;
    INT d_width;
    INT d_height;
    INT d_skip;
    LTGUI_PIXELFORMAT *src;
    LTGUI_PIXELFORMAT *dst;
} LTGUI_BlitInfo;

typedef struct{
    STRING strResKey;
    LPVOID pDc;
}InoDcCacheType;

typedef struct{
    AG_Frame *pGifFrame;
    UINT16 iFrameIndex;
    UINT16 iFrameCnt;
    UINT32 iLastFrameTick;
}InoAniHandleType;

typedef vector<InoDcCacheType*>::iterator					IterVectorDcCache;
typedef vector<InoDcCacheType*>::reverse_iterator			RiterVectorDcCache;

//PACK TEXT RENDER STYLE
INT inline PackTextRenderStyle(INT iFontSZ, INT iAlign, INT iBold = 0, INT iItalic = 0, INT iAutoWarp = 0)
{
    return ((iFontSZ&RD_TXT_FONTSZ)|((iAlign<<RD_TXT_ALIGN_OFF)&RD_TXT_ALIGN) |\
        ((iBold<<RD_TXT_BOLD_OFF)&RD_TXT_BOLD)|((iItalic<<RD_TXT_ITALIC_OFF)&RD_TXT_ITALIC)|\
        ((iAutoWarp<<RD_TXT_AUTOWARP_OFF)&RD_TXT_AUTOWARP));
}

class api_out CRender
{
public:
    virtual ~CRender();
    STATIC CRender* GetInstance();

    INT DoInit(LPCTSTR pFontPath);
    INT DeInit();

    //Normal image
    INT DrawNorImgFromRes(LPVOID pDc, LPVOID hResAdapter, STRING strImgName, RECT stSrcRect, RECT stDstRect, INT iImgAlign = ALIGN_NOTHING, INT iBlendMode = RD_BLENDMODE_BLEND, POINT ptImgOff ={0,0}, BOOL bCached = TRUE);

    INT CalcNewRectOnAlign(IN RECT *pSrcRect, IN RECT *pDstRect, IN INT iAlignStyle, OUT RECT *pNewRect);
    LPVOID ImgfileToDCSize(LPCTSTR pImgPath, INT iWide, INT iHigh);

    LPVOID BufToDcSize(LPCTSTR pBuf, INT iSize, INT iSrcW, INT iSrcH, INT iDstW, INT iDstH);
    LPVOID RGB24ToDc(LPCTSTR pBuf, INT iSize, INT iSrcW, INT iSrcH);
    LPVOID RGB32ToDc(LPCTSTR pBuf, INT iSize, INT iSrcW, INT iSrcH);
    //Conver res or file to dc
    LTGUI_SURFACE* ConvertResToDc(LPVOID hResAdapter, STRING strImgName);

    //create Display canvas
    LPVOID CreateDc(INT iWidth, INT iHigh, INT iBpp = EDC_COLOR_32BIT, EDcFlag eDcFlag = EDC_FLAG_NONE);
    VOID ClearDc(LPVOID pDc, RECT *pClrRect = NULL, DWORD iColor = RD_CLEAR_COLOR);
    //Free dc
    VOID FreeDc(LPVOID pDc);

    //Draw txt
    LTGUI_SURFACE* GetTxtRenderDc(LPCTSTR pUtf8Txt,RECT stSrcRect, DWORD iColor, INT iStyle);
    INT DrawTxt(LPVOID pDc,LPCTSTR pUtf8Txt, RECT stSrcRect, RECT stDstRect, DWORD iColor, INT iStyle, INT iBlendMode = RD_BLENDMODE_BLEND);
    INT BitBlitTxtDcToDc(LPVOID pSrcDc, RECT stSrcRect, LPVOID pDstDc, RECT stDstRect, INT iStyle, POINT stOffsetPt = {0,0}, INT iBlendMode = RD_BLENDMODE_BLEND);
    INT GetSizeUtf8(INT iSize, LPCTSTR pTxt, INT *iWidth, INT *iHigh);
    //Draw Circle/Arc
    INT DrawCircle(LPVOID pDc, INT iCx, INT iCy,DWORD iColor,INT iRadius ,UINT8 iWidth = 1);
    INT FillCircle(LPVOID pDc, INT iCx, INT iCy,DWORD iColor,INT iRadius);
    INT DrawArc(LPVOID pDc, INT iCx, INT iCy,DWORD iColor,INT iRadius, INT iSAngle, INT iEAngle, UINT8 iWidth = 1);
    INT FillArc(LPVOID pDc, INT iCx, INT iCy,DWORD iColor,INT iInRadius, INT iOutRadius, INT iSAngle, INT iEAngle);
    INT DrawPie(LPVOID pDc, INT iCx, INT iCy,DWORD iColor,INT iRadius, INT iSAngle, INT iEAngle, UINT8 iWidth = 1);
    INT FillPie(LPVOID pDc, INT iCx, INT iCy,DWORD iColor,INT iRadius, INT iSAngle, INT iEAngle);
#ifdef WIN32
    char *win32G2U(const char *gb2312);

    char *win32U162U8(const wchar_t *utf16);
#endif

#ifndef WIN32
    char *linuxG2U(const char *gb2312);

    char *linuxA2U(const char *asci);

    char *linuxU82U16(const char *utf8);

    char *linuxU162U8(const char *utf16);

    char *linuxU82GB2312(const char *utf8);
#endif
    INT DrawCurve(LPVOID pDc, DOUBLE *vX, DOUBLE *vY, INT iCnt, INT iStep, DWORD iColor,UINT8 iWidth = 1);
    INT DrawPolygon(LPVOID pDc, SINT16 *vX, SINT16 *vY, INT iCnt, DWORD iColor,UINT8 iWidth = 1);
    INT FillPolygon(LPVOID pDc, SINT16 *vX, SINT16 *vY, INT iCount, DWORD iColor);
    INT DrawTrigon(LPVOID pDc, POINT stPt1, POINT stPt2, POINT stPt3, DWORD iColor,UINT8 iWidth = 1);
    INT FillTrigon(LPVOID pDc, POINT stPt1, POINT stPt2, POINT stPt3, DWORD iColor);
    INT DrawPixel(LPVOID pDc, POINT stPt, DWORD iColor);
    INT DrawLine(LPVOID pDc, POINT stPt1, POINT stPt2, DWORD iColor, UINT8 iWidth = 1);
    INT DrawRect(LPVOID pDc, RECT stRect, DWORD iColor,UINT8 iWidth = 1);
    INT FillRect(LPVOID pDc, RECT stRect, DWORD iColor);

    //self define blit
    STATIC BOOL IsValidBlitRect(RECT &rRect);
    STATIC INT SetBlitInfo(LTGUI_BlitInfo *pBlitInfo, LTGUI_SURFACE *src, LTGUI_RECT *srcrect , LTGUI_SURFACE *dst, LTGUI_RECT *dstrect);
    STATIC VOID BlitRGBtoRGBPixelAlphaMode(LTGUI_BlitInfo *info, INT iAlphaMode);
    INT MyBlitDc(LPVOID pSrc, RECT stSrcRect, LPVOID pDst, RECT stDstRect, INT iBlendMode = RD_BLENDMODE_BLEND);

    //SetSurface alpha
    //bAlphaForValidRGB: TRUE->Only modify No zero RGB pixel
    INT MySetSurfaceAlpha(LPVOID pSrc, BYTE cAlpha, RECT stRect, BOOL bAlphaForValidRGB = FALSE, BOOL bSelfBlend = FALSE);
    INT MyScaleSurfaceAlpha(LPVOID pSrc, INT iScaleV, RECT stRect,BOOL bAlphaForValidRGB = TRUE);
    INT MyCheckSurfaceAlpha(LPVOID pSrc, RECT stRect );
       
    //Setting surface alpha
    INT MyDarkSurfaceAlpha(LPVOID pSrc, BYTE cLevel, RECT stRect);

    INT SaveDctoFile(LPVOID pSrc, LPCTSTR pFile);
    LPVOID GenSurface(BYTE *pBuf, INT iSize);
    UINT16 CalculatePitch(LPVOID pDc);
    INT DrawBox(LPVOID pDc, RECT stRect, DWORD iColor,INT iRadius = 0);
    INT DrawBoxToDc(LPVOID pDestDc, RECT stSrcRect, RECT stDestRect,DWORD iColor,INT iRadius = 0);
    INT PutPixel(LTGUI_SURFACE *pSurface,UINT32 iX, UINT32 iY, DWORD iColor);
    UINT32 GetPixel(LTGUI_SURFACE *surface,UINT32 x, UINT32 y);

    //Roto and zoom Dc
    LTGUI_SURFACE* RotoZoomDc(LTGUI_SURFACE *pSrcDc ,DOUBLE fRotoAngle, DOUBLE fZoom = 1.0f,  INT iSmooth = RD_SMOOTHING_ON,  POINT stSrcRot={0,0},  POINT stDstRot ={0,0}, LTGUI_SURFACE *pDstDc = NULL);

    //Animation interface
    InoAniHandleType* DoInitAnimation(LPVOID hResAdapter, STRING strImgName);
    BOOL CheckAnimationTick(InoAniHandleType *pAniHandle);
    INT DoAnimation(LPVOID pDc, LPVOID hResAdapter, InoAniHandleType *pAniHandle, RECT stSrcRect, RECT stDstRect, INT iImgAlign = ALIGN_NOTHING, INT iBlendMode = RD_BLENDMODE_BLEND);
    INT DeInitAnimation(InoAniHandleType *pAniHandle);

    //Dc cache
    BOOL RmDcFromCache(STRING strResPath);
private:
    CRender();
    INT Resize9Patch(LTGUI_SURFACE *pDc, LTGUI_SURFACE *image, RECT stDstRect);     
    BOOL IsStretchableMarker(UINT32 pixel);

    //Dc cache
    VOID InitialDcCache();
    INT UnInitialDcCache();
    BOOL IsCanDcCache(INT iBlendMode);
    INT PushDcToCache(STRING strResKey, LTGUI_SURFACE *pDc);
    LPVOID HitDcByRes(STRING strResKey);

private:
    STATIC CRender* mpInstance;
    STATIC UINT8 mFontSZ;
    LPVOID mpFont;
    vector<InoDcCacheType*> mCachedDc;
    LTGUI_MUTEX  *mpMutex;
};

#define RD                          (CRender::GetInstance())
}//end namespace

#endif // __MYRENDER_H__
