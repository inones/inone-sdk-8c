/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __CHARSET_CONVERT_H__
#define __CHARSET_CONVERT_H__
#include "inogui.h"
#include "iconv.h"


namespace INONEGUI_FRAMEWORK{

typedef int (*CB_LEN_FIRST_CHAR)(const  unsigned char*, int len);

typedef int (*LTWDT_CB_RETRIEVE_CHAR)(const char*);
typedef struct _CHARSET_CONVERTER
{
	//get a char who has a special code
	LTWDT_CB_RETRIEVE_CHAR retrieve_char;
	void * conv_info;
	void * reserved;
}CHARSET_CONVERTER;

BOOL InitCharsetConv(CHARSET_CONVERTER* converter, const char* from_charset, const char* to_charset, LTWDT_CB_RETRIEVE_CHAR retrieve_char);
void FreeCharsetConv(CHARSET_CONVERTER* conv);
unsigned int CharsetConv(CHARSET_CONVERTER* conv, const char ** pwords);
char*  ConvertWord(CHARSET_CONVERTER* conv, const char* word, char* out, int len);
CB_LEN_FIRST_CHAR GetFirstCharCallback(const char* encoding);
INT  GetLastCharLen(CB_LEN_FIRST_CHAR pFirstCharFun,LPCTUSTR pChar, INT iLen);
}
#endif //__CHARSET_CONVERT_H__
