/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

/**
 *  @file Queue.h
 *  @brief queue header
 */
#ifndef __Queue_H__
#define __Queue_H__

#include "inogui.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>             /* For abort() */
#include <semaphore.h>

namespace INONEGUI_FRAMEWORK {

//disable internal queue sem
#define DISABLE_QSEM	1

#define Msg_t MsgType
typedef struct Queue_s  
{  
    int head;  
    int rear;
#ifndef DISABLE_QSEM
    LTGUI_SEM sem; 
#endif
    pthread_mutex_t q_mutex;	
    Msg_t *data;  
}Queue_t;

class Queue{

public:
       Queue();
	Queue(const char *pname, int size);
    	virtual ~Queue();
	int DeQueue(Msg_t* msg);
	int EnQueue(Msg_t* msg);
	Queue_t* get_Queue(){return &m_queue;};
	char *getQueueName(){return m_name;}
       void WakeupQueue();
private:
    	int QueueInit(Queue_t* Q);
	int QueueDeinit(Queue_t* Q);
	char m_name[MAX_NAME_LEN + 1];
	int m_qsize;
       Queue_t m_queue;
};

}

#endif //__Queue_H__