/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef _RES_MANAGE_H_
#define _RES_MANAGE_H_

#include "inogui.h"
#include "resadapter.h"

namespace INONEGUI_FRAMEWORK {

typedef struct _tagResInfo {
        SFileItemInfo sFileItemInfo;
        LPVOID   lpData;
        DWORD	dwSize;
        _tagResInfo(){
            lpData	= NULL;
            dwSize    = 0;
        }
}RESINFO;

typedef std::vector<CResAdapter*> VtResAdapter;
typedef std::vector<CResAdapter*>::iterator IterResAdapter;
#define RESLAYOUT_MAGICNAME                     "__LAYOUT__"    
class api_out CResManage
{
public:
        virtual ~CResManage();
        STATIC CResManage* GetInstance();

        LPVOID CreateRes(LPCTSTR pResName, STRING &strLayoutId);
        INT GetResInfo(LPVOID hResAdapter, LPCTSTR pResName, RESINFO *pResInfo);
        INT ReadBuffer(LPVOID hResAdapter, LPCTSTR pResName, RESINFO *pResInfo);
        VOID FreeBuffer(LPVOID pBuf);
        INT  DeleteRes(LPVOID pResAdapter);
        VOID DeInit();

private:
        LPVOID ReadFromPath(LPCTSTR pName, DWORD *pLen);
        CResManage();
	
private:
        STATIC CResManage* mpInstance;
        VtResAdapter mVectorRes;
};

#define RESM							(CResManage::GetInstance())
}//end namespace

#endif // _RES_MANAGE_H_
