/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __LOG_H__
#define __LOG_H__

#include "inogui.h"

#define DUSING_ELOG   1
#ifdef DUSING_ELOG
#if !defined(LOG_TAG)    
#define LOG_TAG                    __FILE__
#endif
#undef LOG_LVL
#if defined(XX_LOG_LVL)    
#define LOG_LVL                    XX_LOG_LVL
#endif
#include "utils/elog.h"

#define LogV   log_v
#endif //DUSING_ELOG

namespace INONEGUI_FRAMEWORK {

class api_out CLog
{
public:
        static void LogInit();
        static void EnableLog(int enable);
        static void LogConfig(const char *logPath, int logsize, int enableOutput);	
        static void LogEnd();
        static void LogU(const char *format, ...);
        static void LogI(const char *format, ...);
        static void LogW(const char *format, ...);
        static void LogK(const char *format, ...);
        static void LogE(const char *format, ...);
};

}//end namespace

#endif // __LOG_H__
