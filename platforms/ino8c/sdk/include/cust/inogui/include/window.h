/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __WINDOW_H__
#define __WINDOW_H__
#include "inogui.h"
#include "message.h"
#include "log.h"

namespace INONEGUI_FRAMEWORK {

/*class IWindowListener {  
public:  
    virtual ~IWindowListener() { }  
    virtual STRING GeWindowName() = 0; 
    virtual HWND GeWindowHandle() = 0; 
    virtual INT OnDerivedWndProc (UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL) = 0;
}; */

class api_out Window:public HandleWnd,
					      public Message
{
    public:
        Window();
        Window(Window* pParam);
        virtual ~Window();
        UINT32 GetWindowThreadID(){return mWndThreadID;};
        INT PostMessage(HWND hWnd, UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL);
        LRESULT SendMessage(HWND hWnd, UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL);
		
	//Window message task
	STATIC  INT OnWndProc(VOID *pParam);
	STRING  GetWndName(HWND hWnd);
	INT GetThreadIDFromHWnd(HWND hWnd);
	INT DispatchMessage (PMSG pMsg);
    private:
    
    private:
        BOOL mEnable;
        STATIC CPtrArray mPageList;
        LTGUI_THREAD *mpWndTh;
};
}
#endif //__WINDOW_H__
