/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __SYSPORT_H__
#define __SYSPORT_H__

#define SYS_SDL

#ifdef SYS_SDL
#include "sysport/syssdl.h"
#endif
#endif //__SYSPORT_H__