/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INVALIDRECTMANAGE_H__
#define __INVALIDRECTMANAGE_H__

#include "inogui.h"

namespace INONEGUI_FRAMEWORK {

class api_out InvalidRectManage
{
    public:
        virtual ~InvalidRectManage();
        STATIC InvalidRectManage* GetInstance();	
        VOID DoInit(INT iValidW,INT iValidH);
        VOID DeInit();
        BOOL IsPointInRect(RECT *pRect, POINT *pPoint);
        INT InvalidRect(RECT rtRect, BOOL bInsertBack = TRUE);
        INT GetPaintRect(RECT *pRect);
        BOOL DoesIntersect (const RECT* pSrc1, const RECT* pSrc2);
        VOID  CopyRect (RECT* pdrc, const RECT* psrc);
        BOOL IntersectRect(RECT* pDrc, const RECT* pSrc1, const RECT* pSrc2);   
        BOOL IsRc2CoveredRc1(const RECT* pRc1, const RECT* pRc2);  
	 INT SubtractRect(RECT* rc, const RECT* pSrc1, const RECT* pSrc2);	
	 VOID GetBoundRect (RECT* pDrc,  const RECT* pSrc1, const RECT* pSrc2);
	 BOOL ResetInvalidRect();
    private:
            InvalidRectManage();
            BOOL IsRectEmpty (const RECT* pRc);
            BOOL EqualRect (const RECT* pRc1, const RECT* pRc2);       
            BOOL IsValidRect(RECT *pRect);
            
    private:	
           STATIC InvalidRectManage* mpInstance;
           RECT mPaintRect;
           list<RECT*> mInvaidRectList;
           INT mValidWidth;
           INT mValidHight;
           LTGUI_MUTEX *mpInvalidRectListMutex;
};

#define IRM                 (InvalidRectManage::GetInstance())
}//end namespace

#endif // __INVALIDRECTMANAGE_H__