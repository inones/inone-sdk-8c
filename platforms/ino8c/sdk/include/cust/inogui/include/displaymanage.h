/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __DISPLAYMANAGE_H__
#define __DISPLAYMANAGE_H__
#include "inogui.h"
#include "window.h"
#include "wndanimators/wndanimmanager.h"

namespace INONEGUI_FRAMEWORK {

#define DM_CMD_MASK_OFF		0
enum
{
	DM_CMD_MASK	     		        =	0x0000FFFF,   		//CMD MASK OFF
	DM_CMD_BLOCKED     		 =	0x00010000,			//BLOCKED FLAG
};

typedef enum _EDispCmd{
	DM_CMD_NOTHING,
	DM_CMD_SELFREFRESH,
	DM_CMD_REFRESH,
	DM_CMD_EXITANIM,
	DM_CMD_ENTERANIM,
}EDispCmdType;

//mDmState
enum DispSt{
	DM_ST_NOTHING 	    = 0x00,
	DM_ST_ENABLE 		= 0x01,
	DM_ST_FFRAME		= 0x02,  //first frame refresh ok
	DM_ST_READY		    = 0x04,  //dm ready
	DM_ST_IGNORE		= 0x08,  //dm refresh area cmd ignore
};

typedef struct _SDispCmdData{
        EDispCmdType eDispCmd;
        INT                 iCmdParam;
        UINT32           iTimestamp;
}SDispCmdDataType;

STATIC INT DisplayThreadTask(void *para);
class api_out DisplayManage:public Window,
								  public IWindowListener
{
public:
        virtual ~DisplayManage();
        STATIC DisplayManage* GetInstance();
        LPVOID GetDisplayHandle();
        INT DoInit(INT iHRes,INT iVRes, UINT8 uColorDepth = 32);
        INT DeInit();
        LPVOID GetScreen();
        UINT8 GetColorDepth();
        VOID PostSemNowPaint();
        BOOL RequestDisplayCmd(EDispCmdType eReqCmd, INT iCmdParam, BOOL bBlocked = TRUE);
        friend INT DisplayThreadTask(void *para);
        VOID SetDisplayRefresh(BOOL bOnOff);
        BOOL IsSetDisplayRefresh();

        //IWindowListener
        STRING GetWindowName();
        HWND GetWindowHandle(); 
        LRESULT OnDerivedWndProc(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);  
        BOOL IsDMReady();
        
private:
        DisplayManage();
        VOID DisplayCmdStateMachine();
            
private:
        STATIC DisplayManage* mpInstance;
        LTGUI_THREAD *mDisplayThread;
        LTGUI_SEM *mDisplaySem;
        LTGUI_SURFACE*mScreen;
        LTGUI_MUTEX *mpMutex;
        RECT 		mFirstFrameRect;
        SDispCmdDataType mDispCurCmd;
        SDispCmdDataType mDispReqCmd;
        IWndAnimator *mpWndAnim;
        UINT16 mHRes;
        UINT16 mVRes;
        UINT8 mColorDepth;
        UINT8 mDispState;
};

#define DM              	(DisplayManage::GetInstance())
#define HWND_DISP	(DisplayManage::GetInstance())
}//INONEGUI_FRAMEWORK

#endif //__DISPLAYMANAGE_H__
