/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

/**
*base var typedef
*/
#ifndef __COMMON_H__
#define __COMMON_H__

/**
 * @var SBYTE
 * @brief A type definition for an 8-bit signed character.
 */
#ifndef SBYTE 
typedef signed char     SBYTE;
#endif

/**
 * @var SIZEOF_PTR
 * @brief The size of a pointer. 4 for 32-bit and 8 for 64-bit.
 */
/**
 * @var SIZEOF_HPTR
 * @brief The size of a half or pointer. 2 for 32-bit and 4 for 64-bit.
 */
#if defined(__CPU64__)
#   define SIZEOF_PTR   8
#   define SIZEOF_HPTR  4
#else
#   define SIZEOF_PTR   4
#   define SIZEOF_HPTR  2
#endif

#if SIZEOF_PTR == 8
#   define NR_BITS_BYTE      (8)
#   define NR_BITS_WORD16	 (16)
#   define NR_BITS_WORD      (32)
#   define NR_BITS_DWORD     (64)

#   define BITMASK_BYTE      (0xFF)
#   define BITMASK_WORD16    (0xFFFF)
#   define BITMASK_WORD      (0xFFFFFFFF)
#   define BITMASK_DWORD     (0xFFFFFFFFFFFFFFFF)

#   define INT_PTR_MIN       (-9223372036854775807L-1)
#   define INT_PTR_MAX       (9223372036854775807L)
#   define UINT_PTR_MAX      (18446744073709551615UL)
#else
#   define NR_BITS_BYTE      (8)
#   define NR_BITS_WORD16	 (16)
#   define NR_BITS_WORD      (16)
#   define NR_BITS_DWORD     (32)

#   define BITMASK_BYTE      (0xFF)
#	define BITMASK_WORD16 	 (0xFFFF)
#   define BITMASK_WORD      (0xFFFF)
#   define BITMASK_DWORD     (0xFFFFFFFF)

#   define INT_PTR_MIN       (-2147483647-1)
#   define INT_PTR_MAX       (2147483647)
#   define UINT_PTR_MAX      (4294967295U)
#endif

#ifndef  UINT32_MAX
#define UINT32_MAX           (0xFFFFFFFFu)
#endif

/**
 * @var WORD_HPTR
 * @brief An unsigned int (word) type in half pointer precision.
 */
#if defined(__CPU64__)
typedef unsigned int WORD_HPTR;
#else
typedef unsigned short WORD_HPTR;
#endif

/**
 * @var SWORD_HPTR
 * @brief An signed int type in half pointer precision.
 */
#if defined(__CPU64__)
typedef signed int SWORD_HPTR;
#else
typedef signed short SWORD_HPTR;
#endif

/**
 * @var WORD
 * @brief A type definition for an unsigned integer (word).
 */
typedef WORD_HPTR WORD;

/**
 * @var SWORD
 * @brief A type definition for a signed integer.
 */
typedef SWORD_HPTR SWORD;

/**
 * @var WORD16
 * @brief A type definition for a 16-bit unsigned integer (word).
 */
typedef unsigned short WORD16;

/**
 * @var SWORD16
 * @brief A type definition for a 16-bit signed integer.
 */
typedef signed short SWORD16;

/**
 * @var LONG_PTR
 * @brief A signed long type for pointer precision.
 */
#if defined(__CPU64__)
typedef long LONG_PTR;
#else
typedef long LONG_PTR;
#endif

/**
 * @var LINT
 * @brief Signed integer which has pointer precision.
 */
typedef LONG_PTR LINT;

/**
 * @var LRESULT
 * @brief Signed result of message processing.
 */
typedef LONG_PTR LRESULT;

/**
 * @var LHANDLE
 * @brief Signed resource handle.
 */
typedef LONG_PTR LHANDLE;

/**
 * @var DWORD_PTR
 * @brief An unsigned long type for pointer precision.
 *
 * Commonly used for general 32-bit parameters that have been extended
 * to 64 bits in 64-bit platform.
 */
#if defined(__CPU64__)
typedef unsigned long DWORD_PTR;
#else
typedef unsigned long DWORD_PTR;
#endif

/**
 * @var DWORD
 * @brief A unsigned long type definition for pointer precision.
 */
typedef DWORD_PTR DWORD;

/**
 * @var DWORD32
 * @brief A type definition for a 32-bit unsigned integer.
 */
typedef unsigned int DWORD32;

/**
 * @var SDWORD_PTR
 * @brief A signed long type for pointer precision.
 *
 * Commonly used for general 32-bit parameters that have been extended
 * to 64 bits in 64-bit platform.
 */
#if defined(__CPU64__)
typedef signed long SDWORD_PTR;
#else
typedef signed long SDWORD_PTR;
#endif

/**
 * @var SDWORD
 * @brief A signed long type definition for pointer precision.
 */
typedef SDWORD_PTR SDWORD;

/**
 * @var SDWORD32
 * @brief A type definition for a 32-bit signed integer.
 */
typedef signed int SDWORD32;

#if SIZEOF_PTR == 8
typedef unsigned short  QDWORD;
#define QDWORD_SHIFT    16
#else
typedef unsigned char   QDWORD;
#define QDWORD_SHIFT    8
#endif

/**
 * @var INT_PTR
 * @brief A signed integer type for pointer precision.
 */
#if defined(__CPU64__)
typedef long INT_PTR;
#else
typedef int INT_PTR;
#endif

/**
 * @var UINT_PTR
 * @brief A unsigned integer type for pointer precision.
 */
#if defined(__CPU64__)
typedef unsigned long UINT_PTR;
#else
typedef unsigned long UINT_PTR;
#endif

#ifndef WCHAR
#define WCHAR		char
#endif

//represent data dir
#define OUT
#define IN
#define INOUT

#ifndef MAX_PATH
#define MAX_PATH	260
#endif

#ifndef  STATIC
#define  STATIC static
#endif

#ifndef  CONST
#define  CONST const
#endif

#ifndef VOID
typedef void  VOID;
#endif

#ifndef LPVOID
typedef void * LPVOID;
#endif

#ifndef HWND
typedef void * HWND;
#endif

#ifndef HCTRL
typedef void * HCTRL;
#endif

#ifndef WPARAM
typedef unsigned long WPARAM;
#endif

#ifndef LPARAM
typedef unsigned long LPARAM;
#endif

#ifndef NULL
#define NULL	0
#endif

#ifndef DWORD
typedef unsigned long DWORD;
#endif

#ifndef SINT
typedef int SINT;
#endif

#ifndef INT
typedef int INT;
#endif

#ifndef SINT8
typedef char SINT8;
#endif

#ifndef CHAR
typedef char CHAR;
#endif

#ifndef CTCHAR
typedef const char CTCHAR;
#endif

#ifndef SINT16
typedef short SINT16;
#endif

#ifndef SINT32
typedef int SINT32;
#endif

#ifndef LONG
typedef long LONG;
#endif

#ifndef ULONG
typedef unsigned long ULONG;
#endif

#ifndef SINT64
typedef long long SINT64;
#endif

#ifndef UINT
typedef unsigned int UINT;
#endif

#ifndef UINT8
typedef unsigned char UINT8;
#endif

#ifndef UINT16
typedef unsigned short UINT16;
#endif

#ifndef UINT32
typedef unsigned int UINT32;
#endif

#ifndef UINT64
typedef unsigned long long int UINT64;
#endif

#ifndef BYTE
typedef unsigned char BYTE;
#endif

#ifndef LPBYTE
typedef BYTE * LPBYTE;
#endif

#ifndef BOOL
typedef BYTE BOOL;
#endif

#ifndef LPSTR
typedef char * LPSTR;
#endif

#ifndef LPCTSTR
typedef const char * LPCTSTR;
#endif

#ifndef LPCTUSTR
typedef const unsigned char * LPCTUSTR;
#endif

#ifndef STRING
typedef string STRING;
#endif

#ifndef FLOAT
typedef float FLOAT;
#endif

#ifndef DOUBLE
typedef double DOUBLE;
#endif

#ifndef TRUE
#define TRUE	1
#endif

#ifndef true
#define true	1
#endif

#ifndef FALSE
#define FALSE	0
#endif

#ifndef false
#define false	0
#endif

typedef DWORD32 RGBCOLOR;

class api_out CPtrArray
{
public:
	CPtrArray();
	virtual ~CPtrArray();

	INT IsEmpty();
	INT Find(LPVOID pData);
	INT Add(LPVOID pData);
	INT AddtoTop(LPVOID pData);
	INT GetSize();
	VOID SetMaxSize(INT size);
	INT RemoveByValue(LPVOID p);
	INT RemoveByIndex(INT index);
	INT RemoveAll();
	INT Swap(LPVOID p1, LPVOID p2);
	INT Swap(INT index1, INT index2);
	LPVOID GetAt(INT iIndex);
	LPVOID operator[] (INT nIndex);

private:

protected:
	LPVOID* m_ppVoid;
	INT m_nCount;
	INT maxCount;
	
	LTGUI_MUTEX *mpMutex;
};

typedef struct _mySYSTEMTIME
{
	WORD wYear;
	WORD wMonth;
	WORD wDay;
	WORD wHour;
	WORD wMinute;
	WORD wSecond;
	WORD wMilliseconds;
}SYSTEMTIME;

void api_out GetLocalTime(SYSTEMTIME *pTime);


#define LTGUICFG_NAME			"inogui.cfg"

#define LTGUICFG_OBJ_S_SYS		"sys"
#define LTGUICFG_KEY_I_SSIZE		"ssize"     //screen size (low 16 bits)W / (High 16 bits)H
#define LTGUICFG_KEY_I_TPOFF		"tpoff"	//touch offset (low 16 bits)X / (High 16 bits)Y
#define LTGUICFG_KEY_I_TPSCA		"tpsca"	//touch x/y scale (low 16 bits)X / (High 16 bits)Y
#define LTGUICFG_KEY_S_FONT		"font"
#define LTGUICFG_KEY_S_USRCFG	"usrcfg"
#define LTGUICFG_KEY_S_UART		"uart"	//commuication uart paramter
#define LTGUICFG_KEY_S_LOGFILE	"logfile"	//logfile paramter
#define LTGUICFG_KEY_S_HOME		"home"	//home setting
#define LTGUICFG_KEY_S_TITBAR		"titbar"	//title setting
#define LTGUICFG_KEY_S_NAVBAR	"navbar"	//nav setting
#define LTGUICFG_KEY_S_IME		"ime"	//home setting
#define LTGUICFG_KEY_S_WPP		"wpp"	//wpp setting
#define LTGUICFG_KEY_I_GCFG		"gcfg"	//gesture cfg
#define LTGUICFG_KEY_I_FCLR		"fclr"	//focus color
#define LTGUICFG_KEY_S_VER		"ver"	//ltgui version 
#define LTGUICFG_KEY_I_ATT			"att"		//ltgui attrib 
#define LTGUICFG_KEY_S_CTTRT	        "cttrt"     //ltgui content rect
#define LTGUICFG_KEY_I_ANIM	        "anim"     //ltgui animator cfg
#define LTGUICFG_OBJ_S_ASERV		"aserv"	//autorun service

#define LTGUICFG_ATT_DEFLANG_OFF	0
enum ELTGUICfgAtt{
	LTGUICFG_ATT_DEFLANG			=	0x00FF,
	LTGUICFG_ATT_TITLEBAR		=	0x0100,
	LTGUICFG_ATT_NAVBAR			=	0x0200,
	LTGUICFG_ATT_IME				=	0x0400,
	LTGUICFG_ATT_SHOWFOCUS		=	0x0800,
	LTGUICFG_ATT_DISSTRICT		=	0x1000,
	LTGUICFG_ATT_SHOWFPS		=	0x2000,
	LTGUICFG_ATT_COLOR16BIT		=	0x4000,   //color depth
};

//LTGUICFG_KEY_I_GCFG "gcfg"
#define LTGUICFG_GES_MODE_OFF		28
#define LTGUICFG_GES_MAXTIME_OFF		16
#define LTGUICFG_GES_MINDIST_OFF		0
enum ELTGUICfgGesture{
	LTGUICFG_GES_MODE			=	0xF0000000,
	LTGUICFG_GES_MAXTIME		=	0x0FFF0000,
	LTGUICFG_GES_MINDIST			=	0x0000FFFF
};

typedef enum _EGestureMode{
	GESTURE_MODE_SIMPLE		=	0x0,
	GESTURE_MODE_CACHE		=	0x1,
}EGestureModeType;

#define MAX_AUTORUN_SERV		8
#define MAX_GUINAME_LEN			32
typedef struct _tagSystemCfg
{
	//touch and screen setting
	INT iScreenW;
	INT iScreenH;
	INT iTouchOffsetX;
	INT iTouchOffsetY;
	FLOAT fTouchScaleX;
	FLOAT fTouchScaleY;
    //Content Rect
    CHAR cContentRt[MAX_GUINAME_LEN+1];
    //Wnd animation
    UINT8 iWndAnim;
	//System version
	CHAR cVer[MAX_GUINAME_LEN+1];
	//font settting
	CHAR cFontName[MAX_GUINAME_LEN+1];	
	BOOL bLogEnable;
	//User cfg file
	CHAR  cUserCfgPath[MAX_GUINAME_LEN+1];
	//Commuication Uart name
    CHAR cUart[MAX_GUINAME_LEN+1];
    //logfile path name
    CHAR cLogfile[MAX_PATH+1];
    //Home app name
    CHAR cHome[MAX_GUINAME_LEN+1];	
    //titlebar app name
    CHAR cTitBar[MAX_GUINAME_LEN+1];
    //navbar app name
	CHAR cNavBar[MAX_GUINAME_LEN+1];
	//ime app name
	CHAR cIme[MAX_GUINAME_LEN+1];
	//gesture cfg
	UINT16	iGestureMinDist;
	UINT16	iGestureMaxTime;
	EGestureModeType	eGestureMode;	
	//Focus color
    UINT32 iFocusClr;
    //attrib
    UINT32 iAtt;
    //cache app
	UINT8 iAutorunServCnt;
	CHAR strAutorunServ[MAX_AUTORUN_SERV][MAX_GUINAME_LEN+1];
}SystemCfg;

typedef enum{
    LAYER_PRIOR_MIN                   		= 0x0000,
    LAYER_PRIOR_WALLPAPER     		= LAYER_PRIOR_MIN,
    LAYER_PRIOR_ACTIVITY         		= 0x0001,
    LAYER_PRIOR_USERUIMIN     		= 0x0002,
    LAYER_PRIOR_USERUIMAX     		= 0x0004,
    LAYER_PRIOR_USERUIBOX     		= LAYER_PRIOR_USERUIMAX,
    LAYER_PRIOR_TITLEBAR         		= LAYER_PRIOR_USERUIMAX+1,
    LAYER_PRIOR_NAVBAR            		= LAYER_PRIOR_USERUIMAX+2,
    LAYER_PRIOR_SYSBOX             		= LAYER_PRIOR_USERUIMAX+3,
    LAYER_PRIOR_LOCKSCREEN           	= LAYER_PRIOR_USERUIMAX+4,
    LAYER_PRIOR_INPUTMETHOD           = LAYER_PRIOR_USERUIMAX+5,
    LAYER_PRIOR_SCREENSAVER           	= LAYER_PRIOR_USERUIMAX+6,
    LAYER_PRIOR_CURSOR           		= LAYER_PRIOR_USERUIMAX+7,
    LAYER_PRIOR_MAX
}ELayerPriorType;

typedef enum{
    WINDOW_TYPE_ACTIVITY,
    WINDOW_TYPE_USERUIBOX,
    WINDOW_TYPE_TITLEBAR,
    WINDOW_TYPE_NAVBAR,
    WINDOW_TYPE_SYSBOX,
    WINDOW_TYPE_LOCKSCREEN,
    WINDOW_TYPE_INPUTMETHOD,
    WINDOW_TYPE_SCREENSAVER,
    WINDOW_TYPE_MAX,
}EWindowType;

typedef enum{
    DC_ROT_0   = 0x0,
    DC_ROT_90  = 0x1,
    DC_ROT_180 = 0x2,
    DC_ROT_270 = 0x3,
    DC_ROT_MAX,
}EDcRotateType;

typedef enum{
    LAYER_FLAG_NOTHING                     = 0x0000,
    LAYER_FLAG_ST_INVISIBLE             = 0x0001,
    LAYER_FLAG_ST_DISABLE                = 0x0002,
    LAYER_FLAG_ST_MASK                     = 0xFFFF,
    LAYER_FLAG_ATTRIB_MODAL          = 0x00010000,
    LAYER_FLAG_ATTRIB_TRANSP         = 0x00020000,  //transparent
    LAYER_FLAG_ATTRIB_MASK             =0xFFFF0000,
}ELayerFlagType;

typedef enum{
    ACTIVITY_LAYOUT_INVALID 		= -128,
    ACTIVITY_LAYOUT_RESPACK 		= -2,
    ACTIVITY_LAYOUT_MAIN 			= -1,
    ACTIVITY_LAYOUT_SUBMAIN1		= 0,
    ACTIVITY_LAYOUT_SUBMAIN2,
    ACTIVITY_LAYOUT_SUBMAIN3,
    ACTIVITY_LAYOUT_SUBMAIN4,
    ACTIVITY_LAYOUT_SUBMAIN5,
    ACTIVITY_LAYOUT_SUBMAIN6,
    ACTIVITY_LAYOUT_SUBMAIN7,
    ACTIVITY_LAYOUT_SUBMAIN8,
}EActivityLayoutID;

typedef enum{
	DIR_NOTHING,       //find short distance with me
	DIR_UP,
	DIR_DOWN,
	DIR_LEFT,
	DIR_RIGHT,
	MAX_DIR_STATE,
}EDirType;

typedef struct _tagPoint {
	INT x;
	INT y;
}POINT;

typedef struct _tagSIZE {
	INT w;
	INT h;
}SIZE;

typedef struct _tagRect {
	INT x;
	INT y;
	INT w;
	INT h;
}RECT;

typedef struct _tagRectPad {
	INT l;
	INT t;
	INT r;
	INT b;
}RECTPAD;

typedef struct _Msg {
	HWND    hWnd;
	UINT      uiMsgID;
	WPARAM  wParam;
	LPARAM  lParam;
	DWORD  dwTime;
	VOID *pOther;
} MsgType;

typedef struct _SyncMessage {
	MsgType   stMsg;
	LTGUI_SEM *pSyncSem;
	LRESULT iRetValue;
	struct _SyncMessage* pNext;
} SyncMessage;

#define MAX_MSG_QUEUE_NUM                  (128)
#define DEF_MSG_QUEUE_NUM                   (32)
#define MAX_NAME_LEN		                    8

typedef struct _HDC{
	LPVOID pDcSurface;
	LPVOID pDcOwner;
} HDC;
#define HDCSURFACE(X)			(((PHDC)X)->pDcSurface)
#define HDCOWNER(X)			(((PHDC)X)->pDcOwner)

typedef MsgType MSG;
typedef MsgType* PMSG;
typedef SyncMessage* PSYNCMSG;
typedef HDC* PHDC;

//Msg Type
#define MSG_T_SYNC        	    0x10000000
#define MSG_T_POST             0x20000000

//For publicfunc
typedef INT (*CBSetKeyValue)(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj,  STRING strKeyName);
typedef LPVOID (*CBSetObject)(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj);

enum _GuiPathType{
	GUI_PATH_ROOT,
	GUI_PATH_LIB,
	GUI_PATH_APP,
	GUI_PATH_SERV,
	GUI_PATH_ETC,
	GUI_PATH_RES,
	GUI_PATH_FONT,
	GUI_PATH_LANG,
	GUI_PATH_UI,
	MAX_GUI_PATH
};

typedef LPVOID (*pGetInstance)(STRING strName);

typedef enum {
    LTGUI_THREAD_PRIORITY_LOW,
    LTGUI_THREAD_PRIORITY_NORMAL,
    LTGUI_THREAD_PRIORITY_HIGH,
    LTGUI_THREAD_PRIORITY_TIME_CRITICAL
} LTGUI_ThreadPrior;

//Time define
#define TIME_SCALE                (60)
#define TIME_HOU2SEC          (3600)
#define TIME_SEC2MS             (1000)

//~30fps
#define DISPLAY_REFRESH_MINTIMEMS    10
#define DISPLAY_REFRESH_TIMEMS          30

#define INVALID_VALUE		(-1)
/**
 * \def ERR_OK
 * \brief Return value error ok.
 */
#define ERR_OK                          0

/* Create page error code */
#define ERR_OK_EXISTPAGE           0
#define ERR_OK_NEWPAGE             1

/* Add invalid area */
#define ERR_OK_AREAMERGE          1
#define ERR_OK_AREANEW              2

/* Event doing value */
#define ERR_OK_EVENTDONE          1

/*Anything done */
#define ERR_OK_DONE                     1

/**
 * \def ERR_INVALID
 * \brief Return value error invalid.
 */
#define ERR_INVALID                           (-1)
#define ERR_INVALID_TIMEOUT         (-2)

/**
 * \def ERR_INVALID Render
 * \brief Return value error invalid.
 */
#define ERR_INVALID_FONT                (-1)
#define ERR_INVALID_DC                    (-2)

/* Message id define */
/* Definitions of common messages. */
#define MSG_NULLMSG         0x0000
#define MSG_SYNCMSG         0x0000

/* Message WPARM is buffer pointer */
#define MSG_FLAG_WPARMISBUF     0x0001
/* User manual to release wparam buffer */
#define MSG_FLAG_USERWPARM      0x0002
/* Sync Message Flag */
#define MSG_FLAG_SYNC                    0x0004

#define CHECK_MSGFLAG_WPARMISBUF(x)         (x&MSG_FLAG_WPARMISBUF)
#define CHECK_MSGFLAG_USERWPARM(x)           (x&MSG_FLAG_USERWPARM)
#define CHECK_MSGFLAG_SYNC(x)                        (x&MSG_FLAG_SYNC)

/**
 * \defgroup touch_msgs touch event messages
 * @{
 */

/* Group 1 from 0x0001 to 0x000F, the touch messages. */
#define MSG_FIRSTTOUCHMSG   0x0001
/**
 * \def MSG_TOUCH_DOWN
 * \brief Indicates a touch down event.
 *
 * \code
 * MSG_TOUCH_DOWN
 * \endcode
 *
 * \param x,y The position of touch.
 */
#define MSG_TOUCH_DOWN              (MSG_FIRSTTOUCHMSG + 0x0)
#define MSG_TOUCH_UP                    (MSG_FIRSTTOUCHMSG + 0x1)
#define MSG_TOUCH_MOVE               (MSG_FIRSTTOUCHMSG + 0x2)
#define MSG_TOUCH_MMOVE            (MSG_FIRSTTOUCHMSG + 0x3)
#define MSG_KEY_UP                         (MSG_FIRSTTOUCHMSG + 0x4)
#define MSG_KEY_DOWN                   (MSG_FIRSTTOUCHMSG + 0x5)
#define MSG_KEY_LONGPRESS          (MSG_FIRSTTOUCHMSG + 0x6)
#define MSG_CHAR            			  (MSG_FIRSTTOUCHMSG + 0x7)
#define MSG_GESTURE_UP		  (MSG_FIRSTTOUCHMSG + 0x8)
#define MSG_GESTURE_DOWN		  (MSG_FIRSTTOUCHMSG + 0x9)
#define MSG_GESTURE_LEFT		  (MSG_FIRSTTOUCHMSG + 0xA)
#define MSG_GESTURE_RIGHT		  (MSG_FIRSTTOUCHMSG + 0xB)

#define MSG_LASTTOUCHMSG           0x001F

//touch or mouse point info
 #define MSG_TOUCH_MIN		MSG_TOUCH_DOWN
 #define MSG_TOUCH_MAX		MSG_TOUCH_MMOVE
 #define MSG_GESTURE_MIN		MSG_GESTURE_UP
 #define MSG_GESTURE_MAX		MSG_GESTURE_RIGHT 
 #define MSG_KEY_MIN			MSG_KEY_UP
 #define MSG_KEY_MAX			MSG_CHAR
 #define MSG_INPUT_MIN			MSG_FIRSTTOUCHMSG
 #define MSG_INPUT_MAX			MSG_LASTTOUCHMSG
STATIC inline BOOL IsWndTouchMsg (UINT uMsgId)
{
    if((uMsgId >= MSG_TOUCH_MIN && uMsgId <= MSG_TOUCH_MAX) ||\
	 (uMsgId >= MSG_GESTURE_MIN && uMsgId <= MSG_GESTURE_MAX)){
        return TRUE;
    }else{
        return FALSE;
    }
}

STATIC inline BOOL IsWndKeyMsg (UINT uMsgId)
{
    if(uMsgId >= MSG_KEY_MIN && uMsgId <= MSG_KEY_MAX){
        return TRUE;
    }else{
        return FALSE;
    }
}

STATIC inline BOOL IsWndInputMsg (UINT uMsgId)
{
    if(uMsgId >= MSG_INPUT_MIN && uMsgId <= MSG_INPUT_MAX ){
        return TRUE;
    }else{
        return FALSE;
    }
}

/**
 * \defgroup window_msgs event messages WND request PM
 * @{
 */

/* Group 2 from 0x0010 to 0x001F, the window messages. */
#define MSG_FIRSTWNDMSG               0x0020

#define MSG_WNDRAM_UPDATE          (MSG_FIRSTWNDMSG + 0x0)
#define MSG_WNDRAM_SWITCH          (MSG_FIRSTWNDMSG + 0x1)
/* MSG_WNDRAM_AREA wParam:x(16BIT)y(16BIT) lParam:w(16BIT)h(16BIT)*/
#define MSG_WNDRAM_AREA               (MSG_FIRSTWNDMSG + 0x2)
#define MSG_WNDRAM_BACK               (MSG_FIRSTWNDMSG + 0x3)
#define MSG_WNDRAM_HOME              (MSG_FIRSTWNDMSG + 0x4)
#define MSG_WNDRAM_IMEREG	      (MSG_FIRSTWNDMSG + 0x5)
#define MSG_WNDRAM_IMEUNREG      (MSG_FIRSTWNDMSG + 0x6)
#define MSG_WNDRAM_IMEOPEN	      (MSG_FIRSTWNDMSG + 0x7)
#define MSG_WNDRAM_IMECLOSE	      (MSG_FIRSTWNDMSG + 0x8)
#define MSG_WNDRAM_FOCUSCHG      (MSG_FIRSTWNDMSG + 0x9)
#define MSG_WNDRAM_SAVECFG          (MSG_FIRSTWNDMSG + 0xa)

#define MSG_LASTWNDMSG                  0x002F

/**
 * \defgroup pagemanage_msgs event messages pm to wnd(Notify)
 * @{
 */

/* Group 2 from 0x0020 to 0x002F, the activitymanage messages. */
#define MSG_FIRSTAMMSG                    0x0030
#define MSG_AMTWND_CREATE            (MSG_FIRSTAMMSG + 0x0)
#define MSG_AMTWND_DESTORY         (MSG_FIRSTAMMSG + 0x1)
#define MSG_AMTWND_STOP                (MSG_FIRSTAMMSG + 0x2)
#define MSG_AMTWND_START              (MSG_FIRSTAMMSG + 0x3)
#define MSG_AMTWND_BACK                (MSG_FIRSTAMMSG + 0x4)
#define MSG_AMTWND_PAINT              (MSG_FIRSTAMMSG + 0x5)
#define MSG_AMTWND_CHGLANG         (MSG_FIRSTAMMSG + 0x6)   //language change
#define MSG_AMTWND_CLRSTACK       (MSG_FIRSTAMMSG + 0x7) 
#define MSG_AMTWND_GETLAYOUTID (MSG_FIRSTAMMSG + 0x8)   //get top page id
/* MSG_QUIT */
#define MSG_AMTWND_QUIT              (MSG_FIRSTAMMSG + 0x1F)

#define MSG_LASTAMMSG                       0x003F

STATIC inline BOOL IsAmToWndMsg (UINT uMsgId)
{
    if(uMsgId >= MSG_FIRSTAMMSG && uMsgId <= MSG_LASTAMMSG){
        return TRUE;
    }else{
        return FALSE;
    }
}

STATIC inline BOOL IsWndToAmMsg (UINT uMsgId)
{
    if(uMsgId >= MSG_FIRSTWNDMSG && uMsgId <= MSG_LASTWNDMSG ){
        return TRUE;
    }else{
        return FALSE;
    }
}

/* Group 3 from 0x0040 to 0x004F, the activitymanage messages. */
#define MSG_FIRSTDMMSG                    0x0040
#define MSG_WNDRDM_DMCMD            (MSG_FIRSTDMMSG + 0x0)
#define MSG_WNDRDM_FFAREA            (MSG_FIRSTDMMSG + 0x1)
#define MSG_WNDRDM_AREA                (MSG_FIRSTDMMSG + 0x2)
#define MSG_LASTDMMSG                  	0x004F

STATIC inline BOOL IsWndToDmMsg (UINT uMsgId)
{
    if(uMsgId >= MSG_FIRSTDMMSG && uMsgId <= MSG_LASTDMMSG ){
        return TRUE;
    }else{
        return FALSE;
    }
}

/* Group 2 from 0x0100 to 0x1FF, the Control messages. */
#define MSG_FIRSTCTRLMSG                    0x0100

enum{
	/* Control */
	MSG_CTRL_START		= MSG_FIRSTCTRLMSG,
	MSG_CTRL_STOP,    
	MSG_CTRL_GETTEXT,
	MSG_CTRL_SETTEXT, 
	MSG_CTRL_ENABLE,
	MSG_CTRL_REFRESH,
	MSG_CTRL_REINIT,
	MSG_CTRL_CHGLANG,
	
	/* LBL */
	
	/* IME */
	MSG_IME_REGISTER,
	MSG_IME_UNREGISTER, 
	MSG_IME_OPEN, 
	MSG_IME_CLOSE,
	MSG_IME_SETSTATUS,
	MSG_IME_GETSTATUS,
	MSG_IME_SETTARGET,
	MSG_IME_GETTARGET,
	MSG_IME_SETPOS,
	MSG_IME_GETPOS,
	
	MSG_IMM_IMEOPEN,
	MSG_IMM_IMECHANGED,
	MSG_IMM_IMECLOSE,

	/* SeekBar*/
	MSG_SB_SETVALUE,
	MSG_SB_GETVALUE,
};

#define MSG_LASTCTRLMSG                       0x01FF

STATIC inline BOOL IsCtrlMsg (UINT uMsgId)
{
    if(uMsgId >= MSG_FIRSTCTRLMSG && uMsgId <= MSG_LASTCTRLMSG ){
        return TRUE;
    }else{
        return FALSE;
    }
}

/* Group 3 from 0x0200 to 0x2FF, the System Misc messages. */
#define MSG_FIRSTSYSMSG                    0x0200
#define MSG_SYS_TIMER                                     (MSG_FIRSTSYSMSG + 0x0)

#define  MSG_LASTSYSMSG                     0x2FF
STATIC inline BOOL IsSysMsg (UINT uMsgId)
{
    if(uMsgId >= MSG_FIRSTSYSMSG && uMsgId <= MSG_LASTSYSMSG ){
        return TRUE;
    }else{
        return FALSE;
    }
}
/* GUI Customize Message start*/
#define MSG_GUI						(0x70000000)
#define INO_GUIMSG(X)				(MSG_GUI+X)

/* User Customize Message start*/
#define MSG_USER					(0x80000000)
#define INO_USERMSG(X)				(MSG_USER+X)
STATIC inline BOOL IsUserMsg (UINT uMsgId)
{
    if(uMsgId >= MSG_USER){
        return TRUE;
    }else{
        return FALSE;
    }
}
#endif  //__COMMON_H__
