/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __IME_H__
#define __IME_H__
#include "inogui.h"
#include "log.h"
#include "charset_convert.h"

namespace INONEGUI_FRAMEWORK {

#define LTWDT_MAX_INPUT            128
#define LTWDT_IME_UPDATE_KEY       0x01
#define LTWDT_IME_UPDATE_WORDS     0x02
#define LTWDT_IME_UPDATE_FEEDBACK  0x04
#define LTWDT_IME_UPDATE_CURSOR    0x08
#define LTWDT_IME_UPDATE_ALL       0xFFFF

class ImeManage;
class ImeEngine;
class ImeIterator;

class ImeEngine{
   public:
	virtual LPCTSTR GetEncoding();
	virtual LTWDT_CB_RETRIEVE_CHAR GetRetrieveChar();
	virtual LPCTSTR ImeName()  = 0;
	virtual ImeIterator* NewIterator() = 0;
	virtual ImeIterator* NewAssocIterator() = 0;
	virtual BOOL  FindWords(ImeIterator *pIt, LPCTSTR input, INT start) = 0; 
	virtual BOOL  FindAssociate(ImeIterator *pIt, LPCTSTR words) = 0;
};

class ImeIterator{
   public:
        virtual VOID Empty() = 0;
	virtual int Count() = 0;
	virtual int Next() = 0;
	virtual int Prev() = 0;
	virtual LPCTSTR Word() = 0;
	virtual int Locate(int off, int type) = 0;
	virtual INT CurIndex() = 0;
	virtual DWORD SetProperty(int id, DWORD valude);
	virtual DWORD GetProperty(int id);
public:
	CHARSET_CONVERTER *charset_converter;;
};

class api_out Ime{
    public:
       virtual ~Ime();
	Ime();
	virtual LPCTSTR GetSelWord(BOOL braw);
	virtual DWORD GetImeStatus(int code);
	virtual BOOL  SetImeStatus(INT, DWORD);
	virtual BOOL  SetEncoding(LPCTSTR);

	virtual BOOL AddChar(INT ch);
	virtual BOOL DelChar(BOOL bback);
	virtual BOOL ClearInput();
       virtual BOOL SetInsert(INT pos, INT type);
	virtual VOID Empty(BOOL bUpdate);
	virtual BOOL EmptyKey();	
	virtual INT  OnChar(INT ch,DWORD keyflags);
	virtual INT  OnKeyDown(INT scancode, DWORD keyflags);
	virtual BOOL ShowIme();
	virtual BOOL HideIme();
	virtual BOOL Update(DWORD flfags);
       virtual VOID SetImeEngine(ImeEngine *pImeEng);
	virtual LRESULT Ime_proc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL FindWords(const char* input, int start, int len);
	virtual BOOL FindAssociate(const char* words);
	virtual VOID Attach(HWND hwnd);	
    protected:

    private:
  	 BOOL _isSelWordActive();
	 VOID _setSelWordActive( BOOL bactive);
	  
    public:
	CHAR  input[LTWDT_MAX_INPUT]; 
	INT   len; 
	INT   start:16;
	INT   insert:16;
	ImeManage *mpImeManage;
	ImeEngine   *mpImeEngine;
	ImeIterator  *mpImeKeyIt;
	ImeIterator  *mpImeAssocIt;
	CHARSET_CONVERTER charset_conv; 
	HWND         hwnd_view;
};
}
#endif //__IME_H__