/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __LAYER_H__
#define __LAYER_H__

#include "inogui.h"

namespace INONEGUI_FRAMEWORK {

class ILayerHook{  
public:  
    virtual ~ILayerHook() { }  
    virtual INT OnDraw(PHDC pHDc, RECT rtPaint) = 0; 
    virtual INT PreOnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL) = 0; 
    virtual RECT* GetLayerRect() = 0;
    virtual STRING GetName() = 0;  
    virtual INT GetLayerPrior() = 0;  
    virtual BOOL SetLayerStatus(ELayerFlagType eFlag, BOOL bSet) = 0;
    virtual BOOL CheckLayerStatus(ELayerFlagType eFlag) = 0;
}; 

}//end namespace

#endif // __LAYER_H__