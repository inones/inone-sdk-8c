/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/


#ifndef __INOANIGIF_H__
#define __INOANIGIF_H__

#include "inogui.h"

namespace INONEGUI_FRAMEWORK {

typedef struct
{
	LTGUI_SURFACE*	surface;				/*  surface for this frame */
	int				x, y;					/* Frame offset position */
	int				disposal;				/* Disposal code */
	int				delay;					/* Frame delay in ms */
	int				user;					/* User data (not used by aniGIF) */
} AG_Frame;

#define AG_DISPOSE_NA					0	/* No disposal specified */
#define AG_DISPOSE_NONE					1	/* Do not dispose */
#define AG_DISPOSE_RESTORE_BACKGROUND	2	/* Restore to background */
#define AG_DISPOSE_RESTORE_PREVIOUS		3	/* Restore to previous */

int AG_isGIF( RWHANDLE src );
int  AG_LoadGIF( const char* file, AG_Frame* frames, int maxFrames );
int AG_LoadGIFFromMem( void *mem, int memsize, AG_Frame* frames, int size );
void AG_FreeSurfaces( AG_Frame* frames, int nFrames );
int AG_ConvertSurfacesToDisplayFormat( AG_Frame* frames, int nFrames );
int AG_NormalizeSurfacesToDisplayFormat( AG_Frame* frames, int nFrames );
int AG_LoadGIF_RW( RWHANDLE src, AG_Frame* frames, int size );

}//end namespace

#endif /* __INOANIGIF_H__ */