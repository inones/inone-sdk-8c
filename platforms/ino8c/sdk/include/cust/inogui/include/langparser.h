/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __LANGPARSER_H__
#define __LANGPARSER_H__

#include "inogui.h"

namespace INONEGUI_FRAMEWORK {

#define MAX_LANG_FILES		30
#define MAX_LANG_FILE_LEN	20
#define MAX_ITEM_COUNT	999
#define MAX_ITEM_LEN		120

typedef enum _ELangType
{
	LANG_EN_US	= 0,
	LANG_ZH_CN	= 1,
	LANG_ZH_TW	= 2,
	LANG_ZH_HK	= 2,
}ELangType;

class LangParser
{
public:
	virtual ~LangParser();
	STATIC LangParser* GetInstance();

	INT DoInit(INT iLangId = INVALID_VALUE);
	INT DeInit();

	INT GetLangCount();
	INT SetLang(INT index);
	INT GetLangIndex();
	INT SetDefLang();
	INT SaveSet();

	char *GetStr(const char *szKey);
	char *GetStr(INT id);   
private:
	INT ParserFile(const char *pName);

private:
	char mStringList[MAX_ITEM_COUNT][MAX_ITEM_LEN];
	//max lang file cnt
	char mFileList[MAX_LANG_FILES][MAX_LANG_FILE_LEN];
	//current file path
	STRING mCurPath;
	//total lang file
	INT mTotalLangs;
	//current lang index
	INT mCurLang;
	//default lang index
	INT mDefLang;	
	STRING mFilesbk;

private:
	LangParser();
	STATIC LangParser* mpInstance;
};

#define LP							(LangParser::GetInstance())
}

#endif //__LANGPARSER_H__