/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __RESADAPTER_H__
#define __RESADAPTER_H__

#include "inogui.h"
#include "log.h"

namespace INONEGUI_FRAMEWORK {

#define RESTYPE_MASK			0xFFFFFF00
#define RESTYPE_OFFSET			8

enum EResType
{
	RESTYPE_PIC		= 0x01,
	RESTYPE_NINEPIC	= 0x02,
	RESTYPE_AUDIO		= 0x04,
	RESTYPE_MOVIE		= 0x08,
	RESTYPE_TEXT		= 0x10,
	RESTYPE_LAYOUT  	= 0x20,
    RESTYPE_RAW         = 0x40,
};
#define RESTYPE_RAW_EXT_LEN    3

#define RESCOMPRESS_MASK		0x000000FF
#define RESCOMPRESS_OFFSET		0
enum ECompressType
{
	COMPRESSTYPE_NOTHING = 0x0,
	COMPRESSTYPE_ZLIB = 0x1,
	COMPRESSTYPE_LZ4 = 0x2,
};

struct SPicItemParam
{
	WORD16		wPicW;
	WORD16		wPicH;
};

struct SFileItemInfo
{
	DWORD32	dwAttrib;
	DWORD32	unFileId;
	DWORD32	unPos;
	DWORD32	unSize;
	union {
		SPicItemParam sPicParam;
	};

	SFileItemInfo()
	{
		dwAttrib	= 0;
		unFileId	= -1;
		unPos		= 0;
		unSize		= 0;
		sPicParam	= {0};
	}
};

struct SFileInfo
{
	BYTE	m_byMagic[8];
	BYTE	m_byMacClrDepth;
	BYTE	m_reserved1[2];
	WORD16	m_byScreenW;
	WORD16	m_byScreenH;
	DWORD32	m_reserved2;
	DWORD32	m_dwVer;
};

typedef std::vector<SFileItemInfo> VtFileItems;
typedef std::vector<SFileItemInfo>::iterator IterFileItems;

class CResAdapter
{
public:
        CResAdapter(void);
        ~CResAdapter(void);

        BOOL IsHasOpen(void);
        BOOL SetResPath(const WCHAR* pszFilePath);
        BOOL OpenRes(void);
        VOID CloseRes(void);

        BOOL GetFile(DWORD dwFileId, OUT RWHANDLE& hFileHandle, OUT int& nFilePos, OUT int& nFileLen, OUT int& nAttrib);

        BOOL GetFile(DWORD dwFileId, OUT RWHANDLE& hFileHandle, OUT SFileItemInfo& info);
        BOOL GetFileByIndex(DWORD dwIndex, OUT RWHANDLE& hFileHandle, OUT SFileItemInfo& info);
        INT GetFileCnt(void);
        LPCTSTR GetResPath(){return m_szResPath;};
        RWHANDLE GetResHandle(){return m_hResFile;};

        BOOL GetFileLen(DWORD dwId, OUT int& nFileLen);
        BOOL GetLayoutFile(OUT SFileItemInfo& info);
public:
        INT m_useCount;
        
private:
        BOOL ReadHeader(void);
        BOOL m_bFileOpened; 
        RWHANDLE m_hResFile;
        SFileInfo m_fileInfo;
        VtFileItems m_vtFileItems;
        WCHAR m_szResPath[MAX_PATH];
};

}
#endif //__RESADAPTER_H__
