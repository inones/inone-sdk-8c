/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __CURSORMANAGE_H__
#define __CURSORMANAGE_H__

#include "inogui.h"
#include "layer.h"

namespace INONEGUI_FRAMEWORK {

class api_out CursorManage: public ILayerHook
{
public:
	CursorManage();
	virtual ~CursorManage();
	STATIC CursorManage* GetInstance();
	INT SetCursorPos(RECT rtCursor, INT iParam = NULL);
	VOID SetCursorStatus(BOOL bShow);
	INT DoInit();
	INT DeInit();
	
private:
	INT InvalidateRectArea(RECT stArea);
	//HOOK Callback Start
	INT PreOnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
	INT OnDraw(PHDC pHDc, RECT rtPaint); 
	STRING GetName();  
	INT GetLayerPrior(); 
	RECT* GetLayerRect();
	BOOL SetLayerStatus(ELayerFlagType eFlag, BOOL bSet);
	BOOL CheckLayerStatus(ELayerFlagType eFlag);
	//HOOK Callback End

public:
	friend class ActivityManage;
	
private:
	STATIC CursorManage* mpInstance;
	RECT mCurCursorRect;
	INT mLayerStatus;
	INT mLayerHandle;
	LTGUI_MUTEX *mpMutex;
};

#define CURSORM                                 (CursorManage::GetInstance())
} 
#endif //__CURSORMANAGE_H__