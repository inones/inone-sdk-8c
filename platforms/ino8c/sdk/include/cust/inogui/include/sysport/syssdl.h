/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __SYSSDL_H__
#define __SYSSDL_H__
#include "SDL.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include "SDL_rotozoom.h"
#include "SDL_gfxPrimitives.h"

//define
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
#define LTGUI_SwapLE16(X)	(X)
#define LTGUI_SwapLE32(X)	(X)
#define LTGUI_SwapLE64(X)	(X)
#define LTGUI_SwapBE16(X)	SDL_Swap16(X)
#define LTGUI_SwapBE32(X)	SDL_Swap32(X)
#define LTGUI_SwapBE64(X)	SDL_Swap64(X)
#else
#define LTGUI_SwapLE16(X)	SDL_Swap16(X)
#define LTGUI_SwapLE32(X)	SDL_Swap32(X)
#define LTGUI_SwapLE64(X)	SDL_Swap64(X)
#define LTGUI_SwapBE16(X)	(X)
#define LTGUI_SwapBE32(X)	(X)
#define LTGUI_SwapBE64(X)	(X)
#endif
#define LTGUI_ISBIG_ENDIAN     		(SDL_BYTEORDER == SDL_BIG_ENDIAN)
#define LTGUI_ISLITTLE_ENDIAN     	(SDL_BYTEORDER == SDL_LIL_ENDIAN)
#define LTGUI_EVENT                            SDL_Event
#define LTGUI_THREAD                         SDL_Thread
#define LTGUI_MUTEX                           SDL_mutex
#define LTGUI_SEM                                SDL_sem
#define LTGUI_SURFACE                        SDL_Surface
#define LTGUI_RECT                        	      SDL_Rect
#define LTGUI_TIMERID                        SDL_TimerID
#define LTGUI_PIXELFORMAT		      SDL_PixelFormat
#define LTGUI_MALLOC                          SDL_malloc
#define LTGUI_FREE                               SDL_free

//Lock mutex
#define LTGUI_CREATE_LOCK()                         (SDL_CreateMutex())
#define LTGUI_LOCK(pMutex)                          (SDL_mutexP(pMutex))                              
#define LTGUI_UNLOCK(pMutex)                      (SDL_mutexV(pMutex))
#define LTGUI_DESTROY_LOCK(pMutex)         (SDL_DestroyMutex(pMutex))

//Sem
#define LTGUI_CREATE_SEM(iInitalV)                                 (SDL_CreateSemaphore(iInitalV))
#define LTGUI_WAIT_SEM(pSem)                                        (SDL_SemWait(pSem))
#define LTGUI_TRYWAIT_SEM(pSem)                                  (SDL_SemTryWait(pSem))
#define LTGUI_WAIT_SEMTIMEOUT(pSem,iTimeoutMs)     (SDL_SemWaitTimeout(pSem,iTimeoutMs))
#define LTGUI_POST_SEM(pSem)                                          (SDL_SemPost(pSem))
#define LTGUI_GET_SEM(pSem)                                            (SDL_SemValue(pSem))
#define LTGUI_DESTROY_SEM(pSem)                                   (SDL_DestroySemaphore(pSem))

//Thread 
#define LTGUI_GET_THREADID()                                        (SDL_ThreadID())
#define LTGUI_CREATE_THREAD(pFn,pData)                    (SDL_CreateThread(pFn, pData))
#define LTGUI_WAIT_THREAD(pThread,pStatus)             (SDL_WaitThread(pThread,pStatus))
#define LTGUI_SET_THREADPRIOR(iPrior)                         (SDL_SetThreadPriority((SDL_ThreadPriority)iPrior))
#define LTGUI_GET_TTID()                                                    (syscall(SYS_gettid))
#define LTGUI_SET_THREADNAME(name)                            (prctl(PR_SET_NAME, name, 0, 0, 0))
#define LTGUI_GET_TICK()                                                    (SDL_GetTicks())

//Video
#define LTGUI_LOWER_INIT(iFlag)        (SDL_Init(iFlag))
#define LTGUI_LOWER_SET_VIDEOMODE(iHRes, iVRes, uColorDepth, flags)         (SDL_SetVideoMode(iHRes, iVRes, uColorDepth, flags))

//MISC
#define LTGUI_DELAY(iMs)                                                    (SDL_Delay(iMs))

//Timer
#define LTGUI_ADD_TIMER(iMs,pFn,pData)                        (SDL_AddTimer(iMs,pFn,pData))
#define LTGUI_REMOVE_TIMER(iHandle)                             (SDL_RemoveTimer(iHandle))
#define LTGUI_TIMER_TICKMS                                              (50) 

//FILE RW
#define RWHANDLE                                                                 SDL_RWops*
#define LTGUI_RWOPEN_F                                                     SDL_RWFromFile
#define LTGUI_RWREAD                                                         SDL_RWread
#define LTGUI_RWWRITE                                                       SDL_RWwrite
#define LTGUI_RWSEEK                                                          SDL_RWseek
#define LTGUI_RWCLOSE                                                        SDL_RWclose

//SO LOAD
#define LTGUI_DLOPEN                                                        dlopen
#define LTGUI_DLSYM                                                          dlsym
#define LTGUI_DLCLOSE                                                       dlclose
#define LTGUI_DLERROR 								dlerror

//string
#define LTGUI_ITOA									SDL_ltoa
#endif //__SYSSDL_H__
