/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __IME_PINYIN_H__
#define __IME_PINYIN_H__
#include "inogui.h"
#include "log.h"
#include "imemanage.h"

#define PINYIN_MAGIC_NUMBER          "CCEGB"
#define PINYIN_ENAME_LENGTH          24
#define PINYIN_CNAME_LENGTH          16
#define PINYIN_WORD_LENGTH           16
#define PINYIN_SELECT_KEY_LENGTH     16
#define PINYIN_KEY_LENGTH            16
#define PINYIN_SEL_LENGTH            58


typedef struct {
	unsigned int key1;
	unsigned int key2;
	unsigned short word_offset_idx;    //pharse_offset table index
	unsigned short frequency;
}LTWDT_PINYIN_ITEM;

typedef struct {
	unsigned int off_begin; //begin offset
	unsigned int off_end;   //end offset
}LTWDT_PINYIN_PHOFFSET;

typedef struct {
	char magic_number[sizeof(PINYIN_MAGIC_NUMBER)];
	char ename[PINYIN_ENAME_LENGTH];                  //ascii name
	char cname[PINYIN_CNAME_LENGTH];                  //promt
	char selkey[PINYIN_SELECT_KEY_LENGTH];            //select keys

	char last_full;
	int  totalKey;
	int  maxPress;
	int  maxDupSel;
	int  totalChar;

	unsigned char  keyMap[128];
	unsigned char  keyName[64];
	unsigned short keyIndex[64];

	int    wordNum;
	char*  wordFile;
	char*  assocFile;
	LTWDT_PINYIN_ITEM * item;
}LTWDT_PINYIN_IME_TABLE;

#define PINYIN_MAX_WORD    64
#define PINYIN_MAX_WORDLEN 64
#define PINYIN_INPUT_MAX     32
typedef struct {
	union {
		char chars[4];
		const char* word;
	}word;
	int         len;
}LTWDT_PINYIN_WORD_INFO;

namespace INONEGUI_FRAMEWORK {
class ImePinyinEngine:public ImeEngine{
     public:
	 ImePinyinEngine();
	 virtual ~ImePinyinEngine();
	 friend class ImePinyinIterator;
	 LPCTSTR ImeName();
	 LPCTSTR GetEncoding();
	 LTWDT_CB_RETRIEVE_CHAR GetRetrieveChar();
	 ImeIterator* NewIterator();
	 ImeIterator* NewAssocIterator();
	 BOOL  FindWords(ImeIterator *pIt, LPCTSTR input, INT start); 
	 BOOL  FindAssociate(ImeIterator *pIt, LPCTSTR words);
	 
     private:
	  LTWDT_PINYIN_IME_TABLE * ime_table;
};

class ImePinyinIterator:public ImeIterator{
     public:
	friend class ImePinyinEngine;
	ImePinyinIterator();
	virtual ~ImePinyinIterator();
	VOID Empty();
	int Count();
	int Next();
	int Prev();
	LPCTSTR Word();
	int Locate(int off, int type);
	INT CurIndex();
	void set_word_direct(char* word, LTWDT_PINYIN_WORD_INFO* words, BOOL isAssociate);
	void update_word();
	BOOL py_find_match_keys( LTWDT_PINYIN_IME_TABLE *ime_table,int* input_keys,int start);
	INT py_set_word_info(LTWDT_PINYIN_IME_TABLE *ime_table, LTWDT_PINYIN_WORD_INFO* word, int index);
	BOOL py_find_words( LTWDT_PINYIN_IME_TABLE *ime_table, int input_count);
	BOOL py_find_associate_key( LTWDT_PINYIN_IME_TABLE* ime_table, const char* input);
	BOOL py_find_associate_words( LTWDT_PINYIN_IME_TABLE* ime_table);
	
     private:
	  
     private:
	int           count; 
	int           cur; 
	unsigned int charIndex[PINYIN_INPUT_MAX];
	unsigned int  startKey;
	unsigned int  endKey;
	unsigned long matched_key1;
	unsigned long matched_key2;
	LTWDT_PINYIN_WORD_INFO words[PINYIN_MAX_WORD]; 
	char          word[PINYIN_MAX_WORDLEN];
	BOOL          isAssociate;
};

class api_out ImePinyin:public Ime{
    public:
       virtual ~ImePinyin();
       STATIC ImePinyin* GetInstance();
	   
    private:
        ImePinyin();
	 STATIC ImePinyin *mpInstance;
};

}
#endif //__IME_PINYIN_H__
