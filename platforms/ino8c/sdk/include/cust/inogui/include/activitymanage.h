/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __ACTIVITYMANAGE_H__
#define __ACTIVITYMANAGE_H__

#include "inogui.h"
#include "window.h"
#include "displaymanage.h"
#include "layer.h"

namespace INONEGUI_FRAMEWORK {

#define ClrAMPrivAttAll()               	  	    				CLRBIT_ALL(mSysCfg.iAtt)
#define SetAMPrivAttValue(iBitValue)           			SETBIT_ALL(mSysCfg.iAtt,iBitValue)
#define ClrAMPrivAttByBit(iBitValue)            				CLRBIT_BY_BIT(mSysCfg.iAtt,iBitValue)
#define SetAMPrivAttByBit(iBitValue)            			SETBIT_BY_BIT(mSysCfg.iAtt,iBitValue)
#define IsSetAMPrivAttByBit(iBitValue)        			ISSET_BY_BIT(mSysCfg.iAtt,iBitValue)
#define GetValueAMPrivAttByOffset(mask,offset)  		GETVAL_BY_OFFSET(mSysCfg.iAtt,mask,offset)
#define SetValueAMPrivAttByOffset(mask,offset,val)  		SETVAL_BY_OFFSET(mSysCfg.iAtt,mask,offset,val)

enum ActFlag{
	ACT_FLAG_NOTHING  	= 0x000,
	ACT_FLAG_CLRSTACK  	= 0x0001,
};

class api_out ActivityManage:public Window,
								  public IWindowListener,
                                                             public ILayerHook
{
public:
        virtual ~ActivityManage();
        STATIC ActivityManage* GetInstance();
        STATIC INT SetSysCfgKeyValue(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj, STRING strKeyName);
        STATIC INT SetUserCfgKeyValue(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj, STRING strKeyName);
        INT LaunchActivity(STRING strActivityName,INT iLayoutID = ACTIVITY_LAYOUT_MAIN, LPVOID pParam = NULL, INT iActFlag = ACT_FLAG_NOTHING);
        INT LaunchService(STRING strServiceName,LPVOID pParam = NULL);
        INT StartSimpleActivity(STRING strActivityName, INT iLayoutID = ACTIVITY_LAYOUT_MAIN, LPVOID pParam = NULL , INT iActFlag = ACT_FLAG_NOTHING);
        INT StopSimpleActivity(STRING strActivityName, INT iActFlag = ACT_FLAG_NOTHING);
        BOOL GetFocusAppNameLayoutId(STRING &strActivityName, INT *piLayoutID = NULL);
        HWND FindAppHwndByName(STRING strAppName);
        STRING FindAppNameByHwnd(HWND hWnd);
        INT SetLangId(INT iLangId);
        INT GetLangId();
        VOID SetWallPaper(LPCTSTR pWallpaper);
        VOID ReloadWallPaper();
        LPVOID GetWallPaperDC();   
        INT DoInit(CHAR *pFirstRunApp = NULL,INT iHRes = 800,INT iVRes = 480);
        INT DeInit();
        BOOL IsAMReady(){return mAMReady;};
        INT GetScreenWide(){return mScreenRect.w;};
        INT GetScreenHeight(){return mScreenRect.h;};
        RECT& GetScreenRect(){return mScreenRect;};
        RECT& GetContentRect(){return mContentRect;};
        FLOAT GetTouchScaleX(){return mSysCfg.fTouchScaleX;};
        FLOAT GetTouchScaleY(){return mSysCfg.fTouchScaleY;};
        INT GetTouchOffsetX(){return mSysCfg.iTouchOffsetX;};
        INT GetTouchOffsetY(){return mSysCfg.iTouchOffsetY;};
        UINT8 GetSysWndAnim(){return mSysCfg.iWndAnim;};
        BOOL IsDisplayStrict(){return IsSetAMPrivAttByBit(LTGUICFG_ATT_DISSTRICT);};
        BOOL IsShowFps(){return IsSetAMPrivAttByBit(LTGUICFG_ATT_SHOWFPS);};
        BOOL IsShowFocus(){return IsSetAMPrivAttByBit(LTGUICFG_ATT_SHOWFOCUS);};
        BOOL IsColor16Bit(){return IsSetAMPrivAttByBit(LTGUICFG_ATT_COLOR16BIT);};
        UINT32 GetFocusClr(){return mSysCfg.iFocusClr;};
        EGestureModeType GetGestureMode(){return mSysCfg.eGestureMode;};
        UINT16 GetGestureMaxTime(){return mSysCfg.iGestureMaxTime;};
        UINT16 GetGestureMinDist(){return mSysCfg.iGestureMinDist;};
        VOID SetTouchEnable(BOOL bEnable){mTouchEnable = bEnable;};
        BOOL IsTouchEnable(){return mTouchEnable;};
        LPCTSTR GetCfgPath();
        LPVOID GetFocusActivity(){return mActivityStack.GetAt(0);};
        //IWindowListener
        STRING GetWindowName();
        HWND GetWindowHandle(); 
        LRESULT OnDerivedWndProc(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);      
        INT PostQuitMsgToAllWnd();

        //Layer
        INT UnregUserLayer(INT hLayerHand);
        INT RegUserLayer(ILayerHook *pHook);
        BOOL IsValidLayerPrior(INT iPrior);
        BOOL IsValidUserLayerPrior(INT iPrior);
        INT DispatchEventToHook(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        INT DispatchDrawToHook(LPVOID pDc, RECT rtPaint);
        INT RegInternalLayer(ILayerHook *pHook);
        INT UnregInternalLayer(INT hLayerHand);

        //HOOK Callback Start
        INT PreOnEvent(UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
        INT OnDraw(PHDC pHDc, RECT rtPaint); 
        STRING GetName();  
        INT GetLayerPrior(); 
        RECT* GetLayerRect();
        BOOL SetLayerStatus(ELayerFlagType eFlag,BOOL bSet);
        BOOL CheckLayerStatus(ELayerFlagType eFlag);
        //HOOK Callback End
        friend INT DisplayThreadTask(void *para);
private:
        ActivityManage();
        VOID InitialWallPaper();
        VOID DrawWallPaperToDC(LPVOID pDc);
        INT CalcFirstDrawLayerIndex(RECT &rtPaint);
        INT LaunchSysActivity(STRING strActivityName,INT iLayoutID = ACTIVITY_LAYOUT_MAIN, LPVOID pParam = NULL, BOOL bPending = FALSE);
        INT PushActivity(LPVOID pAct);
        INT PopActivity( );
        INT AddWindow(Window *pWind, CPtrArray *plist);
        Window* CheckInstanceExist(STRING &strActivityName, CPtrArray *pChecklist = NULL);
        Window* CheckInstanceExist(HWND hWnd, CPtrArray *pChecklist = NULL);
        INT InnerGoBack();
        INT InnerGoHome();
        INT InnerLaunchActivity(Window *pActivity, INT iLayoutID = ACTIVITY_LAYOUT_MAIN, LPVOID pParam = NULL, INT iActFlag = ACT_FLAG_NOTHING);
        INT SaveUserCfg();
        INT ReadSystemCfg();
        INT ReadUserCfg();
        
protected:
        SystemCfg mSysCfg;
        STRING mWallpaperPath;
	
private:

        STATIC ActivityManage* mpInstance;
        LPVOID mpLayer[LAYER_PRIOR_MAX];
        RECT mScreenRect;
        RECT mContentRect;

        LPVOID mWallpaperDc;
        BOOL mAMReady;
        BOOL  mTouchEnable;	 

        CPtrArray mServicesList;
        CPtrArray mSysActivityList;
        CPtrArray mActivityStack;
        CPtrArray mUserActivityList;
        INT mLayerStatus;
        INT mLayerHandle;
        HWND mpImeWnd;
        LTGUI_MUTEX *mpLaunchMutex;
        LTGUI_MUTEX *mpMutex;
        LTGUI_MUTEX *mpLayerMutex;
};

#define AM                                 (ActivityManage::GetInstance())
#define HWND_MAIN                (ActivityManage::GetInstance())


static inline INT PostQuit2AllMessageThreads ()
{

    //For all Wnd 
    AM->PostQuitMsgToAllWnd();

    return ERR_OK;
}
}//end namespace

#endif // __ACTIVITYMANAGE_H__
