/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __TIMERMANAGE_H__
#define __TIMERMANAGE_H__
#include "inogui.h"
#include "log.h"

namespace INONEGUI_FRAMEWORK {
#define DEF_ALLTIMER			0xFFFFFFFF    //remove user all timer

class IUserTimer;

class ITimerListener {  
public:  
    virtual ~ITimerListener() { }  
    virtual VOID OnTimer() = 0;  
}; 

typedef struct _UTimerDataType{
	IUserTimer *pListener;
	LTGUI_TIMERID hTimer;
	LPVOID pUser; 
	UINT32 iInterval;
	
	_UTimerDataType(){
		pListener = NULL;
		hTimer = NULL;
		pUser = NULL;
		iInterval = 0;
	};
}UTimerDataType;

STATIC UINT32 UserTimerCallback(UINT32 uInterval, LPVOID lParam);
class IUserTimer {  
	public:  
		IUserTimer();
		virtual ~IUserTimer();
		virtual VOID OnUserTimer(LPVOID pUser,UINT32 iInterval) = 0;  
		BOOL SetUserTimer(LPVOID pUser, UINT32 iInterval);
		VOID UnsetUserTimer(LPVOID pUser, UINT32 iInterval = DEF_ALLTIMER);
		friend UINT32 UserTimerCallback(UINT32 uInterval, LPVOID lParam);
	private:
		UTimerDataType* FindUserTimer(LPVOID pUser, UINT32 iInterval);
		UTimerDataType* NewTimer(LPVOID pUser, UINT32 iInterval);
	private:
		STATIC LTGUI_MUTEX *mpUserTimerMutex;
		IUserTimer *mpUserTimerListener;
		CPtrArray mUserTimerList;
}; 

STATIC UINT32 TimerCallback(UINT32 uInterval, LPVOID lParam);
class api_out TimerManage{
    public:
        virtual ~TimerManage();
        STATIC TimerManage* GetInstance();
        LHANDLE RegTimerListener(ITimerListener *pListener);
        INT UnregTimerListener(LHANDLE hTimerHand);  
        friend UINT32 TimerCallback(UINT32 uInterval, LPVOID lParam);
    private:
        TimerManage();
        INT DispatchTimerEventToListener();

    private:
        STATIC TimerManage *mpInstance;
        CPtrArray mTimerListenerList;
        LTGUI_TIMERID mTimerHandle;
        LTGUI_MUTEX *mpMutex;
};
}
#endif //__TIMERMANAGE_H__