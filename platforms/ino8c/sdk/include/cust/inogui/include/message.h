/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include "inogui.h"
#include "queue.h"
#include "hwnd.h"
#include "log.h"

namespace INONEGUI_FRAMEWORK {

//#define DDEBUG_MSG		1

class Message{

public:
        Message();
        Message(STRING strName);
        Message(STRING strName,UINT uSize);
        virtual ~Message();
        VOID DoPendingSyncMessage();
        INT PostMessage (HWND hWnd, UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL);
        BOOL GetMessage (HWND hWnd, PMSG pMsg);
        VOID MessageInit();
        VOID MessageQuit();
        VOID CancelMessage(HWND hWnd = NULL);
        virtual INT GetThreadIDFromHWnd(HWND hWnd){return ERR_INVALID;};
        virtual STRING  GetWndName(HWND hWnd){return NULL;};  
        virtual INT DispatchMessage (PMSG pMsg){return ERR_INVALID;};
protected:
        LRESULT SendSyncMessage (HWND hWnd, UINT iMsg,WPARAM wParam = NULL, LPARAM lParam = NULL);

protected:
        INT mWndThreadID;
	
private:
        LTGUI_SEM *mpMsgSem;
        LTGUI_SEM *mpMsgQSem;
        LTGUI_MUTEX *mpMutex;
        //Sync msg
        PSYNCMSG mpHeadSyncMsg;     // head
        PSYNCMSG mpTailSyncMsg;      // tail
        DWORD mMsgState;	
        Queue mQueue;
};

}
#endif //__MESSAGE_H__