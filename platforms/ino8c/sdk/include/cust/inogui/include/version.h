/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __VERSION_H__
#define __VERSION_H__

namespace INONEGUI_FRAMEWORK {
	
#define gui_version	("INONEGUI: " __DATE__ " " __TIME__)

}//end namespace

#endif // __VERSION_H__