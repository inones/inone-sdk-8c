/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOPUBFUN_H__
#define __INOPUBFUN_H__

#include "inogui.h"

namespace INONEGUI_FRAMEWORK {

class api_out InoPublicFun
{
public:
	virtual ~InoPublicFun();
	STATIC InoPublicFun* GetInstance();
	VOID DoInit();
	VOID DeInit();	
	STRING& GetCurPath();
	STRING BuildFileFullPath(INT iPathType, STRING strFile);
	STRING GetPathByType(INT iPathType);
	STRING& RectToString(RECT rt);
	STRING& RectpadToString(RECTPAD rt);	
	//To HEX string
	STRING& IntToHexString(INT val);	
	//To HEX string,and 0 num
	STRING& IntToHexString1(INT val, INT zcnt);
	//To Dec  
	STRING& IntToDecString(INT val, INT zcnt);
	VOID Split(STRING& s, STRING& delim,vector< STRING >* ret);
	INT WriteCfgFile(const char *pFile, BYTE *buf, INT length);
	INT ReadCfgFile(const char *pFile, BYTE *buf, INT maxlength);
	INT ReadInternalCfgFile(const char *pFile, BYTE *buf, INT maxlength);
	INT WriteToFile(const char *pFile, BYTE *buf, INT length);
	INT ReadFromFile(const char *pFile, BYTE *buf, INT maxlength);
	INT ParserJsonBuffer(LPVOID pJsonBuff, INT iBuffLen, LPVOID pCallObj, CBSetKeyValue pCb, CBSetObject pCbObj = NULL);
	INT ParserJsonFile(LPCTSTR pJsonPath, LPVOID pCallObj, CBSetKeyValue pCb, CBSetObject pCbObj = NULL);
       //Write json file
       LPVOID OpenJsonFile( LPCTSTR pJsonPath);
       INT AddJsonKeyValue( LPVOID pJsonObj, STRING strKey, INT iValue);
       INT AddJsonKeyValue( LPVOID pJsonObj, STRING strKey, STRING strValue);
       INT CloseJsonFile( LPCTSTR pJsonPath, LPVOID pJsonObj);
       
	BOOL DigitalStringToInteger(LPCTSTR pString, INT *pInteger);
	BOOL IsDigitalString(LPCTSTR pString);
	BOOL CheckAndAdjustUiPath(LPVOID hResAdapter, STRING &rUiName);
	
private:
	InoPublicFun();
	INT ParserArrayOrObject(cJSON *cJsonV, STRING strObjName,LPVOID pCallObj, CBSetKeyValue pCb, LPVOID pJsonObj = NULL, CBSetObject pCbObj = NULL);

private:
	STATIC InoPublicFun* mpInstance;
	STRING mCurPath;
	LTGUI_MUTEX *mpLockWriteRead;

};

#define PBF							(InoPublicFun::GetInstance())

//build path for etc(cfg info)
#define BPATH_ETC(X)			(PBF->BuildFileFullPath(GUI_PATH_ETC,X))
#define BPATH_ETC_C(X)			(BPATH_ETC(X).c_str())
//build path for usretc(cfg info)
inline STRING BPATH_USRETC(STRING X)	{return PBF->BuildFileFullPath(GUI_PATH_ETC,"/usr/"+X);}
#define BPATH_USRETC_C(X)			(BPATH_USRETC(X).c_str())
//build path for ui(layout/image)
#define BPATH_UI(X)				(PBF->BuildFileFullPath(GUI_PATH_UI,X)) 
#define BPATH_UI_C(X)			(BPATH_UI(X).c_str())
//build path for application
#define BPATH_APP(X)			(PBF->BuildFileFullPath(GUI_PATH_APP,X))
#define BPATH_APP_C(X)			(BPATH_APP(X).c_str())
//build path for service
#define BPATH_SERV(X)			(PBF->BuildFileFullPath(GUI_PATH_SERV,X))
#define BPATH_SERV_C(X)		(BPATH_SERV(X).c_str())
//build path for font
#define BPATH_FONT(X)			(PBF->BuildFileFullPath(GUI_PATH_FONT,X))
#define BPATH_FONT_C(X)		(BPATH_FONT(X).c_str())
//build path for lang
#define BPATH_LANG(X)			(PBF->BuildFileFullPath(GUI_PATH_LANG,X))
#define BPATH_LANG_C(X)		(BPATH_LANG(X).c_str())
}//end namespace

#endif // __INOPUBFUN_H__
