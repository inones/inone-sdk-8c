/**
 *  @file gpio.h
 *  @brief gpio header file
 */
#ifndef __GPIO_H__
#define __GPIO_H__

#define GPIO_OP_PREFIX		"/sys/class/gpio_sw/"
#define GPIO_OP_CFG			"/cfg"
#define GPIO_OP_DATA		"/data"

//function define header
#ifdef __cplusplus
extern "C" {
#endif
int set_gpio_value(char* pinname, int value);
int get_gpio_value(char* pinname);
int set_gpio_input(char* pinname);
int set_gpio_output(char* pinname);
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif