#ifndef _STORAGE_H_
#define _STORAGE_H_
#include <string>
#include <vector>

class Storage
{
private:
    static struct fdb_kvdb kvdb;
    static Storage *mpInstance;
private:
    Storage(const char *path, uint32_t secSize, uint32_t secNum);

public:
    // 数据库最大容量 = secSizeInByte * secNum
    // storagePath: 数据库存储路径
    // secSizeInByte: 扇区大小(Byte)，为4096的倍数，默认为4KB。默认无法存入超过 4096 长度的 KV 。
    // 如果想要存入比如：10K 长度的 KV ，可以通过设置扇区大小为 12K，即secSizeInByte=12288 Byte (4096*3 = 12288)
    // secNum: 扇区个数, 默认为4。
    static Storage *getInstance(const char *storagePath = "/mnt/UDISK/fdb_kvdb2", uint32_t secSizeInByte = 4096, uint32_t secNum = 4);
    bool putString(const std::string &key, const std::string &value);
    bool putInt(const std::string &key, int value);
    bool putFloat(const std::string &key, float value);
    bool putBool(const std::string &key, bool value);
    bool putBlob(const std::string &key, void *obj, uint size);
    std::string getString(const std::string &key, const std::string &defValue = "");
    int getInt(const std::string &key, int defValue = 0);
    float getFloat(const std::string &key, float defValue = 0.0f);
    bool getBool(const std::string &key, bool defValue = false);
    void getBlob(const std::string &key, void *obj, uint size);
    bool del(const std::string &key);
    std::vector<std::string> getKeys();
};
#endif
