/**
* Copyright (C) 2021 Shenzhen Aiinone Technology Co., Ltd.
* 
* Author: AiinoneTeam
* Detail: www.aiinone.cn
* History:
*
*/

#ifndef __INOUPDATERES_H__
#define __INOUPDATERES_H__

#include "baseactivity.h"
#include "resmanage.h"

namespace INONEGUI_FRAMEWORK
{
    #define MAX_IMAGE_CNT   20
    
    typedef struct _InoImage
    {
        STRING strImgName;
        STRING strMd5;
        STRING strData;
    } InoImageType;

    typedef struct _InoUpdatePkg{
        STRING strPkgName;
        STRING strVer;
        STRING strDate;
        INT iImgCnt;
        InoImageType stInoImage[MAX_IMAGE_CNT];
    } InoUpdatePkgType;

    typedef enum
    {
        EUPDATE_OK,              //OK
        EUPDATE_ERR_PARAM,       //update paramter error
        EUPDATE_ERR_NOT_EXIST,   //update file not exist
        EUPDATE_ERR_SRC_MD5,     //image src md5 fail
        EUPDATE_ERR_VER_MD5,     //verity md5 fail
        EUPDATE_ERR_OTHER,       //other error
        EUPDATE_ERR_USER_CANCEL, //user cancel
    } EInoUpdateCode;

    typedef VOID (*CbUpdateListener)(STRING strPartition, EInoUpdateCode eCode, INT ProgressValue);

    class InoUpdateRes
    {
    public:
        STATIC InoUpdateRes *GetInstance();
        virtual ~InoUpdateRes();
        InoUpdatePkgType *DoInit(STRING strPkgPath, CbUpdateListener UpdateListener, STRING strUpdateScript = "inoupdatescript.sh");
        INT DeInit();
        STATIC INT OnUpDateJobProc(VOID *pParam);
        STATIC LPVOID SetImgObject(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj);
        STATIC INT SetImgKeyValue(LPVOID pCallObj, cJSON *cJsonV, STRING strObjName, LPVOID pJsonObj, STRING strKeyName);
        InoUpdatePkgType *GetInoUpdatePkgInfo(){return &mInoUpdatePkg;};
        INT StartUpdate();
        INT StopUpdate();
        VOID ResetUpdate();

    private:
        InoUpdateRes();

    private:
        STATIC InoUpdateRes *mpInstance;
        STRING mStrInoPkgPath;
        STRING mStrInoUpdateScript;
        InoUpdatePkgType mInoUpdatePkg;
        CResAdapter *mpResimg;
        CbUpdateListener mUpdateListener;
        BOOL    mUpdateStarting;
        BOOL    mCancelUpdate;
        LTGUI_THREAD *mpUpdateTh;
    };
}

#define INOU (InoUpdateRes::GetInstance())
#endif //__INOUPDATERES_H__