@echo off

setlocal
::set OLDPATH=%PATH%
::set PATH=%PATH%;%~dp0\bin;"%~dp0\..\..\..\tools"
set PATH=%SystemRoot%;%SystemRoot%\system32;%~dp0\bin;"%~dp0\..\..\..\tools"
::adb shell killall -9 supergui gdbserver
set TARGET_DIR=/mnt/UDISK/ltgui
set TARGET_APPDIR=%TARGET_DIR%/app
set TARGET_UIDIR=%TARGET_DIR%/res/ui
set TARGET_RUNENV=killall supergui;source /mnt/UDISK/tslib-env.sh;export LD_LIBRARY_PATH=/mnt/UDISK/ltgui/lib;export SDL_MOUSEDRV=TSLIB;export SDL_MOUSEDEV=/dev/input/event1;chmod a+x supergui

::adb push %1 %TARGET_UIDIR%
::adb push %2 %TARGET_APPDIR%/liblthome.so
adb forward tcp:6000 tcp:6000
::adb shell "/mnt/UDISK/gdbserver :6000 /mnt/UDISK/ltgui/supergui &"
::RESTORE DEFAULT PATH
::set PATH=%OLDPATH%
endlocal