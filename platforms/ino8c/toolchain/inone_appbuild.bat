@echo off

setlocal
@REM ADD TOOLCHAIN PATH AND TOOLS TO PATH
set TOOLCHAIN_DIR="%~dp0"
set SDK_DIR=%TOOLCHAIN_DIR%\..\sdk
:: path setting by vscode terminal,remove it
::set OLDPATH=%PATH%
::set PATH=%PATH%;%~dp0\bin;"%~dp0\..\..\..\tools"
set PATH=%SystemRoot%;%SystemRoot%\system32;%~dp0\bin;"%~dp0\..\..\..\tools"
set TOOLCHAIN_PREFIX=riscv64-unknown-linux-gnu

::set COMPILE_TARGET="%PROJECTPATH%\native"
set COMPILE_PARAMETER=%1

if [%2]==[] ( 
SET COMPILE_TARGET="%PROJECTPATH%\native"
) else (  
SET COMPILE_TARGET=%2
)

set PLATFORM="ino8c"
@REM check usr folder exist
::set USR_DIR=%TOOLCHAIN_DIR%\usr
::if not exist %USR_DIR% (
::	::echo {%USR_DIR%} not exist,create it!
::	mklink /J %USR_DIR% %TOOLCHAIN_DIR%
::)

@REM define compile gcc env
set CC=%TOOLCHAIN_PREFIX%-gcc
set CXX=%TOOLCHAIN_PREFIX%-g++
set STRIP=%TOOLCHAIN_PREFIX%-strip
set LD=%TOOLCHAIN_PREFIX%-ld
set AR=%TOOLCHAIN_PREFIX%-ar
set NM=%TOOLCHAIN_PREFIX%-nm
set RANLIB=%TOOLCHAIN_PREFIX%-ranlib
set STRIP=%TOOLCHAIN_PREFIX%-strip
set CP=cp
set LN=
set RM=rm -rf
set MKDIR=l_mkdir -p
set MV=mv

@REM common include define
set SDK_DIR=%SDK_DIR:\=/%
set SDK_LIB=%SDK_DIR%/lib
set SDK_INCLUDE=%SDK_DIR%/include
set SDK_CUST_INCLUDE=%SDK_DIR%/include/cust
set RELEASE_APPDIR=%COMPILE_TARGET:\=/%/../out
set STAGING_DIR=%RELEASE_APPDIR%

@REM CONFIG CFLAG AND LINKFLAGS

::SET USE_STDLIBCXX version
::set APP_INCLUDE=-I./ -I./include -I./inonectrl -I$(SDK_CUST_INCLUDE) -I$(SDK_CUST_INCLUDE)/SDL -I$(SDK_CUST_INCLUDE)/inogui/include -I$(SDK_CUST_INCLUDE)/inowidgets/include -I$(SDK_INCLUDE)
::set EXT_CFLAGS=-DD_USE_CJSON -fdiagnostics-color=auto -Os -Wno-conversion-null
::set EXT_LINKFLAGS=-lunwind

::SET USE_UCLIBCXX version
set APP_INCLUDE=-I./ -I./include -I./inonectrl -I$(SDK_CUST_INCLUDE) -I$(SDK_CUST_INCLUDE)/SDL -I$(SDK_CUST_INCLUDE)/inogui/include -I$(SDK_CUST_INCLUDE)/inowidgets/include -I$(SDK_INCLUDE) -I$(SDK_INCLUDE)/uClibc++
set EXT_CFLAGS=-std=c++11 -DD_USE_CJSON -fdiagnostics-color=auto -Os -nostdinc++ -nodefaultlibs -Wno-conversion-null -Wno-deprecated
set EXT_LINKFLAGS=-luClibc++ -lgcc_s -lpthread -lm -lc 

@REM SET BUILD PARAMTER
set APP_CFLAGS= -g -rdynamic $(INCLUDE) -fPIC -Wl,--no-undefined $(EXT_CFLAGS) 
set APP_LINKFLAGS= -L$(SDK_LIB) -linogui -linowidgets -ldl -lpthread $(EXT_LINKFLAGS) 

@REM START COMPILE TARGET
set COMPILE_PARAMETER="-e PLATFORM=%PLATFORM% %COMPILE_PARAMETER%"
set COMPILE_PARAMETER=%COMPILE_PARAMETER:"=%
::echo COMPILE_TARGET:%COMPILE_TARGET% COMPILE_PARAMETER:%COMPILE_PARAMETER%

:: STARTING MSG
call :colortheword "Build {%COMPILE_PARAMETER%} [START]" 09 ""

:: Make build target
make -C %COMPILE_TARGET% %COMPILE_PARAMETER% -s

:: COLOR GREEN:0A RED: 0C BULE:09
if %ERRORLEVEL% NEQ 0 (
    call :colortheword "Build {%COMPILE_PARAMETER%} [FAIL]" 0C ""
) else (
	call :colortheword "Build {%COMPILE_PARAMETER%} [SUCCESS]" 0A ""
)

::RESTORE DEFAULT PATH
::set PATH=%OLDPATH%
endlocal

goto :eof

REM Echo Color string,Paramter<>Must []Option, Invoke <str1=ColorStr> [str2=Color] [str3=NoColorStr]
:colortheword <str1=ColorStr> [str2=Color] [str3=NoColorStr] 
	set "COLORSTR=%~1"
	set "COLOR=07"&if not "%~2."=="." set "COLOR=%~2"
	set "NOCOLORSTR= "&if not "%~3."=="." set "NOCOLORSTR=%~3"
	for /F %%a in ('"prompt $h & for %%b in (1) do rem"')do set /p="%%a%NOCOLORSTR%"<nul>"%COLORSTR%"
	findstr /a:%COLOR% .* "%COLORSTR%" nul
	del /q "%COLORSTR%" >nul 2>nul
	echo .
goto :eof