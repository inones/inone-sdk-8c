@echo off
@echo Run APP

setlocal
adb shell killall -9 superdaemon inogui
set TARGET_DIR=/tmp
set TARGET_APPDIR=%TARGET_DIR%/
set TARGET_UIDIR=%TARGET_DIR%/
set INOGUI_DIR=/mnt/UDISK/inonegui

@REM save paramter
set APP_NAME=%1
set APP_UIRES=%2
set APP_LOGICRES=%3

@REM push target and running
@REM for debug demo board
::adb push %APP_UIRES% %TARGET_UIDIR%
::adb push %APP_LOGICRES% %TARGET_APPDIR%
::adb shell . /mnt/UDISK/inonegui/run-env.sh;/mnt/UDISK/inonegui/inogui %APP_NAME%

@REM for release demo board
adb push %APP_UIRES% %TARGET_UIDIR%
adb push %APP_LOGICRES% %TARGET_APPDIR%
adb shell . %INOGUI_DIR%/run-env.sh %INOGUI_DIR%;export LD_PRELOAD=/tmp/libinogui.so:/tmp/libinowidgets.so;%INOGUI_DIR%/inogui %APP_NAME%
endlocal