#include <stdio.h>

union {
        unsigned long bits32;
        unsigned char bytes[4];
} value;

 int  isLittleEndian() {
        value.bytes[0] = 0;
        value.bytes[1] = 1;
        value.bytes[2] = 0;
        value.bytes[3] = 0;

        return value.bits32 == 256;
}

 int  main() {
	if( isLittleEndian())
			printf("is little endian! \n");
	else
			printf("is big endian! \n");
	return 0;
}