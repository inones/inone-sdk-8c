@echo off
@echo Run APP

setlocal
set TARGET_DIR=/mnt/UDISK
set TARGET_APPDIR=%TARGET_DIR%/
set TARGET_UIDIR=%TARGET_DIR%/

if [%1]==[] ( 
set TARGETNAME=lty_lvdemo
) else (  
SET TARGETNAME=%1
)

@REM save paramter
set APP_PROG=%CD%\out\\%TARGETNAME%

@REM for release demo board
adb push %APP_PROG% %TARGET_APPDIR%
adb shell ln -sf /dev/input/event2 /dev/input/touchscreen;chmod a+x /mnt/UDISK/%TARGETNAME%;/mnt/UDISK/%TARGETNAME%
endlocal
